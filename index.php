<?php
	/*Descrição do arquivo:

	Esta é a pagina inicial do sistema, seu objetivo é apresentar ao aluno uma tela
	contendo as datas de inscrições dos programas e um formulario onde o aluno insere
	seu email e é cadastrado no sistema.
	*/

	require_once('db.php');
	session_start();

	excluir_zip_antigos();
	$periodo_inscricoes = pegar_periodo_inscricoes();

	if(isset($_POST['email'])){
		$email = $_POST['email']; 
		$valida_email = valida_email($email);

		if(is_bool($valida_email)){
			$verifica_bd = verifica_email_bd($email);
		
			if(is_bool($verifica_bd)){
				$cadastra_aluno = cadastra_aluno($email);

				$icone  = "<i class='fa fa-check-circle-o' aria-hidden='true'></i>";
				$titulo = $cadastra_aluno[0];
				$msg 	= envia_email($email, $cadastra_aluno[1]);
				$class 	= 'sucesso';

			}else{ //Se verifica_db retornar mensagem de erro
				$icone = "<i class='fa fa-times-circle-o' aria-hidden='true'></i>";
				$titulo = $verifica_bd;
				$msg 	= envia_email($email);
				$class 	= 'erro';
			}
		}else{
			$icone = "<i class='fa fa-times-circle-o' aria-hidden='true'></i>";
			$titulo = "E-mail inválido!";
			$msg 	= $valida_email;
			$class 	= 'erro';
		}

		echo 	"<div class='modal fade msg' role='dialog'>
					<div class='modal-dialog modal-lg'>
						<div class='modal-content'>
							<div class='modal-header $class'>
								<button type='button' class='close' data-dismiss='modal'>&times;</button>
								<h4 class='modal-title'>$icone $titulo</h4>
	      					</div>

		      				<div class='modal-body' id='corpo-modal'>
		        				<p>$msg</p>
		      				</div>
	      				</div>
	    			</div>
				</div>";
	}

	if(login_adm()){
		$href='"administrador.php"';
	}else{
		$href='"#" id="adm"';

		echo 	"<div class='modal fade' role='dialog' id='login-adm'>
					<div class='modal-dialog'>
						<div class='modal-content'>
							<div class='modal-header login-adm-header'>
								<button type='button' class='close' data-dismiss='modal'>&times;</button>
								<h4 class='modal-title'>Acesso ao Administrador</h4>
	      					</div>

		      				<div class='modal-body' id='corpo-modal'>
		        				<form role='form' id='form-login-adm' class='form-horizontal' role='form' method='post' action=''>
									<div class='form-group'>
										<label class='control-label col-sm-3' for='login'>Login: </label>
										<div class='col-sm-9'>
											<input type='text' id='login' name='login' maxlength='20'/>
										</div>
									</div>

									<div class='form-group'>
										<label class='control-label col-sm-3' for='login'>Senha: </label>
										<div class='col-sm-9'>
											<input type='password' id='senha' name='senha'/>
										</div>
									</div>
									<div id='erro-login'></div>

									<div id='div-botao-login-adm'>
										<button class='bp-2'>Login</button>
									</div>
								</form>
		      				</div>
	      				</div>
	    			</div>
				</div>";
	}

?>

<html>
	<head>
		<title>Pós-Graduação | Instituto de Matemática, Estatística e Computação Científica</title>
        <link rel="icon" href="imagens/favicon.svg" />
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />
		<link rel="stylesheet" href="css/bootstrap.min.css" />
		<link rel="stylesheet" href="css/estilo.css" />
		<link rel="stylesheet" href="css/index.css" />
		<script src="js/jquery-1.11.3.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/jquery-index.js"></script>
	</head>

	<body>
		<div class="container" id="conteudo-principal">
			<nav class="navbar" id="menu">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar" id="botao-menu-responsivo">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span> 
				    	</button>
						<a class="navbar-brand" href="http://www.ime.unicamp.br/"><img src="imagens/logo-imecc-branco.png" id="menu-logo"/> <span id="menu-nome">Instituto de Matemática, Estatística e Computação Científica</span></a>
				    </div>

				    <div class="collapse navbar-collapse" id="navbar">
				    	<ul class="nav navbar-nav navbar-right">
				    		<li class="menu-item"><a href=<?= $href ?> >ADMINISTRADOR</a></li>
				    	</ul>
				    </div>
			  </div>
			</nav>
			<div id="painel">
				<p>Pós-Graduação • Gerenciamento de Inscrições</p>
			</div>

			<div class="row" id="apresentacao">
				<div class="col-sm-4">
					<div class="row">
						<div class="col-sm-6 img-apresentacao">
							<a href="http://www.dac.unicamp.br/"><img src="imagens/logo-dac-posgrad.png"/></a>
						</div>

						<div class="col-sm-6 img-apresentacao">
							<a href="http://www.prpg.unicamp.br"><img src="imagens/logo-prpg.png"/></a>
						</div>

						<div class="col-sm-12 img-apresentacao">
							<a href="http://www.capes.gov.br"><img src="imagens/logo-capes.png"/></a>
						</div>

						<div class="col-sm-6 img-apresentacao">
							<a href="http://www.cnpq.br"><img src="imagens/logo-cnpq.png"/></a>
						</div>
						
						<div class="col-sm-6 img-apresentacao">
							<a href="http://www.fapesp.br"><img src="imagens/logo-fapesp.png"/></a>
						</div>
					</div>
				</div>

				<div id="texto-apresentacao" class="col-sm-8" style="text-align: justify;">
				<!-- <div id="apresentacao">
					<h2><i class="fa fa-info-circle" aria-hidden="true"></i> Acesso</h2>
					<div class="col-sm-8 col-sm-offset-4"> -->
						<p>Olá candidato(a)!</p>
						<p>Através deste sistema você pode enviar os documentos necessários para completar suas inscrições nos programas da Pós-Graduação do IMECC.</p>
						<p>Para acessar, realize o cadastro do seu e-mail clicando no botão abaixo, e lhe encaminharemos uma mensagem com o link de acesso que você utilizará para gerenciar todas as suas inscrições.</p>
						<p>Caso não receba essa mensagem ou acidentalmente a apague, insira novamente seu e-mail e nós reencaminharemos a mensagem.</p>
						
						<button type="button" id="toggle-form-acesso" class="bp-1">Cadastrar e-mail</button>

						<form id="form-acesso" action="" method="post">
							<label id="label-email" class="label-padrao-1">E-mail:</label>
							<input id="email" class="campos-padrao-1" type="text" name="email" placeholder="exemplo@dominio.com" required/>
							<input id="solicitar-acesso" class="bp-1" type="submit" value="Solicitar acesso" />
							<!--<button type="button" class="botao-login">Solicitar Acesso</button>-->
						</form>
				</div>
			</div>

			<div id="periodo-inscricoes">
				<h2><i class="fa fa-calendar" aria-hidden="true"></i> Período de Inscrições</h2>
				<?php
					$programa = "";
					$qtdreg = sizeof($periodo_inscricoes);

					for ($i=0; $i<$qtdreg; $i++) {
						$periodo_inscricoes[$i]['dt_inicial'] = date('d/m/Y', strtotime($periodo_inscricoes[$i]['dt_inicial']));
						$periodo_inscricoes[$i]['dt_final'] = date('d/m/Y', strtotime($periodo_inscricoes[$i]['dt_final']));


						if($periodo_inscricoes[$i]['corrigido'] != $programa) {
							if($programa != "") echo "</tbody>";
							echo "<table class='table table-hover tabela-periodo-inscricoes'>
									<thead class='nome-programa'><tr><td class='col-md-12' colspan='3'>".$periodo_inscricoes[$i]['corrigido']."</td></tr></thead><tbody>";
						}

						echo "<tr>
								<td style='width: 40%'>".$periodo_inscricoes[$i]['nivel']."</td>
								<td style='width: 20%'>".$periodo_inscricoes[$i]['semestre']." de "
									 .$periodo_inscricoes[$i]['ano']."</td>
								<td style='width: 40%'>".$periodo_inscricoes[$i]['dt_inicial']." a ".
									  $periodo_inscricoes[$i]['dt_final']." </td>
							  </tr>";
						$programa = $periodo_inscricoes[$i]['corrigido'];

					}

					echo "</table>";
				?>
			</div>
			<?php require_once('footer.html'); ?>
		</div>
	</body>
</html>