<?php
	// Modificado para apagar carta de recomendacao baseado em id_recomendacao no lugar de path + id
	// (por seguranca, visto que esta funcionalidade estara disponivel tambem para o candidato)

	require_once('db.php');
	session_start();

	global $con;

	$ok = false;

	if(isset($_POST['idr']) && (($recomendacao = carrega_solicitacao_recomendacao($_POST['idr'])) !== false)) {
		$chave_arquivo = getcwd() . sprintf('/posdados/%s/%s/Carta_%s',
			$recomendacao['id_candidato'], $recomendacao['num_inscricao'], $recomendacao['id_carta']);

		// Apaga Carta_XXXX* (incluindo Carta_XXXX.pdf e Carta_XXXX-YYYYY.xfdf)
		foreach(glob($chave_arquivo . '*') as $arquivo) {
			unlink($arquivo);
		}

		if(! file_exists($chave_arquivo . '.pdf')) { // apagou mesmo o arquivo (ou ele jah nao existia)
			if($con->query("delete from cartas where id = '" . $recomendacao['id_carta'] . "'") !== false) {
				$ok = true;
			}
		}
	}

	print($ok ? '1' : '0');
?>
