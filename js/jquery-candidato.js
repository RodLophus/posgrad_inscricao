$(window).bind('load', function(){
    $('.inscricao-info').each(calcula_height);
    $('.conteudo-inscricao').hide();
    verifica_qtd_inscricoes();
});

$(window).on('resize', function(){
    $('.inscricao-info').each(calcula_height);
});

$(document).ready(function(){
    $('#campos-nivel').hide();
    $('#campos-periodo').hide();
    $('#campos-numero-inscricao').hide();
    $('.fp-1').hide();   
    $('#input-num-inscricao').prop('required', false);
    /*$('.upload').css('opacity', 0);*/
    /*$('#aviso-arquivo').hide();*/
    /*$('.input-arquivo').prop('disabled', true);*/
    $('[data-toggle="tooltip"]').tooltip({ trigger: 'hover' }); 
    $('.msg').modal('show');
    $('#h2-nova-inscricao').click(mostra_nova_inscricao);
    $('#select-programa').change(mostra_niveis);
    $('.inscricao-header').click(mostra_inscricao_completa);
    $('.deleta-inscricao').click(deleta_inscricao);
    $('.adiciona-recomendante').click(adiciona_recomendante);
    $('.editar-docs').click(mostra_campos_envio);
    $('.input-arquivo').change(envia_arquivo);
    $('.inscricao-info').each(calcula_height);
    $('.deleta-recomendante').click(deleta_recomendante);
    $('.envia-email-recomendante').click(envia_email_recomendante);
    $('#form-inscricao').submit(cria_inscricao);
    $(document).keyup(verifica_esc);


    $('#select-nivel').change(function(){
        if($('#select-nivel').val() != null){
            var programa = $("#select-programa").val();
            var nivel = $("#select-nivel").val();
            var ano_atual = new Date().getFullYear();
            var ano_seguinte = ano_atual + 1;

            $.ajax({
                type: "POST",
                url: "js-candidato-inscricao.php",
                data: {programa : programa, nivel : nivel},
                cache: false,
                success: function(nivel_semestre_ano){
                    
                    nivel_semestre_ano = JSON.parse(nivel_semestre_ano);

                    if(nivel_semestre_ano['semestre'] == 'escolher') {
                        $('#select-semestre').empty();
                        $('#select-semestre').append('<option value="1º semestre">1º semestre</option>');
                        $('#select-semestre').append('<option value="2º semestre">2º semestre</option>');
                        $('#select-semestre').append('<option value="Verão">Verão</option>');
                    }
                    else {
                        $('#select-semestre').empty();
                        $('#select-semestre').append('<option value="'+nivel_semestre_ano["semestre"]+'" selected>'
                                                     +nivel_semestre_ano["semestre"]+'</option>');
                    }

                    if(nivel_semestre_ano["ano"] == "escolher") {
                        $("#select-ano").empty();
                        $('#select-ano').append('<option value="'+ano_atual+'">'+ano_atual+'</option>');
                        $('#select-ano').append('<option value="'+ano_seguinte+'">'+ano_seguinte+'</option>');
                    }
                    else {
                        $("#select-ano").empty();
                        $('#select-ano').append('<option value="'+nivel_semestre_ano["ano"]+'" selected>'
                                                     +nivel_semestre_ano["ano"]+'</option>');
                    } 

                    if(nivel_semestre_ano["ni"] == 0) {
                        $("#campos-numero-inscricao").fadeOut();
                        $("#input-num-inscricao").prop("required", false);
                                                           
                    }
                    else {
                        $("#campos-numero-inscricao").fadeIn(); 
                        $("#input-num-inscricao").prop("required", true);                                                                                                    
                    }

                    $("#campos-periodo").fadeIn();
                }
            });
        }
        else {
            $("#campos-nivel").hide();
            $("#campos-periodo").hide();
            $("#campos-numero-inscricao").hide();
        }
    }).trigger("change");
});

function verifica_esc(e){
    if(e.which == 27) {
        $('.modal').modal('hide');
    }
}

function cria_inscricao(e){
    e.preventDefault();
    var valores = $(e.target).serializeArray();

    $.ajax({
        type: "POST",
        url: 'js-candidato-criar.php',
        data: {infos:valores},
        cache: false,
        success: function(msg){
            if(msg == 1) location.reload();                

            else {
                var div = "<div class='modal fade msg' role='dialog'>";
                div += "      <div class='modal-dialog modal-sm'>";
                div += "          <div class='modal-content'>";
                div += "              <div class='modal-header erro'>";
                div += "                  <button type='button' class='close' data-dismiss='modal'>&times;</button>";
                div += "                  <h4 class='modal-title'><i class='fa fa-times-circle-o' aria-hidden='true'></i> Erro ao criar inscrição</h4>";
                div += "              </div>";
                div += "              <div class='modal-body' id='corpo-modal'>";
                div += "                  <p>"+msg+"</p>";
                div += "              </div>";
                div += "          </div>";
                div += "      </div>";
                div += "  </div>";                    

                $(div).appendTo('body');
                $('.msg').modal('show');
            }
        }
    });
}             

function mostra_niveis(){
    $('#campos-nivel').fadeIn();
    $("#campos-periodo").hide();    
    $("#campos-numero-inscricao").hide();
    var programa = $("#select-programa").val();

    $.ajax({
        type: "POST",
        url: "js-candidato-inscricao.php",
        data: {programa : programa},
        cache: false,
        success: function(nivel_semestre_ano){
            nivel_semestre_ano = JSON.parse(nivel_semestre_ano);                          

            if(!$.isArray(nivel_semestre_ano["nivel"]) && 
               nivel_semestre_ano["semestre"] != "escolher" && 
               nivel_semestre_ano["ano"] != "escolher") {

                $("#select-nivel").empty();
                $('#select-nivel').append('<option value="'+nivel_semestre_ano["id_nivel"]+'" selected>'
                                             +nivel_semestre_ano["nivel"]+'</option>');

                $("#select-nivel").trigger("change");
            }
            else {
                $("#select-nivel").empty();
                $('#select-nivel').append('<option value="" disabled selected style="display:none">Nivel</option>');

                $.each(nivel_semestre_ano["nivel"], function(chave, valor){
                    $('#select-nivel').append('<option value="'+valor["id"]+'">'+valor["nivel"]+'</option>');
                });
                
            }
        }
    });
}


function deleta_inscricao(){
    var inscricao = $(this).closest('.inscricao').attr('id');
    var id = $('#id_aluno').val();

	div  = '<div class="modal fade" id="modal-inscricao" role="dialog">';
	div += '	<div class="modal-dialog">';
	div += '		<div class="modal-content">';
	div += '			<div class="modal-header duvida">';
	div += '				<h4 class="modal-title"><i class="fa fa-trash" aria-hidden="true"></i> Apagar inscrição</h4>';
	div += '			</div>';
	div += '			<div class="modal-body">';
	div += '				<p>Você tem certeza que deseja apagar esta inscrição?</p>';
	div += '				<p>(Esta ação não poderá ser desfeita e os documentos enviados serão apagados.)</p>';
	div += '			</div>';
	div += '			<div class="modal-footer">';
	div += '				<button type="button" class="btn-del-ins bp-1" data-dismiss="modal">Apagar</button>';
	div += '				<button type="button" class="btn-cancelar bp-3" data-dismiss="modal">Cancelar</button>';
	div += '			</div>';
	div += '		</div>';
	div += '	</div>';
	div += '</div>';

 	$(div).appendTo('body').modal({ keyboard: true });
 	$('.modal').on('shown.bs.modal', function(){ $('.btn-del-ins').focus(); });
 	$('.modal').on('hidden.bs.modal', function(){ $(this).remove(); });
 	$('.btn-del-ins').on('click', function(){
 		 $.ajax({
	        type: 'get',
	        url: 'js-candidato-deletar.php',
	        data: { inscricao: inscricao, id: id },
	        cache: false,
	        success: function(resultado){  
	            resultado = JSON.parse(resultado);

	            div = '#'+ inscricao;
	            if(resultado[1] == true){
	            	$(div).slideUp(400, function(){
            								div  = '<div class="modal fade" id="modal-inscricao" role="dialog">';
											div += '	<div class="modal-dialog">';
											div += '		<div class="modal-content">';
											div += '			<div class="modal-header sucesso">';
											div += '				<button type="button" class="close" data-dismiss="modal">&times;</button>';
											div += '				<h4 class="modal-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Sucesso</h4>';
											div += '			</div>';
											div += '			<div class="modal-body">';
										    div += '				<p>'+resultado[0]+'</p>';
											div += '			</div>';
											div += '		</div>';
											div += '	</div>';
											div += '</div>';
 											$(div).appendTo('body').modal({ keyboard: true });
 											$('.modal').on('shown.bs.modal', function(){ $('.close').focus(); });
 											$('.modal').on('hidden.bs.modal', function(){ $(this).remove(); });
	            						});
	            }
	            else{
	            	div  = '<div class="modal fade" id="modal-inscricao" role="dialog">';
					div += '	<div class="modal-dialog">';
					div += '		<div class="modal-content">';
					div += '			<div class="modal-header erro">';
					div += '				<button type="button" class="close" data-dismiss="modal">&times;</button>';
					div += '				<h4 class="modal-title"><i class="fa fa-times-circle-o" aria-hidden="true"></i> Erro</h4>';
					div += '			</div>';
					div += '			<div class="modal-body">';
				    div += '				<p>'+resultado[0]+'</p>';
					div += '			</div>';
					div += '		</div>';
					div += '	</div>';
					div += '</div>';
					$(div).appendTo('body').modal({ keyboard: true });
					$('.modal').on('shown.bs.modal', function(){ $('.close').focus(); });
					$('.modal').on('hidden.bs.modal', function(){ $(this).remove(); });
	            }
	        }
	    });
 	});
}


function mostra_inscricao_completa(e){
    if( $(e.target).is('.deleta-inscricao') || $(e.target).is('.di-icone') ){
        $(this).closest('.inscricao').find('.conteudo-inscricao').slideDown();
    }else{
        $(this).closest('.inscricao').find('.conteudo-inscricao').slideToggle('swing');
    }
}


function mostra_nova_inscricao(){
    $('.fp-1').toggle('swing');
}


function calcula_height(){
    var info = $(this).closest('.inscricao').find('.inscricao-info');
    var documentos = $(this).closest('.inscricao').find('.inscricao-documentos');

    info.css('height', 'auto');
    documentos.css('height', 'auto');

    if($(window).width() >= 768){
        if(info.outerHeight() < documentos.outerHeight()){
            info.css('height', documentos.outerHeight()+'px');
        }else{
            documentos.css('height', info.outerHeight()+'px');

        }
    }

}


function verifica_qtd_inscricoes(){
    var qtd = $('#qtd_inscricoes').val();

    if(qtd == 0){
        $('#inscricoes').remove();
        $('#apresentacao').html('<h1>Bem-vindo, candidato(a)!</h1>\
                                 <p>Preencha os dados do formulário abaixo para adicionar sua inscrição ao sistema.');
    }else{
        $('#apresentacao').html('<h1>Bem-vindo novamente, candidato(a)!</h1>\
                                 <p>Preencha os dados do formulário abaixo para adicionar uma nova inscrição ao sistema.');
    }

    if(qtd < 3){
        $('.fp-1').show();
        $('.conteudo-inscricao').show();
    }
}


function mostra_campos_envio(){
    $(this).closest('.inscricao').find('.conteudo-inscricao').slideDown();
    $(this).closest('.inscricao').find('.modificacao span').css('margin-right', '0px');
    $(this).closest('.inscricao').find('.upload').css('opacity', 1);
    $(this).closest('.inscricao').find('.input-arquivo').prop('disabled', false);
    $('#aviso-arquivo').fadeIn();
}


function envia_arquivo(){
    var id = $(this).attr('id');

    $(this).closest('.form').find('.img-upload-pdf').attr('src', 'imagens/loading.gif');
    $('#form-upload-'+id).submit();
    $(this).prop('disabled', true);
}

// ---------------------------------------------------------------------

function deleta_recomendante(){
	var linha = $(this).parent().parent();
	var idr = $(this).data('idr');
	var div  = '<div class="modal fade" id="modal-inscricao" role="dialog">';
	div += '	<div class="modal-dialog">';
	div += '		<div class="modal-content">';
	div += '			<div class="modal-header duvida">';
	div += '				<h4 class="modal-title"><i class="fa fa-trash" aria-hidden="true"></i> Apagar recomendante</h4>';
	div += '			</div>';
	div += '			<div class="modal-body">';
	div += '				<p>Você tem certeza que deseja apagar este recomendante?</p>';
	div += '				<p>Esta ação não poderá ser desfeita, e a carta de recomendação associada (caso exista) também será apagada.</p>';
	div += '			</div>';
	div += '			<div class="modal-footer">';
	div += '				<button type="button" class="btn-del-recomendante bp-1" data-dismiss="modal">Apagar</button>';
	div += '				<button type="button" class="btn-cancelar bp-3" data-dismiss="modal">Cancelar</button>';
	div += '			</div>';
	div += '		</div>';
	div += '	</div>';
	div += '</div>';
	$(div).appendTo('body').modal({ keyboard: true });
	$('.modal').on('shown.bs.modal', function(){ $('.btn-cancelar').focus(); });
	$('.modal').on('hidden.bs.modal', function(){ $(this).remove(); });
	$('.btn-del-recomendante').on('click', function(){
		$.ajax({
			type: "POST",
			url: "js-deletar-arquivo.php",
			data: {idr: idr},
			cache: false,
			success: function(resultado){
				if(resultado){
					$('.aviso-recomendantes').show();
					$(linha).remove();
					$('.inscricao-info').each(calcula_height);
					div = "<div class='modal fade msg' role='dialog'>";
					div += "      <div class='modal-dialog modal-sm'>";
					div += "          <div class='modal-content'>";
					div += "              <div class='modal-header sucesso'>";
					div += "                  <button type='button' class='close' data-dismiss='modal'>&times;</button>";
					div += "                  <h4 class='modal-title'><i class='fa fa-check-circle-o' aria-hidden='true'></i> Sucesso</h4>";
					div += "              </div>";
					div += "              <div class='modal-body' id='corpo-modal'>";
					div += "                  <p>Recomendante excluído com sucesso.</p>";
					div += "              </div>";
					div += "          </div>";
					div += "      </div>";
					div += "  </div>";                    
					$(div).appendTo('body').modal('show');
				}else{
					div = "<div class='modal fade msg' role='dialog'>";
					div += "      <div class='modal-dialog modal-sm'>";
					div += "          <div class='modal-content'>";
					div += "              <div class='modal-header erro'>";
					div += "                  <button type='button' class='close' data-dismiss='modal'>&times;</button>";
					div += "                  <h4 class='modal-title'><i class='fa fa-times-circle-o' aria-hidden='true'></i> Erro</h4>";
					div += "              </div>";
					div += "              <div class='modal-body' id='corpo-modal'>";
					div += "                  <p>Não foi possível excluir o recomendante. Por favor, tente novamente.</p>";
					div += "              </div>";
					div += "          </div>";
					div += "      </div>";
					div += "  </div>";                    
					$(div).appendTo('body').modal('show');
				}
			} // sucess
		}); //$ajax()
	}); // on('click'...)
}


function adiciona_recomendante(){
	var inscricao = $(this).closest('.inscricao').attr('id');
	div  = '<div class="modal fade" id="modal-recomendante" role="dialog">';
	div += '	<div class="modal-dialog">';
	div += '		<div class="modal-content">';
	div += '			<div class="modal-header duvida">';
	div += '				<h4 class="modal-title"><i class="fa fa-file-text di-icone" aria-hidden="true"></i>&nbsp;&nbsp;Adicionar recomendante</h4>';
	div += '			</div>';
	div += '			<div class="modal-body">';
	div += '				<form id="form-recomendante" method="post">';
	div += '					<div class="form-group">';
	div += '						<label for="recomendante-nome">Nome: </label>';
	div += '						<input  id="recomendante-nome" type="text" name="nome_recomendante" class="form-control" maxlength="50" required="true"/>';
	div += '					</div>';
	div += '					<div class="form-group">';
	div += '						<label for="recomendante-email">E-mail: </label>';
	div += '						<input  id="recomendante-email" type="email" name="email_recomendante" class="form-control" maxlength="60" required="true"/>';
	div += '					</div>';
	div += '					<p>O sistema irá enviar um e-mail para o endereço indicado, com instruções para que o recomendante encaminhe a Carta de Recomendação.</p>';
	div += '					<input type="hidden" name="num_inscricao" value="' + inscricao + '" />';
	div += '				</form>';
	div += '			</div>';
	div += '			<div class="modal-footer">';
	div += '				<button type="button" id="btn-confirmar-recomendante" class="bp-5" data-loading-text="<i class=\'fa fa-cog fa-spin\'></i> Processando" disabled>Confirmar</button>';
	div += '				<button type="button" id="btn-cancelar-recomendante" class="bp-3" data-dismiss="modal">Cancelar</button>';
	div += '			</div>';
	div += '		</div>';
	div += '	</div>';
	div += '</div>';
	$(div).appendTo('body').modal({ keyboard: true });
	$('.modal').on('shown.bs.modal', function(){ $('#recomendante-nome').focus(); });
	$('.modal').on('hidden.bs.modal', function(){ $(this).remove(); });
	$('#recomendante-nome, #recomendante-email').on('keyup', function(){
		$('#btn-confirmar-recomendante').prop('disabled', 
			!($('#recomendante-nome').val() && $('#recomendante-email').val()));
	});
	$('#btn-confirmar-recomendante').on('click', function(){
		$(this).button('loading');
		$('#form-recomendante').submit();
	});
}


function envia_email_recomendante() {
	var idr = $(this).data('idr');
	$('.inscricao-info').each(calcula_height);
	div = "<div class='modal fade msg' role='dialog'>";
	div += "      <div class='modal-dialog modal-sm'>";
	div += "          <div class='modal-content'>";
	div += "              <div class='modal-header duvida'>";
	div += "                  <button type='button' class='close' data-dismiss='modal'>&times;</button>";
	div += "                  <h4 class='modal-title'><i class='fa fa-gear fa-spin' aria-hidden='true'></i> Processando</h4>";
	div += "              </div>";
	div += "              <div class='modal-body' id='corpo-modal'>";
	div += "                  <p>Enviando e-mail ao recomendante.  Por favor, aguarde.</p>";
	div += "              </div>";
	div += "          </div>";
	div += "      </div>";
	div += "  </div>";                    
	$(div).appendTo('body').modal('show');
	$('.modal').on('hidden.bs.modal', function(){
		$(this).remove();
		// Revisita a pagina (location.reload() nao eh bom aqui porque re-envia POSTDATA)
		window.location.href = window.location.href;
	});
	$('.modal').on('shown.bs.modal', function(){
		$.ajax({
			type: "POST",
			url: "js-candidato-email-recomendante.php",
			data: {idr: idr},
			cache: false,
			success: function(resultado){
				if(resultado == 1){
					div += "<div class='modal-dialog modal-sm'>";
					div += "    <div class='modal-content'>";
					div += "        <div class='modal-header sucesso'>";
					div += "            <button type='button' class='close' data-dismiss='modal'>&times;</button>";
					div += "            <h4 class='modal-title'><i class='fa fa-check-circle-o' aria-hidden='true'></i> Sucesso</h4>";
					div += "        </div>";
					div += "        <div class='modal-body' id='corpo-modal'>";
					div += "            <p>E-mail enviado com sucesso.</p>";
					div += "        </div>";
					div += "    </div>";
					div += "</div>";
				}else{
					div += "<div class='modal-dialog modal-sm'>";
					div += "    <div class='modal-content'>";
					div += "        <div class='modal-header erro'>";
					div += "            <button type='button' class='close' data-dismiss='modal'>&times;</button>";
					div += "            <h4 class='modal-title'><i class='fa fa-times-circle-o' aria-hidden='true'></i> Erro</h4>";
					div += "        </div>";
					div += "        <div class='modal-body' id='corpo-modal'>";
					div += "            <p>" + resultado + "<br/>Se o problema persistir, entre em contato com posgrad.inscricao@ime.unicamp.br.</p>";
					div += "        </div>";
					div += "    </div>";
					div += "</div>";
				}
				$('.modal').html(div);
			}
		});
	});
}
