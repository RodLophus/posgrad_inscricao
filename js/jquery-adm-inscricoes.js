

$(document).ready(function(){ //início do jquery           
    //para que exibamos os calendários nos campos de data no evento focus

    $('#informacoes').on("focus", ".dt_inicial, .dt_final", function() {
        $(this).datepicker({
            dateFormat: 'dd/mm/yy',
            dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
            dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
            dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
            monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
            monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez']
        });
    });    

    $("#informacoes").on("change", ".select_nivel",function(){//ao alterar o select de nível            
        var nivel = $(this).val();
        prog = $(this).closest("tr").find('.select_programa'); //chegamos à mesma linha do select nível para modificar seu respectivo select programa
        prog.empty(); //limpar o select para que, ao adicionar valores, não sejam exibidos os antigos

        $.ajax({
            type: "POST",
            url: "js-adm-inscricoes.php",
            data: {nivel : nivel},
            cache: false,
            success: function(programas){ 
                programas = JSON.parse(programas); //decodificar a variável recebida do php para ser manipulada pelo jquery

                prog.append('<option value="" disabled selected style="display:none">Programa</option>');                            

                $.each(programas, function(chave, valor){
                    prog.append('<option value="'+programas[chave]["id_programa"]+'">'
                                                      +programas[chave]["corrigido"]+'</option>');
                });                        
            }
        });
    });

    $("#informacoes").on("change", ".select_programa", function(){//ao alterar o select de programa    
        var programa = $(this).val();
        var linha = $(this).closest("tr"); //chegar até a linha do select para manipular os seus campos
        var nivel = linha.find('.select_nivel').val();

        $.ajax({
            type: "POST",
            url: "js-adm-inscricoes.php",
            data: {nivel:nivel, programa:programa},
            cache: false,
            success: function(programas){ 
                programas = JSON.parse(programas); //decodificar a variável recebida do php para ser manipulada pelo jquery
                //declarar e manipular os campos da linha do select programa alterado
                var semestre =  linha.find('.select_semestre');
                var ano = linha.find('.select_ano');
                var dt_inicial = linha.find('.dt_inicial');
                var dt_final = linha.find('.dt_final');

                semestre.find('option[value="'+programas["semestre"]+'"]').prop('selected', true);                           
                ano.find('option[value="'+programas["ano"]+'"]').prop('selected', true);                           
                dt_inicial.attr('value', programas["dt_inicial"]);      
                dt_final.attr('value', programas["dt_final"]);      
            }
        });
    });

    $('#adicionar').on('click', function(){  //no clique da opção de alterar mais uma inscrição  
        var linha = $('.linha:last').eq(0).clone(true).insertAfter(".linha:last");   //copia a linha anterior e adiciona na tabela informações            
        linha.find('select').val(''); //muda o valor do select para que não esteja selecionada a mesma opção da linha clonada
        linha.find('input').val(''); //muda o valor do input para que não esteja escrita a mesma data da linha clonada
        linha.find('.select_programa:last').empty(); //apaga as opções do select programa anterior 
        linha.find('.select_programa:last').append('<option value="" disabled selected style="display:none">Programa</option>'); //inclui a opção default

        $("#controle").val(parseInt($("#controle").val())+1);//salva a quantidade de linhas que estão sendo alteradas

        //verifica se estão sendo alterados o mesmo número de campos que existem no banco de dados  
        if($("#controle").val() == $("#controle").attr("class")-1) $("#adicionar").remove(); 
     });   

     $('#alterar_tudo').on('click',  function(){  //no clique da opção para alterar todas as incrições  
        var controle = parseInt($("#controle").val()); //quantidade de linhas já adicionadas           
        var qtd = parseInt($("#controle").attr('class')); //quantidade total de linhas no banco de dados       
        var i;

        for (var i=0;i<=controle;i++) {
            $(".linha").remove(); //apaga todas as linhas para que sejam criadas novamente com os devidos campos já selecionados
        }

        $("#controle").val("0"); //zera a quantidade de linhas já adicionadas
        
        $.ajax({
            type: "POST",
            url: "js-adm-inscricoes.php",
            data: {qtd:qtd},
            cache: false,
            success: function(colunas){ 
                colunas = JSON.parse(colunas); //decodifica o array para ser manipulado em jquery.
                //colunas é um array com todos os campos da linha.

                $.each(colunas, function(chave, valor){ 
                    var linha = $("<tr class='linha'>");
                    linha.append(colunas[chave]); //adiciona as colunas na linha
                    $("#informacoes").append(linha);   //adiciona a linha na tabela
                }); 

            }
        });

        $("#controle").val(qtd); //atualiza o número de linhas a ser alterado
        //esconde os campos que não poderão mais ser utilizados
        $("#adicionar").remove(); 
        $("#alterar_tudo").remove();
    });

});