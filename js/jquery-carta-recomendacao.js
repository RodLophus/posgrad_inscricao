$(document).ready(function() {
	$('#tabela-avaliacao-candidato td').click(ativa_radio_button);
	$('.form-group-dinamico input[type="checkbox"], .form-group-dinamico input[type="radio"]').change(atualiza_campo_dinamico);
	for(var i = 1; i <= 4; i++) $('#cbCandidatoAluno' + i).change(atualiza_candidato_aluno);
	$('#modal-confirma-preenchimento').modal({backdrop: 'static', keyboard: false});
	$('#modal-confirma-preenchimento .btn-default').click(confirma_preenchimento);
});


// Ativa radio button correspondente quando clicar nas celulas da tabela
function ativa_radio_button() {
	$(this).children('.form-check-input').first().prop('checked', true);
}


// Desativa todos os outros checkboxes quando a opcao "Nao se aplica"
// for acionada
function atualiza_candidato_aluno() {
	var id_base = 'cbCandidatoAluno';
	if($(this).prop('id') == id_base + '4') {
		if($(this).prop('checked')) {
			for(var i = 1; i <= 3; i++) {
				$('#' + id_base  + i).prop('checked', false);
			}
		}
	} else {
		$('#' + id_base + '4').prop('checked', false);
	}
}


// Controla campos de texto que sao ativados / desativados dinamicamente
// conforme o estado de outros controles
function atualiza_campo_dinamico() {
	var campo_slave = '#' + $(this).closest('.form-group-dinamico').data('slave-control');
	var sou_master = $(this).is('.campo-master');
	switch($(this).prop('type')) {
		case 'checkbox':
			// Checkbox: campo de texto segue o estado ("checked") do campo master
			if(sou_master) {
				if($(this).prop('checked')) {
					$(campo_slave).removeAttr("disabled");
				} else {
					$(campo_slave).attr("disabled", "disabled");
				}
			}
			break;
		case 'radio':
			// Radio: so gera evento change() para o item que eh clicado
			// (se este for o master, ativa o campo de texto)
			if(sou_master) {
				$(campo_slave).removeAttr("disabled");
			} else {
				$(campo_slave).attr("disabled", "disabled");
			}
			break;
	}
}


function confirma_preenchimento() {
	if($('#rbPreencherCartaNao').prop('checked'))
		window.location.replace(window.location.href + '&action=refuse');
}
