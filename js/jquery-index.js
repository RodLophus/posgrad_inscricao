$(document).ready(function(){
	$('.msg').modal('show');
	$('#form-acesso').css('display', 'none');
	$('#toggle-form-acesso').click(form_toggle);
	$('#adm').click(modal_login_adm);
	$('#form-login-adm').on('submit', login_adm);
	$('#login-adm').on('shown.bs.modal', autofocus_login);
});

function form_toggle(){
	$('#toggle-form-acesso').toggleClass('bp-1-clicado');
	$("#form-acesso").toggle('swing');
}

function modal_login_adm(){
	$('#login-adm').modal('show');
}

function autofocus_login(){
    $('#login').focus();
}

function login_adm(e){
	e.preventDefault();
	
	$('#solicitar-acesso').prop('disabled', true);
	var login = $('#login').val();
	var senha = $('#senha').val();

	if(login == '' && senha == ''){
		$('#erro-login').html('Por favor, digite seu login e senha.');
	
	}else if(login == ''){
		$('#erro-login').html('Por favor, digite seu login.');
	
	}else if(senha == ''){
		$('#erro-login').html('Por favor, digite sua senha.');

	}else{
		$.ajax({
			type: 'POST',
			url: 'js-login.php',
			cache: false,
			data: { login: login, senha: senha },
			success: function(resultado){
				if(resultado == '0'){
					$('#solicitar-acesso').prop('disabled', false);
					$('#erro-login').html('Login ou senha incorretos. Por favor, tente novamente!');

				}else{
					$(location).attr('href', 'administrador.php');

				}

			}
		});
	}
}