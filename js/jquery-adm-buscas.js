$(document).ready(function(){ //início do jquery
	$('tr[data-href]').on('click', abre_inscricao_adm);
    $('.h2-busca').off('click').on('click', exibe_pesquisa);
    //$('#download-arquivo:not([class=download-concluido])').off().on('click', download_arquivo);
    $('.compactar-arquivos').on('click', download_arquivo);
    esconde_pesquisas();
    mostra_nenhum_resultado();
    
    $('#busca-programa').on('change', function exibe_niveis(){ 
    	var programa = $(this).val();

    	$.ajax({
    		type: 'POST',
    		url: 'js-adm-inscricoes.php',
    		data: { programa: programa },
    		cache: false,
    		success: function(niveis){
    			niveis = JSON.parse(niveis); //decodifica o array para formato jquery

    			//$('#select_nivel, #label_nivel').prop("hidden", false);
    			$('#busca-nivel').empty(); //esvazia o campo para que sejam adicionadas opções sem que as antigas apareçam
    			$('#busca-nivel').append('<option value="" selected style="display: none;" disabled>Selecione...</option>');

    			if(niveis.length == 1){
    				$('#busca-nivel').append('<option value="'+niveis[0]["id_nivel"]+'" selected>'+ niveis[0]["descricao"] +'</option>');

    			}else{
        			$.each(niveis, function(chave){//laço de repetição do jquery
        				$('#busca-nivel').append('<option value="'+niveis[chave]["id_nivel"]+'">'+ niveis[chave]["descricao"] +'</option>');
    				});	
    			}
    		}

    	});

    }).trigger("change"); //trigger para que, no caso de somente existir 1 nível, ele fique selecionado e o valor dele seja recuperado.

});

function abre_inscricao_adm(){
	window.open($(this).data('href'));
}

function exibe_pesquisa(){
	$('#'+$(this).attr('name')).toggle('swing');
}

function esconde_pesquisas(){
	if( $('#resultados').is(":visible") ){
		$('#apresentacao-coord h1, form').hide();
		$('#div-resultados p').insertBefore('.qtd-resultados');
	}

	if($(window).width() <= 767){
		$('form').hide();
		$('.h2-busca:last').css('margin-bottom', '30px');
	}
}

function mostra_nenhum_resultado(){
	var controle = $("#controle-resultados").val();

	if(controle == '1'){
		$('.qtd-resultados').css({'text-align': 'center', 'font-size': '12pt', 'font-style': 'normal', 'color': 'black', 'margin-bottom': '25px'});
		$('.qtd-resultados').html('Nenhum resultado encontrado. Tente novamente.');
	}
}

function download_arquivo(){
    var btn = $(this);
    btn.removeClass('compactar-arquivos');
    btn.html('<i class="fa fa-spinner fa-pulse"></i> Compactando arquivos...');
    
    var registros = btn.attr('data-href');
    var programa = btn.attr('data-program').replace(/[ÀÁÂÃÄÅ]/,'A').replace(/[àáâãäå]/,'a').replace(/[ÈÉÊË]/,'E').replace(/[éèêë]/,'e').replace(/[ÍÌÎÏ]/,'I').replace(/[íìîï]/, 'i').replace(/[ÓÒÔÖ]/, 'O').replace(/[óòôö]/, 'o').replace(/[ÚÙÛÜ]/, 'U').replace(/[úùûü]/, 'u').replace(/[Ç]/, 'C').replace(/[ç]/, 'c');
    var semestre = btn.attr('data-period');
    var ano = btn.attr('data-year');

    $.ajax({
        type: 'post',
        url: 'js-adm-download.php',
        data: { registros: registros },
        cache: false,
        success: function(res){
            var hora = $.now();
            btn.attr({ 'href': res, 'class': 'download-concluido bp-5' });

            if(semestre != '') btn.attr({'download': programa+'_'+semestre+'S'+ano+'_'+hora+'.zip'});
            else btn.attr({'download': programa+'_'+ano+'_'+hora+'.zip'});

            btn.html('<i class="fa fa-check" aria-hidden="true"></i> Baixar arquivos');
            btn.off();
            $('.download-concluido')[0].click();
        }
    });
}
