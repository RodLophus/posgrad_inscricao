$(document).ready(function(){
	$("#input-busca").on("keyup", pesquisa);
	
	$('tr[data-href]').on('click', function(){
		window.open($(this).data('href'));
	});
});

function pesquisa() {
	var campo = $("#input-busca").val();
	$("td:not(:contains('"+campo+"'))").parent().css("display", "none");
	$("td:contains('"+campo+"')").parent().css("display", "table-row");

	var qtd = 0;

	$.each($('tbody tr'), function(){
		if ($(this).is(':visible')) qtd++;
	});

	if(qtd>0 && campo != "") {
		$(".nreg").hide();
		$(".nencontrados").show();

		if (qtd==1) $(".nencontrados").html(qtd+" registro encontrado para \""+campo+"\".");			
		else $(".nencontrados").html(qtd+" registros encontrados para \""+campo+"\".");
	}
	else if (qtd==0){
		$(".nreg").hide();
		$(".nencontrados").show();
		$(".nencontrados").html("Nenhum registro encontrado para \""+campo+"\".")
	}
	else {
		$(".nreg").show();
		$(".nencontrados").hide();		
	}
}