$(window).bind('load', function(){
	$('.inscricao-info').each(calcula_height);
	$('.conteudo-inscricao').hide();
	verifica_qtd_inscricoes();
});

$(window).on('resize', function(){
	$('.inscricao-info').each(calcula_height);
});

$(document).ready(function(){
	$('#campos-nivel').hide();
	$('#campos-periodo').hide();
	$('#campos-numero-inscricao').hide();
	$('.fp-1').hide();   
	$('#input-num-inscricao').prop('required', false);
	$('.upload').css('opacity', 0);
	$('#aviso-arquivo').hide();
	$('.input-arquivo').prop('disabled', true);

	$('.msg').modal('show');
	$('#h2-nova-inscricao').click(mostra_nova_inscricao);
	$('#select-programa').change(mostra_niveis);
	$('.inscricao-header').click(mostra_inscricao_completa);
	$('.deleta-inscricao').click(deleta_inscricao);
	$('.editar-docs').click(mostra_campos_envio);
	$('.add-carta').click(mostra_campos_add);
	$('.input-arquivo').change(envia_arquivo);
	$('.input-carta').change(envia_carta);
	$('.inscricao-info').each(calcula_height);
	$('.deleta-arquivo').click(deleta_arquivo);
	$('.b-download-pdf').click(abre_link_pdf);
	$('#form-inscricao').submit(cria_inscricao);
	$(document).keyup(verifica_esc);

	$('#select-nivel').change(function(){      
		if($('#select-nivel').val() != null) {            
			var programa = $("#select-programa").val();
			var nivel = $("#select-nivel").val();
			var ano_atual = new Date().getFullYear();
			var ano_seguinte = ano_atual + 1;

			$.ajax({
				type: "POST",
				url: "js-adm-candidato-inscricao.php",
				data: {programa : programa, nivel : nivel},
				cache: false,
				success: function(nivel_semestre_ano){
					
					nivel_semestre_ano = JSON.parse(nivel_semestre_ano);

					$('#select-semestre').empty();
					$('#select-semestre').append('<option value="'+nivel_semestre_ano["semestre"]+'" >'
												 +nivel_semestre_ano["semestre"]+'</option>');

					$("#select-ano").empty();
					$('#select-ano').append('<option value="'+nivel_semestre_ano["ano"]+'" selected>'
												 +nivel_semestre_ano["ano"]+'</option>');

					if(nivel_semestre_ano["ni"] == 0) {
						$("#campos-numero-inscricao").fadeOut();
						$("#input-num-inscricao").prop("required", false);
														   
					}
					else {
						$("#campos-numero-inscricao").fadeIn(); 
						$("#input-num-inscricao").prop("required", true);                                                                                                    
					}

					$("#campos-periodo").fadeIn();
				}
			});
		}
	}).trigger('change');

});

function verifica_esc(e){
	if(e.which == 27) {
		$('.modal').modal('hide');
	}
}

function cria_inscricao(e){
	e.preventDefault();
	var valores = $(e.target).serializeArray();

	$.ajax({
		type: "POST",
		url: 'js-candidato-criar.php',
		data: {infos:valores},
		cache: false,
		success: function(msg){
			if(msg == 1) location.reload();                

			else {
				var div = "<div class='modal fade msg' role='dialog'>";
				div += "      <div class='modal-dialog modal-sm'>";
				div += "          <div class='modal-content'>";
				div += "              <div class='modal-header erro'>";
				div += "                  <button type='button' class='close' data-dismiss='modal'>&times;</button>";
				div += "                  <h4 class='modal-title'><i class='fa fa-times-circle-o' aria-hidden='true'></i> Erro ao criar inscrição</h4>";
				div += "              </div>";
				div += "              <div class='modal-body' id='corpo-modal'>";
				div += "                  <p>"+msg+"</p>";
				div += "              </div>";
				div += "          </div>";
				div += "      </div>";
				div += "  </div>";                    

				$(div).appendTo('body');
				$('.msg').modal('show');

				
			}
		}
	});             
}

function mostra_niveis(){
	$('#campos-nivel').fadeIn();
	$("#campos-periodo").hide();    
	$("#campos-numero-inscricao").hide();
	var programa = $(this).val();

	$.ajax({
		type: "POST",
		url: "js-adm-candidato-inscricao.php",
		data: {programa : programa},
		cache: false,
		success: function(niveis){
			niveis = JSON.parse(niveis);  
			$("#select-nivel").empty();
			$('#select-nivel').append('<option value="" disabled selected style="display:none">Nivel</option>');
			
			$.each(niveis, function(chave, valor){    
				$('#select-nivel').append('<option value="'+niveis[chave]["id_nivel"]+'" class='+niveis[chave]["ni"]+'>'
											+niveis[chave]["nivel"]+' ('+niveis[chave]["status"]+')</option>');
			});

			$('#campos-nivel').fadeIn();
		}
	});                    
}


/*function deleta_inscricao(){
	var inscricao = $(this).attr('id');
	var id = $('#id_aluno').val();

	if(confirm('Você tem certeza que deseja apagar esta inscrição?\n(Esta ação não poderá ser desfeita e os documentos enviados serão apagados.)')){
		$.ajax({
			type: "GET",
			url: "js-candidato-deletar.php",
			data: {inscricao : inscricao, id : id},
			cache: false,
			success: function(resultado){  
				resultado = JSON.parse(resultado);

				div = '#'+ inscricao;
				if(resultado[1]) $(div).slideUp();

				alert(resultado[0]);
			}
		});
	}
}*/

function deleta_inscricao(){
	var inscricao = $(this).closest('.inscricao').attr('id');
	var id = $('#id_aluno').val();

	div  = '<div class="modal fade" id="modal-inscricao" role="dialog">';
	div += '    <div class="modal-dialog">';
	div += '        <div class="modal-content">';
	div += '            <div class="modal-header duvida">';
	div += '                <h4 class="modal-title"><i class="fa fa-trash" aria-hidden="true"></i> Apagar inscrição</h4>';
	div += '            </div>';
	div += '            <div class="modal-body">';
	div += '                <p>Você tem certeza que deseja apagar esta inscrição?</p>';
	div += '                <p>(Esta ação não poderá ser desfeita e os documentos enviados serão apagados.)</p>';
	div += '            </div>';
	div += '            <div class="modal-footer">';
	div += '                <button type="button" class="btn-del-ins bp-1" data-dismiss="modal">Apagar</button>';
	div += '                <button type="button" class="btn-cancelar bp-3" data-dismiss="modal">Cancelar</button>';
	div += '            </div>';
	div += '        </div>';
	div += '    </div>';
	div += '</div>';

	$(div).appendTo('body').modal({ keyboard: true });
	$('.modal').on('shown.bs.modal', function(){ $('.btn-del-ins').focus(); });
	$('.modal').on('hidden.bs.modal', function(){ $(this).remove(); });
	$('.btn-del-ins').on('click', function(){
		 $.ajax({
			type: 'get',
			url: 'js-candidato-deletar.php',
			data: { inscricao: inscricao, id: id },
			cache: false,
			success: function(resultado){  
				resultado = JSON.parse(resultado);

				div = '#'+ inscricao;
				if(resultado[1] == true){
					$(div).slideUp(400, function(){
											div  = '<div class="modal fade" id="modal-inscricao" role="dialog">';
											div += '    <div class="modal-dialog">';
											div += '        <div class="modal-content">';
											div += '            <div class="modal-header sucesso">';
											div += '                <button type="button" class="close" data-dismiss="modal">&times;</button>';
											div += '                <h4 class="modal-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Sucesso</h4>';
											div += '            </div>';
											div += '            <div class="modal-body">';
											div += '                <p>'+resultado[0]+'</p>';
											div += '            </div>';
											div += '        </div>';
											div += '    </div>';
											div += '</div>';
											$(div).appendTo('body').modal({ keyboard: true });
											$('.modal').on('shown.bs.modal', function(){ $('.close').focus(); });
											$('.modal').on('hidden.bs.modal', function(){ $(this).remove(); });
										});
				}
				else{
					div  = '<div class="modal fade" id="modal-inscricao" role="dialog">';
					div += '    <div class="modal-dialog">';
					div += '        <div class="modal-content">';
					div += '            <div class="modal-header erro">';
					div += '                <button type="button" class="close" data-dismiss="modal">&times;</button>';
					div += '                <h4 class="modal-title"><i class="fa fa-times-circle-o" aria-hidden="true"></i> Erro</h4>';
					div += '            </div>';
					div += '            <div class="modal-body">';
					div += '                <p>'+resultado[0]+'</p>';
					div += '            </div>';
					div += '        </div>';
					div += '    </div>';
					div += '</div>';
					$(div).appendTo('body').modal({ keyboard: true });
					$('.modal').on('shown.bs.modal', function(){ $('.close').focus(); });
					$('.modal').on('hidden.bs.modal', function(){ $(this).remove(); });
				}
			}
		});
	});
}


function mostra_inscricao_completa(e){
	if( $(e.target).is('.botao-opcoes-inscricoes') || $(e.target).is('.inscricoes-icone') || $(e.target).is('.opcao') ){
		$(this).closest('.inscricao').find('.conteudo-inscricao').slideDown();
	}else{
		$(this).closest('.inscricao').find('.conteudo-inscricao').slideToggle('swing');
	}
}


function mostra_nova_inscricao(){
	$(".fp-1").toggle('swing');
}


function calcula_height(){
	var info = $(this).closest('.inscricao').find('.inscricao-info');
	var documentos = $(this).closest('.inscricao').find('.inscricao-documentos');


	info.css('height', 'auto');
	documentos.css('height', 'auto');

	if($(window).width() >= 768){
		if(info.outerHeight(true) < documentos.outerHeight(true))
			info.css('height', documentos.outerHeight(true)+'px');
		
		if(documentos.outerHeight(true) < info.outerHeight(true))
			documentos.css('height', info.outerHeight(true)+'px');
	}
}


function verifica_qtd_inscricoes(){
	var qtd = parseInt($('#qtd_inscricoes').val());

	if(qtd == 0){
		$('#documentos-candidato').remove();
		$('.fp-1').show();
	}

	else if(qtd == 1){
		$('.conteudo-inscricao').show();
	}
}


function mostra_campos_envio(){
	$(this).closest('.inscricao').find('.conteudo-inscricao').slideDown();
	$(this).closest('.inscricao').find('.modificacao span').css('margin-right', '0px');
	$(this).closest('.inscricao').find('.upload').css('opacity', 1);
	$(this).closest('.inscricao').find('.input-arquivo').prop('disabled', false);
	$('#aviso-arquivo').fadeIn();
}

function mostra_campos_add(){
	$(this).closest('.inscricao').find('.conteudo-inscricao').slideDown();
	$(this).closest('.inscricao').find('.modificacao span').css('margin-right', '0px');
	$(this).closest('.inscricao').find('.nenhuma-carta').remove();
	$(this).closest('.inscricao').find('.tr-upload-carta').fadeIn();
	$('.inscricao-info').each(calcula_height);
	$('#aviso-arquivo').fadeIn();
}


function envia_arquivo(){
	var id = $(this).attr('id');

	$(this).closest('.form').find('.img-upload-pdf').attr('src', 'imagens/loading.gif');
	$('#form-upload-'+id).submit();
	$(this).prop('disabled', true);
}

function envia_carta(e){
	if ( $(this).closest('.inscricao').find('#recomendante').val() == '' ){
		$(this).closest('.inscricao').find('.input-carta').val('');
		var div = "<div class='modal fade msg' role='dialog'>";
		div += "      <div class='modal-dialog modal-sm'>";
		div += "          <div class='modal-content'>";
		div += "              <div class='modal-header erro'>";
		div += "                  <button type='button' class='close' data-dismiss='modal'>&times;</button>";
		div += "                  <h4 class='modal-title'><i class='fa fa-times-circle-o' aria-hidden='true'></i> Erro</h4>";
		div += "              </div>";
		div += "              <div class='modal-body' id='corpo-modal'>";
		div += "                  <p>Prrencha o nome do recomendante e selecione o arquivo novamente.</p>";
		div += "              </div>";
		div += "          </div>";
		div += "      </div>";
		div += "  </div>";                    
		$(div).appendTo('body').modal('show');
		
	}else {
		$(this).closest('.botao-upload').find('.img-upload-pdf').attr('src', 'imagens/loading.gif');
		$(this).parent().closest('.inscricao').find('.form-carta').submit();
	}
}

function deleta_arquivo(){
	var linha = $(this).parent();
	var idr = $(this).data('idr');
	$.ajax({
		type: "POST",
		url: "js-deletar-arquivo.php",
		data: {idr: idr},
		cache: false,
		success: function(resultado){
			if(resultado){
				$(linha).remove();
				$('.inscricao-info').each(calcula_height);
				var div = "<div class='modal fade msg' role='dialog'>";
				div += "      <div class='modal-dialog modal-sm'>";
				div += "          <div class='modal-content'>";
				div += "              <div class='modal-header sucesso'>";
				div += "                  <button type='button' class='close' data-dismiss='modal'>&times;</button>";
				div += "                  <h4 class='modal-title'><i class='fa fa-check-circle-o' aria-hidden='true'></i> Sucesso</h4>";
				div += "              </div>";
				div += "              <div class='modal-body' id='corpo-modal'>";
				div += "                  <p>Arquivo excluído com sucesso.</p>";
				div += "              </div>";
				div += "          </div>";
				div += "      </div>";
				div += "  </div>";                    
				$(div).appendTo('body').modal('show');
			}else{
				var div = "<div class='modal fade msg' role='dialog'>";
				div += "      <div class='modal-dialog modal-sm'>";
				div += "          <div class='modal-content'>";
				div += "              <div class='modal-header erro'>";
				div += "                  <button type='button' class='close' data-dismiss='modal'>&times;</button>";
				div += "                  <h4 class='modal-title'><i class='fa fa-times-circle-o' aria-hidden='true'></i> Erro</h4>";
				div += "              </div>";
				div += "              <div class='modal-body' id='corpo-modal'>";
				div += "                  <p>Não foi possível excluir o arquivo. Por favor, tente novamente.</p>";
				div += "              </div>";
				div += "          </div>";
				div += "      </div>";
				div += "  </div>";                    
				$(div).appendTo('body').modal('show');
			}
		}
	});
	
}


function abre_link_pdf(){
	var link = $(this).data('href');
	if (link != '') window.open(link);
	else{
		var div = "<div class='modal fade msg' role='dialog'>";
		div += "      <div class='modal-dialog modal-sm'>";
		div += "          <div class='modal-content'>";
		div += "              <div class='modal-header erro'>";
		div += "                  <button type='button' class='close' data-dismiss='modal'>&times;</button>";
		div += "                  <h4 class='modal-title'><i class='fa fa-times-circle-o' aria-hidden='true'></i> Nenhum arquivo</h4>";
		div += "              </div>";
		div += "              <div class='modal-body' id='corpo-modal'>";
		div += "                  <p>O candidato ainda não enviou os documentos solicitados para esta inscrição.</p>";
		div += "              </div>";
		div += "          </div>";
		div += "      </div>";
		div += "  </div>";                    
		$(div).appendTo('body').modal('show');
	}
}
