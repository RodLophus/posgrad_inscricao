$(document).ready(function(){
	$('#form-emails').submit(function(e){
		e.preventDefault();
		var valores = $(e.target).serializeArray();
		$.each(valores, function(chave, valor){
			if(valor.value != "") {
				$.ajax({
            		type: "POST",
					url: 'js-adm-avaliadores.php',
					data: {login: valor.name, email: valor.value},
					cache: false,
					success: function(infos){
						infos = JSON.parse(infos);
						$('.'+infos[1]).html(infos[0]);
					}
				});				
			}
		});
	});
});