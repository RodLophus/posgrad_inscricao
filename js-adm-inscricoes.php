<?php
    SESSION_START();

    if(isset($_POST['nivel'])) {//caso a variável nível tenha sido passada na chamada do jquery
        if(isset($_SESSION['periodo_inscricoes']))  //caso houve êxito na passagem por sessão da variável periodo_inscricoes, ela é armazenada em uma nova variável
            $periodo_inscricoes = $_SESSION['periodo_inscricoes'] ;
        else {//caso contrário, precisamos setar novamente a variável
            require_once('db.php');
            $periodo_inscricoes = pegar_periodo_inscricoes();
        }

		$nivel = $_POST['nivel'];
		
		if(!isset($_POST['programa'])) {//caso só tenha sido passada a variável nível , a variável programa não estará setada
            $reg = 0;
            //procuramos no vetor periodo_inscricoes, na coluna id_nivel os valores iguais à variável nível, e retornamos apenas as posições das linhas
            $chave = array_keys(array_column($periodo_inscricoes,'id_nivel'),$nivel); 

			foreach($chave as $pos) {
                //salvamos as informações num novo array que obterá somente as linhas desejadas
				$programas[$reg]['id_programa'] = $periodo_inscricoes[$pos]['id_programa']; 
				$programas[$reg]['corrigido'] = $periodo_inscricoes[$pos]['corrigido'];
				$reg++;
			}
		}

		else {//as variáveis nível e programa foram passadas como parâmetro pelo jquery
			$programa = $_POST['programa'];
            //procuramos no vetor periodo_inscricoes, na coluna id_programa os valores iguais à variável nível, e retornamos apenas as posições das linhas
			$chave = array_keys(array_column($periodo_inscricoes,'id_programa'),$programa);

			foreach($chave as $pos) {
				if ($periodo_inscricoes[$pos]['id_nivel'] == $nivel) //caso o nível também seja igual
					$programas = $periodo_inscricoes[$pos]; //adicionamos num novo array que contém apenas as linhas que desejamos
			}
		}

		echo json_encode($programas);//codificamos o array para que possa ser lido no jquery e o retornamos
        return; //interrompe o processamento do arquivo
	}
    elseif(isset($_POST["qtd"])) { //se a chamada do jquery passou como parâmetro apenas a variável qtd
        $periodo_inscricoes = $_SESSION['periodo_inscricoes'] ;        
        $qtd = $_POST["qtd"];

        $ano = date('Y');
        $anoseg = $ano+1;
        $colunas = array();

        for($i=0; $i<$qtd; $i++) {

        //adicionamos as colunas de cada linha
        $colunas[$i] = "
                    <td class='nivel'><select class ='select_nivel campos-padrao-2' name='niveis[]'>
                           <option value='".$periodo_inscricoes[$i]['id_nivel']."'>"
                                .$periodo_inscricoes[$i]['nivel']."</option>
                        </select></td>

                    <td class='programa'><select class ='select_programa campos-padrao-2' name='programas[]'>
                            <option value='".$periodo_inscricoes[$i]['id_programa']."'>"
                                .$periodo_inscricoes[$i]['corrigido']."</option>
                        </select></td>

                    <td class='semestre'><select class='select_semestre campos-padrao-2' name='semestres[]'>
                            <option value='1º semestre'>1º semestre</option>
                            <option value='2º semestre'>2º semestre</option>
                            <option value='Verão'>Verão</option>
                        </select></td>   

                    <td class='ano'><select class ='select_ano campos-padrao-2' name='anos[]'>
                            <option value='".$ano."' >".$ano."</option>
                            <option value='".$anoseg."' >".$anoseg."</option>
                        </select></td>

                    <td class='dt-inicial'><input type='text' class='dt_inicial campos-padrao-2' name='dt_iniciais[]' value='".$periodo_inscricoes[$i]['dt_inicial']."' /></td>
                    <td class='dt-final'><input type='text' class='dt_final campos-padrao-2' name='dt_finais[]' value='".$periodo_inscricoes[$i]['dt_final']."' /></td>
            ";
        }
      
        echo json_encode($colunas); //codificamos o array para que possa ser lido no jquery e o retornamos
        return; //interrompe o processamento do arquivo
    }
    elseif(isset($_POST['programa'])){ //se a página foi chamada pelo jquery com a variável programa sendo passada
        require_once('db.php');
        $niveis = carrega_niveis($_POST['programa']);

        echo json_encode($niveis);//codifica o array de php para um formato que possa ser lido no jquery e o retornamos
    }
?>