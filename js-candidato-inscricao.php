<?php
    SESSION_START();

	if (isset($_POST['nivel'])) $id_nivel = $_POST['nivel']; //significa que 'nivel' foi passado como parâmetro
	else $id_nivel = 0;
	
	$dadosprograma = $_SESSION['periodo_inscricoes'];
	$programa = $_POST['programa'];
	$semestre = "";
	$ano = "";
	$ni = 1;
	
	$chave = array();//vetor que armazenará as posições de $dadosprograma onde śtatus = aberto e id_programa é o desejado. 
	foreach($dadosprograma as $chaves => $valor) { //percorrendo o array
		if($valor["status"] == 'aberto' && $valor["id_programa"] == $programa) //verificando condição
			$chave[] = $chaves; //salvando no array que só contém as informações desejadas
	}

	if ($id_nivel == 0) {//'nivel' não foi passado como parâmetro
		$i = 0;
 
		if (sizeof($chave) == 1) {//se houver apenas uma linha com as condições desejadas
			$nivel_semestre_ano = array("id_nivel" => $dadosprograma[$chave[0]]['id_nivel'], 
										"nivel" => $dadosprograma[$chave[0]]['nivel'], 
										"semestre" => $dadosprograma[$chave[0]]['semestre'], 
										"ano" => $dadosprograma[$chave[0]]['ano']);
			echo json_encode($nivel_semestre_ano); //codifica o array para que possa ser lido no jquery
			return; //interrompe o processamento deste arquivo pois já temos a informação necessária
		}

		foreach($chave as $pos) {//percorrendo todos os índices necessários
			if($dadosprograma[$pos]['status'] == 'aberto') {
				if($id_nivel == 0) {			//a variavel ainda nao está setada
					$id_nivel = array(); 
					$id_nivel[$i]['id'] = $dadosprograma[$pos]['id_nivel'];
					$id_nivel[$i]['nivel'] = $dadosprograma[$pos]['nivel'];
				}

				elseif ($dadosprograma[$pos]['nivel'] != $id_nivel[$i]['nivel']) {	//se ainda não foi salvo aquele nível
					$i++; //índice para andar uma posiçao no vetor
					$id_nivel[$i]["id"] = $dadosprograma[$pos]['id_nivel'];
					$id_nivel[$i]["nivel"] = $dadosprograma[$pos]['nivel'];
				}
			}
		}

		$nivel_semestre_ano = array("nivel" => $id_nivel); //para que possamos deixar algum padrão em comum com o retorno dado quando só existe 1 linha e possamos verificar sem erros no jquery
		echo json_encode($nivel_semestre_ano);//codifica o array para que possa ser lido no jquery
		return; //interrompe o processamento deste arquivo pois já temos a informação necessária
	} 
	else { //então 'nivel' estava setado
		foreach($chave as $pos) {//percorrendo todos os índices necessários
			if ($dadosprograma[$pos]['id_nivel'] == $id_nivel) {//encontrando as linha correspondente ao nivel setado 				

				$semestre = $dadosprograma[$pos]['semestre']; 
				$ano = $dadosprograma[$pos]['ano']; 
				$ni = $dadosprograma[$pos]['b_num_inscricao']; //boolean que indica se o candidato precisa preencher seu número de inscrição ou não
			}
		}
		$nivel_semestre_ano = array("semestre" => $semestre, 
									"ano"      => $ano,
									"ni"	   => $ni);
		echo json_encode($nivel_semestre_ano);//codifica o array para que possa ser lido no jquery
	}

?>