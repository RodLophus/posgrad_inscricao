<?php
	require_once('db.php');
	header('Content-Type: text/html; charset=UTF-8');

	$helpEmail = 'inscricao.posgrad@ime.unicamp.br';

	// Suporte multilingual
	// Para gerar traducoes (ex.: em ingles):
	// - Extrair strings: xgettext --keyword --keyword=t --from-code=utf-8 *.php
	// - Editar arquivo "messages.po", inserindo as traducoes
	// - Gerar arquivo .mo: msgfmt messages.po -o locale/en_US/LC_MESSAGES/messages.mo
	if(isset($_GET['lang'])) {
		$lang = $_GET['lang'];
	} else {
		$lang = explode('_', locale_accept_from_http($_SERVER['HTTP_ACCEPT_LANGUAGE']));
		$lang = $lang[0];
	}
	switch($lang) {
		// setlocale() funciona em alguns sistemas (Apache) e putnev(...) em outros (Nginx)
		case 'en': putenv('LC_ALL=en_US'); setlocale(LC_ALL, 'en_US'); break;
		default:   putenv('LC_ALL=pt_BR'); setlocale(LC_ALL, 'pt_BR'); $lang = 'pt'; break;
	}

	$dominio = "messages";
	bindtextdomain($dominio, 'locale');
	bind_textdomain_codeset($dominio, 'UTF-8');
	textdomain($dominio);

	$errMsg = $okMsg = $avisoMsg = '';
	$errMsgRodape = t('<p>Em caso de dúvidas, entre em contato com a equipe de suporte, através do e-mail: <a href="%s">%s</a>.</p>', "mailto:$helpEmail", $helpEmail);

	$hoje = date('d/m/Y');
	$anoAtual = date('Y');

	// Tenta carregar a solicitacao de recomendacao.
	// Desconsidera IDs numericos por seguranca.  As funcoes subsequentes permitem selecionar cartas pelo 
	// ID da recomendacao (hash) ou pelo ID da carta.  Como este ultimo eh um numero sequencial, um usuario
	// mal intencionado poderia fornecer cartas de recomendacao fraudulentas informando esse numero como ID.
	$idRecomendacao = '';
	if(!(isset($_GET['id']) && (! is_numeric($_GET['id'])) && (($recomendacao = carrega_solicitacao_recomendacao($_GET['id'])) !== false))) {
		$errMsg = t('<p>Não foi possível carregar a solicitação de carta de recomendação.</p><p>Por favor verifique se a URL que você está tentando acessar está correta.</p>');
	} else {

		$idRecomendacao = $_GET['id'];
		$prazoEnvioCarta = strtotime($recomendacao['prazo_inscricao']) + 60*60*24*7;
		$prazoEnvioCartaStr = date($lang == 'en' ? 'F, d Y' : 'd/m/Y', $prazoEnvioCarta ) . ($lang == 'en' ? " (Brazil's official time)" : " (horário de Brasília)");
		$dataAtualizacao = date($lang == 'en' ? 'F, d Y, H:i:s T' : 'd/m/Y, à\s H:i:s\h', strtotime($recomendacao['data_atualizacao']));

		$doutorado = stripos($recomendacao['nivel'], 'doutorado') !== false;

		// Prazo expirado?
		if(mktime(0, 0, 0, date('n'), date('j'), date('Y')) > $prazoEnvioCarta) {
			$errMsg = t('<p>O prazo para encaminhamento desta carta de recomendação expirou em %s.</p>', $prazoEnvioCartaStr);
		} else {

			// A carta jah foi enviada?
			switch($recomendacao['status']) {
				case CR_CARTA_RECEBIDA:
					$avisoMsg = t('<p>Esta carta de recomendação já foi recebida pelo sistema em <strong>%s</strong>.  Caso ela seja enviada novamente, a nova versão irá substituir a anterior.</p>', $dataAtualizacao);
					break;
				case CR_RECOMENDACAO_REJEITADA:
					$errMsg = t('<p>Esta solicitação de carta de recomendação foi rejeitada pelo solicitante em %s, e não pode mais ser modificada.</p>', $dataAtualizacao);
					break;
				default:
					$avisoMsg = t('<p>Você precisa encaminhar esta Carta de Recomendação até o dia <strong>%s</strong>!!</p>', $prazoEnvioCartaStr);
			}

			// O recomendante estah rejeitando o pedido de carta?
			if(isset($_GET['action']) && $_GET['action'] == 'refuse') {
				if(($msg = atualiza_status_recomendacao($idRecomendacao, CR_RECOMENDACAO_REJEITADA)) === true) {
					$okMsg = t('<p>Solicitação de carta de recomendação rejeitada com sucesso.</p>') . $errMsgRodape;
				} else {
					$errMsg = t('<p>Erro gravando resposta à solicitação de carta de recomendação: %s</p>', $msg);
				}
			}

			$recomendanteEmail = $recomendacao['email_recomendante'];
			$recomendanteNome  = $recomendacao['nome_recomendante'];

			if(!empty($_POST)) {
				if(($msg = grava_carta_recomendacao($recomendacao, $_POST, $lang)) === true)
					$okMsg = t('<p>Carta de recomendação encaminhada com sucesso.  Obrigado!!</p>') .
						t('<p>Se desejar, você pode alterar as informações enviadas, submetendo uma nova carta de recomendação através deste sistema até o dia %s.</p>', $prazoEnvioCartaStr) .
						t('<p>Você pode baixar uma cópia da carta de recomendação clicando <a href="%s">aqui</a>.</p>', 'download-carta-recomendacao.php?id=' . $idRecomendacao);
				else
					$errMsg = t('<p>Erro gravando carta de recomendação: %s</p>', $msg);

				// Monta array apenas com as chaves iniciadas por "recomendante"
				$recomendante = array();
				foreach($_POST as $key => $value)
					if(($newKey = preg_replace('/^txRecomendante/', '', $key)) != $key)
						$recomendante[$newKey] = $value;
				if(! empty($recomendante)) grava_dados_recomendante($recomendante);
			}

			// Utiliza prioritariamente a versao do nome do recomendante que foi gravada na
			// tabela de recomendantes (i.e.: permite ao recomendante corrigir o nome)
			if($recomendante = carrega_dados_recomendante($recomendanteEmail))
				if(isset($recomendante['nome']))
					$recomendanteNome = $recomendante['nome'];
		}
	}

?>

<!DOCTYPE html>
<html>

<head>
	<title>Carta de Recomendação</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />
	<link rel="stylesheet" href="css/bootstrap.min.css" />
	<link rel="stylesheet" href="css/estilo.css" />
	<link rel="stylesheet" href="css/carta-recomendacao.css" />
	<script src="js/jquery-1.11.3.js"></script>
	<script src="js/jquery-ui-1.12.0.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery-carta-recomendacao.js"></script>
</head>

<body>
	<div class="container" id="conteudo-principal">

		<nav class="navbar" id="menu">
			<div class="container-fluid">
				<div class="seletor-idioma">
					<a class="link-idioma <?= ($lang != 'pt' ? 'idioma-inativo' : '') ?>" href="?lang=pt&id=<?= $idRecomendacao ?>" data-toggle="tooltip" title="Português"><img src="imagens/pt-br.png"</a>
					<a class="link-idioma <?= ($lang != 'en' ? 'idioma-inativo' : '') ?>" href="?lang=en&id=<?= $idRecomendacao ?>" data-toggle="tooltip" title="English"><img src="imagens/en.png"</a>
				</div>
				<div class="navbar-header">
					<a class="navbar-brand" href="http://www.ime.unicamp.br/"><img src="imagens/logo-imecc-branco.png" id="menu-logo"/> <span id="menu-nome">Instituto de Matemática, Estatística e Computação Científica</span></a>
				</div>
			</div>
		</nav>

		<div id="painel">
			<p><?= t('Pós-Graduação') ?> • <?= t('Carta de Recomendação') ?></p>
		</div>

<?php if($errMsg): ?>

		<div class="alert alert-danger alert-resultado" role="alert">
			<div class="aviso-titulo"><strong><?= t('Erro!') ?></strong></div>
			<div class="aviso-corpo"><?= $errMsg . $errMsgRodape ?></div>
		</div>

<?php elseif($okMsg) : ?>

		<div class="alert alert-success alert-resultado" role="alert">
			<div class="aviso-titulo"><strong><?= t('Concluído!') ?></strong></div>
			<div class="aviso-corpo"><?= $okMsg ?></div>
		</div>

<?php else: ?>

<?php if($recomendacao['status'] != CR_CARTA_RECEBIDA): ?>

		<div class="modal fade" id="modal-confirma-preenchimento" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header duvida">
						<h4 class="modal-title"><i class="fa fa-question-circle" aria-hidden="true"></i> <?= t('Carta de Recomendação Online') ?></h4>
					</div>
					<div class="modal-body">
						<p><?= t('<strong>%s</strong> indicou sua pessoa para recomendá-lo(a) em sua candidatura para o curso de %s (nível: %s) nesta Instituição.', $recomendacao['nome_candidato'], t($recomendacao['programa']), t($recomendacao['nivel'])) ?></p>
						<label><?= t('Você deseja preencher uma Carta de Recomendação online para este(a) candidato(a)?') ?></label>
						<div class="form-check">
							<label class="form-check-label">
								<span class="checkbox-fa"><input type="radio" class="form-check-input" name="rbPreencherCarta" id="rbPreencherCartaSim" value="sim" checked><i class="fa fa-check"></i></span>
								<?= t('SIM. <i>Preencher uma Carta de Recomendação online</i>') ?>
							</label>
						</div>
						<div class="form-check">
							<label class="form-check-label">
								<span class="checkbox-fa"><input type="radio" class="form-check-input" name="rbPreencherCarta" id="rbPreencherCartaNao" value="nao"><i class="fa fa-check"></i></span>
								<?= t('NÃO. <i>Rejeitar o pedido de Carta de Recomendação</i>') ?>
							</label>
						</div>
					</div>
					<div class="modal-footer">
						<span class="aviso-rodape"><?= t('IMPORTANTE: Esta opção não poderá ser alterada depois!') ?></span>
						<button type="button" class="btn btn-default bp-1" data-dismiss="modal"><?= t('Continuar') ?></button>
					</div>
				</div>
			</div>
		</div>

<?php endif; ?>

		<div class="formulario"><form method="post">

			<div class="alert alert-danger aviso" role="alert">
				<div class="aviso-titulo"><strong><?= t('Importante:') ?></strong></div>
				<div class="aviso-corpo"><?= $avisoMsg ?></div>
			</div>

			<input type="hidden" name="txCartaData" value="<?= $hoje ?>">

			<fieldset class="identificacao-candidato">
				<legend><?= t('Identificação do candidato') ?></legend>
				<div class="form-group form-inline">
					<label for="txCandidatoNome"><?= t('Nome do candidato:') ?></label>
					<input type="text" class="campo-fixo" name="txCandidatoNome" id="txCandidatoNome" value="<?= $recomendacao['nome_candidato'] ?>" readonly>
				</div>
				<div class="form-group form-inline">
					<label for="txCandidatoCurso"><?= t('Curso:') ?></label>
					<input type="text" class="campo-fixo" name="txCandidatoCurso" id="txCandidatoCurso" value="<?= t($recomendacao['programa']) ?>" readonly>
				</div>
				<div class="form-group form-inline">
					<label for="txCandidatoNivel"><?= t('Nível:') ?></label>
					<input type="text" class="campo-fixo" name="txCandidatoNivel" id="txCandidatoNivel" value="<?= t($recomendacao['nivel']) ?>" readonly>
				</div>
			</fieldset>

			<fieldset class="form-group">
				<legend><?= t('Informações confidenciais sobre o candidato') ?></legend>
				<div class="form-group form-inline form-long-text">
					<label for="txCandidatoConheco">1. <?= t('Conheço o candidato desde') ?></label>
					<input type="text" class="campos-padrao-1" name="txCandidatoConheco" id="txCandidatoConheco" required="true" maxlength="60">
				</div>

				<div class="form-group form-group-dinamico" data-slave-control="txCandidatoAluno">
					<label>2. <?= t('O candidato foi meu aluno de') ?></label>
					<div class="form-check">
						<label class="form-check-label">
							<span class="checkbox-fa"><input type="checkbox" class="form-check-input" name="cbCandidatoAluno[]" id="cbCandidatoAluno1" value="graduacao" checked><i class="fa fa-check"></i></span>
							<?= t('Graduação') ?>
						</label>
					</div>
					<div class="form-check">
						<label class="form-check-label">
							<span class="checkbox-fa"><input type="checkbox" class="form-check-input" name="cbCandidatoAluno[]" id="cbCandidatoAluno2" value="pos-graduacao"><i class="fa fa-check"></i></span>
							<?= t('Pós-graduação') ?>
						</label>
					</div>
					<div class="form-check form-short-text">
						<label class="form-check-label">
							<span class="checkbox-fa"><input type="checkbox" class="form-check-input campo-master" name="cbCandidatoAluno[]" id="cbCandidatoAluno3" value="outros"><i class="fa fa-check"></i></span>
							<?= t('Outros - especificar:') ?>
						</label>
						<input type="text" class="campos-padrao-1" name="txCandidatoAluno" id="txCandidatoAluno" required="true" maxlength="30" disabled>
					</div>
					<div class="form-check">
						<label class="form-check-label">
							<span class="checkbox-fa"><input type="checkbox" class="form-check-input" name="cbCandidatoAluno[]" id="cbCandidatoAluno4" value="na"><i class="fa fa-check"></i></span>
							<?= t('Não se aplica') ?>
						</label>
					</div>
				</div>

				<div class="form-group form-group-dinamico" data-slave-control="txCandidatoRelacao">
					<label>3. <?= t('Com relação ao candidato, fui (sou) seu') ?></label>
					<div class="form-check">
						<label class="form-check-label">
							<span class="checkbox-fa"><input type="checkbox" class="form-check-input" name="cbCandidatoRelacao[]" id="cbCandidatoRelacao1" value="professor-1-disciplina" checked><i class="fa fa-check"></i></span>
							<?= t('Professor em disciplina') ?>
						</label>
					</div>
					<div class="form-check">
						<label class="form-check-label">
							<span class="checkbox-fa"><input type="checkbox" class="form-check-input" name="cbCandidatoRelacao[]" id="cbCandidatoRelacao2" value="professor-orientador"><i class="fa fa-check"></i></span>
							<?= t('Professor orientador') ?>
						</label>
					</div>
					<div class="form-check">
						<label class="form-check-label">
							<span class="checkbox-fa"><input type="checkbox" class="form-check-input" name="cbCandidatoRelacao[]" id="cbCandidatoRelacao3" value="professor-varias-disciplinas"><i class="fa fa-check"></i></span>
							<?= t('Professor em mais de uma disciplina') ?>
						</label>
					</div>
					<div class="form-check">
						<label class="form-check-label">
							<span class="checkbox-fa"><input type="checkbox" class="form-check-input" name="cbCandidatoRelacao[]" id="cbCandidatoRelacao4" value="chefe-departamento"><i class="fa fa-check"></i></span>
							<?= t('Chefe de departamento') ?>
						</label>
					</div>
					<div class="form-check">
						<label class="form-check-label">
							<span class="checkbox-fa"><input type="checkbox" class="form-check-input" name="cbCandidatoRelacao[]" id="cbCandidatoRelacao5" value="colega"><i class="fa fa-check"></i></span>
							<?= t('Colega') ?>
						</label>
					</div>
					<div class="form-check form-short-text">
						<label class="form-check-label">
							<span class="checkbox-fa"><input type="checkbox" class="form-check-input campo-master" name="cbCandidatoRelacao[]" id="cbCandidatoRelacao6" value="outros"><i class="fa fa-check"></i></span>
							<?= t('Outros - especificar:') ?>
						</label>
						<input type="text" class="campos-padrao-1" name="txCandidatoRelacao" id="txCandidatoRelacao" required="true" maxlength="30" disabled>
					</div>
				</div>

				<div class="form-group">
					<label for="txCandidatoDesempenho">4. <?= t('Como você avalia o desempenho do candidato nas atividades que acompanhou de perto, analisando sua capacidade, dedicação, iniciativa, criatividade e motivação?') ?></label>
					<textarea class="form-control" name="txCandidatoDesempenho" id="txCandidatoDesempenho" rows="2" required="true" maxlength="420"></textarea>
				</div>

				<div class="form-group">
					<label for="txCandidatoExperiencia">5. <?= t('Como você avalia a experiência acadêmica do candidato na área escolhida, até o momento, em termos de conhecimentos teóricos, habilidades e abrangência de formação?') ?></label>
					<textarea class="form-control" name="txCandidatoExperiencia" id="txCandidatoExperiencia" rows="2" required="true" maxlength="420"></textarea>
				</div>

				<div class="form-group">
					<label for="txCandidatoPotencial">6. <?= t('Caso o candidato esteja se inscrevendo ao doutorado, como você avalia o seu potencial para realização de pesquisa original? (por favor procure fundamentar a sua resposta)') ?></label>
					<textarea class="form-control" name="txCandidatoPotencial" id="txCandidatoPotencial" rows="2" required="true" maxlength="420" <?= $doutorado ? '' : 'readonly' ?>><?= $doutorado ? '' : t('(Não se aplica)') ?></textarea>
				</div>

				<div class="form-group form-group-dinamico" data-slave-control="txCandidatoHistorico">
					<label>7. <?= t('O histórico escolar do candidato reflete adequadamente sua capacidade?') ?></label>
					<div class="form-check">
						<label class="form-check-label">
							<span class="checkbox-fa"><input type="radio" class="form-check-input" name="rbCandidatoHistorico" id="rbCandidatoHistorico1" value="sim" checked><i class="fa fa-check"></i></span>
							<?= t('Sim') ?>
						</label>
					</div>
					<div class="form-check">
						<label class="form-check-label">
							<span class="checkbox-fa"><input type="radio" class="form-check-input campo-master" name="rbCandidatoHistorico" id="rbCandidatoHistorico2" value="nao"><i class="fa fa-check"></i></span>
							<?= t('Não (justifique abaixo)') ?>
						</label>
					</div>
					<textarea class="form-control" name="txCandidatoHistorico" id="txCandidatoHistorico" rows="2" required="true" maxlength="420" disabled></textarea>
				</div>

				<div class="form-group">
					<label for="txCandidatoAvancado">8. <?= t('Escreva abaixo sua opinião sobre a adequação e a capacidade do candidato para estudos avançados e pesquisa (na área indicada pelo mesmo), fundamentando-a.') ?></label>
					<textarea class="form-control" name="txCandidatoAvancado" id="txCandidatoAvancado" rows="2" required="true" maxlength="420"></textarea>
				</div>

				<fieldset class="form-group">
					<div class="form-group form-inline">
						<label for="txCandidatoConhecidoAnos">9. <?= t('Comparando com alunos que conheci em') ?></label>
						<input type="number" class="campos-padrao-1" name="txCandidatoConhecidoAnos" id="txCandidatoConhecidoAnos" min="1" max="99" required="true">
						<label for="txCandidatoConhecidoAnos"><?= t('anos, eu classificaria o candidato da seguinte maneira:') ?></label>
					</div>
					<table class="table table-bordered table-striped" id="tabela-avaliacao-candidato">
						<thead>
							<tr>
								<th></th>
								<th><?= t('Entre 1 ou 2% melhores') ?></th>
								<th><?= t('Entre os 5% melhores') ?></th>
								<th><?= t('Entre os 10% melhores') ?></th>
								<th><?= t('Entre os 25% melhores') ?></th>
								<th><?= t('Entre os 50% melhores') ?></th>
								<th><?= t('Entre os 50% piores') ?></th>
								<th><?= t('Sem base para julgar') ?></th>
							</tr>
						</thead>
						<tbody>
							<tr class="form-check">
								<th><?= t('Aproveitamento em disciplinas') ?></th>
								<td><input type="radio" class="form-check-input" name="ckCandidatoAproveitamento" id="ckCandidatoAproveitamento1" value="1"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoAproveitamento" id="ckCandidatoAproveitamento2" value="2"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoAproveitamento" id="ckCandidatoAproveitamento3" value="3"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoAproveitamento" id="ckCandidatoAproveitamento4" value="4" checked><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoAproveitamento" id="ckCandidatoAproveitamento5" value="5"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoAproveitamento" id="ckCandidatoAproveitamento6" value="6"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoAproveitamento" id="ckCandidatoAproveitamento7" value="7"><i class="fa fa-check fa-lg"></i></td>
							</tr>
							<tr class="form-check">
								<th><?= t('Conhecimento científico') ?></th>
								<td><input type="radio" class="form-check-input" name="ckCandidatoConhecimento" id="ckCandidatoConhecimento1" value="1"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoConhecimento" id="ckCandidatoConhecimento2" value="2"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoConhecimento" id="ckCandidatoConhecimento3" value="3"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoConhecimento" id="ckCandidatoConhecimento4" value="4" checked><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoConhecimento" id="ckCandidatoConhecimento5" value="5"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoConhecimento" id="ckCandidatoConhecimento6" value="6"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoConhecimento" id="ckCandidatoConhecimento7" value="7"><i class="fa fa-check fa-lg"></i></td>
							</tr>
							<tr class="form-check">
								<th><?= t('Iniciativa e criatividade') ?></th>
								<td><input type="radio" class="form-check-input" name="ckCandidatoIniciativa" id="ckCandidatoIniciativa1" value="1"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoIniciativa" id="ckCandidatoIniciativa2" value="2"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoIniciativa" id="ckCandidatoIniciativa3" value="3"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoIniciativa" id="ckCandidatoIniciativa4" value="4" checked><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoIniciativa" id="ckCandidatoIniciativa5" value="5"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoIniciativa" id="ckCandidatoIniciativa6" value="6"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoIniciativa" id="ckCandidatoIniciativa7" value="7"><i class="fa fa-check fa-lg"></i></td>
							</tr>
							<tr class="form-check">
								<th><?= t('Perseverança') ?></th>
								<td><input type="radio" class="form-check-input" name="ckCandidatoPerseveranca" id="ckCandidatoPerseveranca1" value="1"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoPerseveranca" id="ckCandidatoPerseveranca2" value="2"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoPerseveranca" id="ckCandidatoPerseveranca3" value="3"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoPerseveranca" id="ckCandidatoPerseveranca4" value="4" checked><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoPerseveranca" id="ckCandidatoPerseveranca5" value="5"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoPerseveranca" id="ckCandidatoPerseveranca6" value="6"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoPerseveranca" id="ckCandidatoPerseveranca7" value="7"><i class="fa fa-check fa-lg"></i></td>
							</tr>
							<tr class="form-check">
								<th><?= t('Capacidade de expressão oral') ?></th>
								<td><input type="radio" class="form-check-input" name="ckCandidatoExpressaoOral" id="ckCandidatoExpressaoOral1" value="1"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoExpressaoOral" id="ckCandidatoExpressaoOral2" value="2"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoExpressaoOral" id="ckCandidatoExpressaoOral3" value="3"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoExpressaoOral" id="ckCandidatoExpressaoOral4" value="4" checked><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoExpressaoOral" id="ckCandidatoExpressaoOral5" value="5"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoExpressaoOral" id="ckCandidatoExpressaoOral6" value="6"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoExpressaoOral" id="ckCandidatoExpressaoOral7" value="7"><i class="fa fa-check fa-lg"></i></td>
							</tr>
							<tr class="form-check">
								<th><?= t('Capacidade de expressão escrita') ?></th>
								<td><input type="radio" class="form-check-input" name="ckCandidatoExpressaoEscrita" id="ckCandidatoExpressaoEscrita1" value="1"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoExpressaoEscrita" id="ckCandidatoExpressaoEscrita2" value="2"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoExpressaoEscrita" id="ckCandidatoExpressaoEscrita3" value="3"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoExpressaoEscrita" id="ckCandidatoExpressaoEscrita4" value="4" checked><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoExpressaoEscrita" id="ckCandidatoExpressaoEscrita5" value="5"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoExpressaoEscrita" id="ckCandidatoExpressaoEscrita6" value="6"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoExpressaoEscrita" id="ckCandidatoExpressaoEscrita7" value="7"><i class="fa fa-check fa-lg"></i></td>
							</tr>
							<tr class="form-check">
								<th><?= t('Capacidade intelectual') ?></th>
								<td><input type="radio" class="form-check-input" name="ckCandidatoCapacidade" id="ckCandidatoCapacidade1" value="1"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoCapacidade" id="ckCandidatoCapacidade2" value="2"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoCapacidade" id="ckCandidatoCapacidade3" value="3"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoCapacidade" id="ckCandidatoCapacidade4" value="4" checked><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoCapacidade" id="ckCandidatoCapacidade5" value="5"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoCapacidade" id="ckCandidatoCapacidade6" value="6"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoCapacidade" id="ckCandidatoCapacidade7" value="7"><i class="fa fa-check fa-lg"></i></td>
							</tr>
							<tr class="form-check">
								<th><?= t('Motivação para estudos avançados') ?></th>
								<td><input type="radio" class="form-check-input" name="ckCandidatoMotivacao" id="ckCandidatoMotivacao1" value="1"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoMotivacao" id="ckCandidatoMotivacao2" value="2"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoMotivacao" id="ckCandidatoMotivacao3" value="3"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoMotivacao" id="ckCandidatoMotivacao4" value="4" checked><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoMotivacao" id="ckCandidatoMotivacao5" value="5"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoMotivacao" id="ckCandidatoMotivacao6" value="6"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoMotivacao" id="ckCandidatoMotivacao7" value="7"><i class="fa fa-check fa-lg"></i></td>
							</tr>
							<tr class="form-check">
								<th><?= t('Capacidade para trabalho individual') ?></th>
								<td><input type="radio" class="form-check-input" name="ckCandidatoIndividual" id="ckCandidatoIndividual1" value="1"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoIndividual" id="ckCandidatoIndividual2" value="2"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoIndividual" id="ckCandidatoIndividual3" value="3"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoIndividual" id="ckCandidatoIndividual4" value="4" checked><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoIndividual" id="ckCandidatoIndividual5" value="5"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoIndividual" id="ckCandidatoIndividual6" value="6"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoIndividual" id="ckCandidatoIndividual7" value="7"><i class="fa fa-check fa-lg"></i></td>
							</tr>
							<tr class="form-check">
								<th><?= t('Avaliação geral') ?></th>
								<td><input type="radio" class="form-check-input" name="ckCandidatoGeral" id="ckCandidatoGeral1" value="1"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoGeral" id="ckCandidatoGeral2" value="2"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoGeral" id="ckCandidatoGeral3" value="3"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoGeral" id="ckCandidatoGeral4" value="4" checked><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoGeral" id="ckCandidatoGeral5" value="5"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoGeral" id="ckCandidatoGeral6" value="6"><i class="fa fa-check fa-lg"></i></td>
								<td><input type="radio" class="form-check-input" name="ckCandidatoGeral" id="ckCandidatoGeral7" value="7"><i class="fa fa-check fa-lg"></i></td>
							</tr>
						</tbody>
					</table>
				</fieldset>

				<div class="form-group form-group-dinamico" data-slave-control="txCandidatoRecomendaria">
					<label>10. <?= t('Você recomendaria a aceitação do candidato em seu próprio programa de pós graduação?') ?></label>
					<div class="form-check">
						<label class="form-check-label">
							<span class="checkbox-fa"><input type="radio" class="form-check-input" name="rbCandidatoRecomendaria" id="rbCandidatoRecomendaria1" value="sem-reservas" checked><i class="fa fa-check"></i></span>
							<?= t('Sim. Sem reservas') ?>
						</label>
					</div>
					<div class="form-check">
						<label class="form-check-label">
							<span class="checkbox-fa"><input type="radio" class="form-check-input campo-master" name="rbCandidatoRecomendaria" id="rbCandidatoRecomendaria2" value="com-reservas"><i class="fa fa-check"></i></span>
							<?= t('Sim, mas com reservas (justifique abaixo)') ?>
						</label>
					</div>
					<div class="form-check">
						<label class="form-check-label">
							<span class="checkbox-fa"><input type="radio" class="form-check-input" name="rbCandidatoRecomendaria" id="rbCandidatoRecomendaria3" value="nao"><i class="fa fa-check"></i></span>
							<?= t('Definitivamente não') ?>
						</label>
					</div>
					<div class="form-check">
						<label class="form-check-label">
							<span class="checkbox-fa"><input type="radio" class="form-check-input" name="rbCandidatoRecomendaria" id="rbCandidatoRecomendaria4" value="sem-curso"><i class="fa fa-check"></i></span>
							<?= t('Não dispomos desse curso') ?>
						</label>
					</div>
					<div class="form-check">
						<label class="form-check-label">
							<span class="checkbox-fa"><input type="radio" class="form-check-input campo-master" name="rbCandidatoRecomendaria" id="rbCandidatoRecomendaria5" value="outros"><i class="fa fa-check"></i></span>
							<?= t('Outros (especifique abaixo)') ?>
						</label>
					</div>
					<textarea class="form-control" name="txCandidatoRecomendaria" id="txCandidatoRecomendaria" rows="2" required="true"  maxlength="420" disabled></textarea>
				</div>

				<div class="form-group">
					<label for="txCandidatoComentarios">11. <?= t('Utilize o espaço abaixo para escrever quaisquer comentários adicionais (opcional).') ?></label>
					<textarea class="form-control" name="txCandidatoComentarios" id="txCandidatoComentarios" rows="15" maxlength="5200"></textarea>
				</div>
			</fieldset>

			<fieldset class="form-group form-horizontal" id="dados-recomendante">
				<legend><?= t('Dados do recomendante') ?></legend>
				<div class="form-group">
					<label for="txRecomendanteNome" class="control-label col-sm-2"><?= t('Nome:') ?></label>
					<span class="col-sm-10"><input type="text" class="campos-padrao-1" name="txRecomendanteNome" id="txRecomendanteNome" maxlength="90" required="true" value="<?= $recomendanteNome ?>"></span>
				</div>
				<div class="form-group">
					<label for="txRecomendanteEmail" class="control-label col-sm-2"><?= t('E-mail:') ?></label>
					<span class="col-sm-10"><input type="email" class="campos-padrao-1" name="txRecomendanteEmail" id="txRecomendanteEmail" maxlength="60" required="true" value="<?= $recomendanteEmail ?>" readonly></span>
				</div>
				<div class="form-group">
					<label for="txRecomendanteInstituicao" class="control-label col-sm-2"><?= t('Instituição ou empresa:') ?></label>
					<span class="col-sm-10"><input type="text" class="campos-padrao-1" name="txRecomendanteInstituicao" id="txRecomendanteInstituicao" maxlength="90" required="true" value="<?= $recomendante ? $recomendante['instituicao'] : '' ?>"></span>
				</div>
				<div class="form-group">
					<label for="txRecomendanteDepartamento" class="control-label col-sm-2"><?= t('Departamento:') ?></label>
					<span class="col-sm-10"><input type="text" class="campos-padrao-1" name="txRecomendanteDepartamento" id="txRecomendanteDepartamento" maxlength="90" required="true" value="<?= $recomendante ? $recomendante['departamento'] : '' ?>"></span>
				</div>
				<div class="form-group">
					<label for="txRecomendanteCargo" class="control-label col-sm-2"><?= t('Cargo:') ?></label>
					<span class="col-sm-10"><input type="text" class="campos-padrao-1" name="txRecomendanteCargo" id="txRecomendanteCargo" maxlength="90" required="true" value="<?= $recomendante ? $recomendante['cargo'] : '' ?>"></span>
				</div>
				<div class="form-group">
					<label for="txRecomendanteGrau" class="control-label col-sm-2"><?= t('Grau acadêmico mais alto:') ?></label>
					<span class="col-sm-10"><input type="text" class="campos-padrao-1" name="txRecomendanteGrau" id="txRecomendanteGrau" maxlength="90" required="true" value="<?= $recomendante ? $recomendante['grau'] : '' ?>"></span>
				</div>
				<div class="form-group">
					<label for="txRecomendanteGrauAno" class="control-label col-sm-2"><?= t('Ano de obtenção do grau:') ?></label>
					<span class="col-sm-10"><input type="number" class="campos-padrao-1" name="txRecomendanteGrauAno" id="txRecomendanteGrauAno" min="<?= $anoAtual - 100 ?>" max="<?= $anoAtual ?>" required="true" value="<?= $recomendante ? $recomendante['ano_grau'] : '' ?>"></span>
				</div>
				<div class="form-group">
					<label for="txRecomendanteGrauInstituicao" class="control-label col-sm-2"><?= t('Instituição em que obteve o grau:') ?></label>
					<span class="col-sm-10"><input type="text" class="campos-padrao-1" name="txRecomendanteGrauInstituicao" id="txRecomendanteGrauInstituicao" maxlength="90" required="true" value="<?= $recomendante ? $recomendante['instituicao_grau'] : '' ?>"></span>
				</div>
				<div class="form-group">
					<label for="txRecomendanteArea" class="control-label col-sm-2"><?= t('Área de sua especialidade:') ?></label>
					<span class="col-sm-10"><input type="text" class="campos-padrao-1" name="txRecomendanteArea" id="txRecomendanteArea" maxlength="90" required="true" value="<?= $recomendante ? $recomendante['area'] : '' ?>"></span>
				</div>
				<div class="form-group">
					<label for="txRecomendanteEndereco" class="control-label col-sm-2"><?= t('Endereço completo:') ?></label>
					<span class="col-sm-10"><input type="text" class="campos-padrao-1" name="txRecomendanteEndereco" id="txRecomendanteEndereco" maxlength="100" required="true" placeholder="Rua, número / Bairro / Cidade / Estado / CEP" value="<?= $recomendante ? $recomendante['endereco'] : '' ?>"></span>
				</div>
				<div class="form-group">
					<label for="txRecomendanteTelefone" class="control-label col-sm-2"><?= t('Telefone:') ?></label>
					<span class="col-sm-10"><input type="text" class="campos-padrao-1" name="txRecomendanteTelefone" id="txRecomendanteTelefone" maxlength="30" required="true" value="<?= $recomendante ? $recomendante['telefone'] : '' ?>"></span>
				</div>
			</fieldset>

			<div class="painel-inferior">
				<button type="submit" class="btn btn-default bp-1"><?= t('Enviar Carta de Recomendação') ?></button>
			</div>

		</form></div>

<?php endif; ?>

		<?php require_once('footer.html'); ?>
	</div>
</body>

</html>
