<nav class="navbar" id="menu">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar" id="botao-menu-responsivo">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span> 
            </button>
            <a class="navbar-brand" href="http://www.ime.unicamp.br/"><img src="imagens/logo-imecc-branco.png" id="menu-logo"/> <span id="menu-nome">Instituto de Matemática, Estatística e Computação Científica</span></a>
        </div>

        <div class="collapse navbar-collapse" id="navbar">
            <ul class="nav navbar-nav navbar-right">
                <li class="menu-item adm"><a href="administrador.php"><span class="adm-nome"><?= $_SESSION['login'] ?></span> <span class="adm-pg"><i class="fa fa-home" aria-hidden="true"></i> Página Inicial</span></a></li><br class="adm-nome"/>
                <li class="menu-item opc"><a href="adm-buscas.php">Buscar inscrições</a></li>
                <?php if($adm){ ?>
                <li class="menu-item opc"><a href="adm-inscricoes.php">Abrir inscrições</a></li>
                <li class="menu-item opc"><a href="adm-avaliadores.php">Gerenciar avaliadores</a></li>
                <li class="menu-item opc"><a href="adm-incompletas.php">Inscrições Incompletas</a></li>
                <!-- <li class="menu-item opc"><a href="administrador.php?lra=1">Limpeza de registros antigos</a></li> -->
                <?php } ?>
                <li class="menu-item opc"><a href="adm-senha.php">Alterar senha</a></li>
                <li class="menu-item"><a href="logout.php">Sair <i class="fa fa-sign-out" aria-hidden="true"></i></a></li>
            </ul>
        </div>
  </div>
</nav>

<script>
 $(document).ready(function(){
    esconde_opcoes();
    $(document).on('resize', esconde_opcoes);

    var url = window.location.pathname;
    var pos = -1;
    var href="";

    $('.menu-adm a').each(function(){
        href = $(this).attr('href');
        pos = url.lastIndexOf(href);
        
        if (pos>0)
            $(this).find('li').attr('class', 'active');
    });

 });

function esconde_opcoes(){
    if($(window).width() >= 768){
        $('.opc').hide();
        $('.adm-pg').hide();
        $('.adm-nome').show();
        $('.fa-sign-out').show();
    }else{
        $('.opc').show();
        $('.adm-pg').show();
        $('.adm-nome').hide();
        $('.fa-sign-out').hide();
    }
 }
</script>

<div id="painel" class="menu-adm">
    <ul>
        <a href="adm-buscas.php"><li class="link1">Buscar inscrições</li></a>
        <?php if($adm){ ?>
        <a href="adm-inscricoes.php"><li class="link2">Abrir inscrições</li></a>
        <a href="adm-avaliadores.php"><li class="link3">Gerenciar avaliadores</li></a>
        <a href="adm-incompletas.php"><li class="link4">Inscrições incompletas</li></a>
        <!-- <a href="administrador.php?lra=1"><li class="link5">Limpeza de registros antigos</li></a> -->
        <?php } ?>
        <a href="adm-senha.php"><li class="link6">Alterar senha</li></a>
    </ul>
</div>     
