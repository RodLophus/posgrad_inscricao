<?php

  header('Content-Type: text/html; charset=utf-8');
  setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
  date_default_timezone_set('America/Sao_Paulo');


  // Status das cartas de recomendacao
  define('CR_RECOMENDANTE_DEFINIDO',  0);
  define('CR_AVISO_ENVIADO',          1);
  define('CR_CARTA_RECEBIDA',         2);
  define('CR_RECOMENDACAO_REJEITADA', 3);

  $con = new mysqli("localhost", "posgrad_db", "PosG@IMECC", "posgrad_inscricao");

  if ($con->connect_errno) die("Falha na conexão: " . $con->connect_error);
  $con->query('SET character_set_results=utf8;');

  //realiza a codificacao para utf-8 e traz informaçoes do banco de dados com acento.
  $res = $con->query("SET NAMES 'utf8';");
  $res = $con->query = 'SET character_set_client=utf8;';

  $salt = "F#y%1*2@"; //complemento para gerar hashs


  //FUNÇÕES INDEX.PHP
  function sql_injection($campo1, $campo2){
      $campo1 = $campo1 . $campo2;

      if(preg_match("(from|select|insert|delete|where|drop|drop table|show tables|'|#|\*|--|\\\\)", $campo1))
        return true;

      return false;
  }

  function login_adm(){
      global $con;

      if(!isset($_SESSION['login']) || !isset($_SESSION['senha'])) return false;

      $res = $con->query("select login, senha from adm where login = '$_SESSION[login]' and senha = '$_SESSION[senha]';");

      if($res->num_rows>0) return true;

      return false;
  }


  // FUNÇÕES DAS PÁGINAS DE ADMINISTRADOR
/*  function excluir_registros_antigos(){
    	global $con;
    	$ano = date("Y") - 5;
      $msg = false;

      $res = $con->query("select id_aluno, num_inscricao from inscricoes where Year(data) < '$ano';");

      if($res->num_rows == 0) $msg = array("Não existem registros antigos a serem excluídos.",'sucesso');
      else {
          while($linha = $res->fetch_array()) {
              $diretorio = getcwd() . "/posdados/". $linha['id_aluno'] . "/" . $linha['num_inscricao'] . "/";
              if(!apagar_arquivos($diretorio)) return array("Não foi possível excluir os arquivos antigos.",'erro');
          }

          $res = $con->query("delete from inscricoes where Year(data) < '$ano';");
      }

      $res = $con->query("delete from aluno where Year(data) < '$ano';");
      if($con->affected_rows>0) $msg = false;

      if($msg === false)
        return array("Registros antigos excluídos com sucesso!", 'sucesso');

      return $msg;
  }*/

  function excluir_registros_antigos(){
      global $con;
      $ano = date("Y") - 6;
      $res = $con->query("select id_aluno, num_inscricao from inscricoes where Year(data) <= '$ano';");

      if($res->num_rows) {
          while($linha = $res->fetch_array()) {
              $diretorio = getcwd() . "/posdados/". $linha['id_aluno'] . "/" . $linha['num_inscricao'] . "/";
              apagar_arquivos($diretorio);
          }

          $res = $con->query("delete from inscricoes where Year(data) <= '$ano';");
      }

      $res = $con->query("select id from aluno where Year(data) <= '$ano';");

      if($res->num_rows) {
	      while($linha = $res->fetch_array()) {
	      		$diretorio = getcwd() . "/posdados/". $linha['id'] . "/";
	      		rmdir($diretorio);
	      }

    		$res = $con->query("delete from aluno where Year(data) <= '$ano';");
      }

  }


  function verifica_adm(){
      if(!isset($_SESSION['login']) || !isset($_SESSION['senha']))
        header('location: index.php');

      elseif($_SESSION['login'] == 'administrador' || $_SESSION['login'] == 'secpos')
        return true;

      return false;
  }

  function permissao_adm(){
      if(!isset($_SESSION['login']) || !isset($_SESSION['senha']))
        header('location: logout.php');

      if(!verifica_adm()) header('location: administrador.php');
  }

  function programa_adm($login){
    global $con;

      $res = $con->query("select ap.id_programa, p.nome, p.corrigido
                        from adm_programa as ap
                        join programa as p on (p.id=ap.id_programa)
                        where ap.login = '".$login."';");

    return $res->fetch_assoc();
  }

  function carrega_programas(){
    	global $con;
    	$res = $con->query("select id, corrigido from programa;");
      $progs = array();

      while ($linha = $res->fetch_array()) $progs[] = $linha;

    	return $progs;
  }

  function busca_periodo($ano, $semestre, $programa, $nivel){
    	global $con;

      $query = "select I.num_inscricao, A.nome, P.id as programa, P.corrigido, N.descricao as nivel, A.id, PI.b_num_inscricao
                from aluno as A
                join inscricoes as I on (A.id = I.id_aluno)
                join nivel as N on (N.id = I.id_nivel)
                join programa as P on (P.id = I.id_programa)
                join periodo_inscricoes as PI on (PI.id_programa = P.id and PI.id_nivel = N.id)
                where I.semestre = '$semestre' and I.ano = '$ano' and P.id = $programa and N.id = $nivel
                order by A.nome asc;";

    	$res = $con->query($query);
      $qtdreg = $res->num_rows;
      $registros = array();

      while($linha = $res->fetch_array()) $registros[] = $linha;
      //$zip = pdf_zip($registros);

      //return array($registros,$qtdreg, $zip);
      return array($registros,$qtdreg);
  }

  function busca_nome($nome, $adm){
    	global $con;

      if ($adm)
        $query = "select I.num_inscricao, A.nome, P.id as programa, P.corrigido, N.descricao as nivel, A.id, PI.b_num_inscricao
                    from aluno as A, inscricoes as I, programa as P, nivel as N, periodo_inscricoes as PI
                    where I.id_aluno = A.id and I.id_programa = P.id and A.nome like '%$nome%'
                     and I.id_nivel = N.id and I.id_programa = PI.id_programa and I.id_nivel = PI.id_nivel
                    order by A.nome asc, I.num_inscricao desc;";

      else {
          $prog = programa_adm($_SESSION['login']);
          $query = "select I.num_inscricao, A.nome, P.id as programa, P.corrigido, N.descricao as nivel, A.id, PI.b_num_inscricao
                      from aluno as A, inscricoes as I, programa as P, nivel as N, periodo_inscricoes as PI
                      where I.id_aluno = A.id and P.id = I.id_programa and A.nome like '%$nome%'
                        and I.id_programa = '".$prog['id_programa']."' and I.id_programa = P.id and I.id_nivel = N.id
                        and I.id_programa = PI.id_programa and I.id_nivel = PI.id_nivel
                      order by A.nome asc, I.num_inscricao desc;";
      }

    	$res = $con->query($query);
    	$qtdreg = $res->num_rows;
      $registros = array();

      while($linha = $res->fetch_array()) $registros[] = $linha;

      return array($registros,$qtdreg);
  }

  function busca_num_inscricao($numero, $adm){
    	global $con;

    	if ($adm)
        $query = "select I.num_inscricao, A.nome, P.id as programa, P.corrigido, N.descricao as nivel, A.id, PI.b_num_inscricao
                    from aluno as A, inscricoes as I, programa as P, nivel as N, periodo_inscricoes as PI
                    where I.id_aluno = A.id and I.id_programa = P.id and I.num_inscricao like '%$numero%'
                     and I.id_nivel = N.id and I.id_programa = PI.id_programa and I.id_nivel = PI.id_nivel
                     and PI.b_num_inscricao NOT IN('0')
                    order by A.nome asc";

      else {
          $prog = programa_adm($_SESSION['login']);
          $query = "select I.num_inscricao, A.nome, P.corrigido, N.descricao as nivel, A.id, PI.b_num_inscricao
                    from aluno as A, inscricoes as I, programa as P, nivel as N, periodo_inscricoes as PI
                    where I.id_aluno = A.id and I.id_programa = P.id and I.num_inscricao like '%$numero%'
                     and I.id_nivel = N.id and I.id_programa = PI.id_programa and I.id_nivel = PI.id_nivel
                     and PI.b_num_inscricao NOT IN('0') and P.id = ".$prog['id_programa']."
                    order by A.nome asc;";
      }

    	$res = $con->query($query);

    	$qtdreg = $res->num_rows;
      $registros = array();

      while($linha = $res->fetch_array()) $registros[] = $linha;

      return array($registros,$qtdreg);
  }

  function pdf_zip($registros){
      $zip = new ZipArchive();
      $tmp = 'posdados/tmp';
      $diretorio = getcwd().'/'.$tmp;

      if(!is_dir($diretorio)) mkdir($diretorio, 0775);
      $caminho = $tmp.'/'.uniqid().'.zip';

      if($zip->open($caminho, ZIPARCHIVE::CREATE) === true){
        foreach($registros as $key => $registro){
          $pdf = pdf_unico($registro['id'], $registro['num_inscricao']);
          if($pdf != false) $zip->addFile(getcwd().'/'.$pdf,
            strtr($registro['nome'], ' áàãâéèẽêíìĩîóòõôúùũûçñÁÀÃÂÉÈẼÊÍÌĨÎÓÒÕÔÚÙŨÛÇÑ',
                                     '_aaaaeeeeiiiioooouuuucnAAAAEEEEIIIIOOOOUUUUCN').'-'.$registro['num_inscricao'].'.pdf');
        }

        $zip->close();
        return $caminho;
      }

      return false;

  }

  function excluir_zip_antigos(){
      $diretorio = 'posdados/tmp';

      if(is_dir(getcwd().'/'.$diretorio)){
        $arquivos = scandir($diretorio);

        foreach($arquivos as $key => $arquivo){
          if(!is_dir($arquivo))
            if($arquivo != '.' && $arquivo != '..')
              if(date('Y/m/d') != date('Y/m/d', filemtime($diretorio.'/'.$arquivo))) unlink($diretorio.'/'.$arquivo);
        }
      }
  }


  //FUNÇÕES DE GERENCIAR-USUARIOS.PHP
  function coordenadores($id_programa = null){
      global $con;

      if($id_programa != null)
        $query = "select n.descricao
                       from adm as a
                        join adm_programa as ap on (a.login = ap.login)
                        join periodo_inscricoes as pi on (pi.id_programa = ap.id_programa)
                        join programa as p on (p.id=ap.id_programa) 
                        join nivel as n on (n.id=pi.id_nivel)
                        where ap.id_programa = $id_programa;";
        else
           $query = "select a.login, a.email, ap.id_programa, p.corrigido
                       from adm as a
                        join adm_programa as ap on (a.login = ap.login)
                        join programa as p on (p.id=ap.id_programa)";

      $res = $con->query($query);

      while($linha = $res->fetch_array()) $adm[] = $linha;

      return $adm;
  }

  function email_adm($login, $email){
    global $con;
    global $salt;

    $nova_senha = uniqid("");
    $valido = valida_email($email);

    if (is_bool($valido)) {
      $confirm_email = email_administrador($email, $login, $nova_senha);
        if (!$confirm_email)
          return array("Erro ao enviar e-mail com nova senha ao coordenador ".$login.".", $login);

          $nova_senha = md5($salt . $nova_senha);
          $res = $con->query("update adm set senha = '$nova_senha', email = '".$email."' where login = '".$login." ';");

          if($con->affected_rows>0)
            return array("Nova senha enviada para ".$login.".",$login);

          return array("Não foi possível inserir nova senha ao coordenador ".$login.".",$login);
      }

      return array($valido ." para ". $login.".", $login);
  }


  // FUNÇÕES DE INCOMPLETOS.PHP
  function incompletos(){
    global $con;
    $i=0;
    $res = $con->query("select * from aluno where nome is null order by data desc;"); 
    $registros = array();

    while($linha = $res->fetch_array()){
        $registros[$i]['email'] = $linha["email"];
        $registros[$i]['data'] =  date("d/m/Y",strtotime($linha['data']));
        $registros[$i]['id'] = $linha["id"];
        $i++;
    }

    return array($registros, $res->num_rows);
 }


  //FUNÇÕES DE SENHA.PHP
  function senha_adm($login, $senha){
      global $con;
      global $salt;

      $senha = md5($salt . $senha);
      $res = $con->query("update adm set senha = '$senha' where login = '$login';");

      if($con->affected_rows>0) return true;//senha alterada

      return false;//senha não alterada
  }


  //FUNÇÕES DE INDEX.PHP
  function pegar_periodo_inscricoes(){
      global $con;
      $qtdreg = 0;

      $res = $con->query("select pi.*, p.nome as programa, p.corrigido, n.descricao as nivel, n.id
                  from periodo_inscricoes as pi 
                  join programa as p on (pi.id_programa = p.id)
                  join nivel as n on (n.id=pi.id_nivel)
                  order by p.corrigido, n.id;");

      while($linha = $res->fetch_array()) {
          $periodo_inscricoes[$qtdreg] = $linha;

          if (data_entre($periodo_inscricoes[$qtdreg]['dt_inicial'], $periodo_inscricoes[$qtdreg]['dt_final']))
              $periodo_inscricoes[$qtdreg]['status'] = "aberto";
          else $periodo_inscricoes[$qtdreg]['status'] = "fechado";

          $qtdreg++;
      }

      return $periodo_inscricoes;
  }

  function data_entre($ini, $fin){
      $atual = date("Y-m-d");

      if($atual >= $ini and $atual <= $fin) return true;

      return false;
  }

  function cadastra_aluno($email){
      global $con;
      global $salt;

      $data = date("Y/m/d");
      $id = md5($email.$data.$salt);

      $res = $con->query ("INSERT INTO aluno (id, email) VALUES ('$id', '$email');");

      if($con->affected_rows>0) return array("E-mail cadastrado!", $id);

      return array("Não foi possível cadastrar o e-mail.", null);
  }


  //FUNÇOES DE EDITAR-ALUNO.PHP
  function mostra_aluno($id, $num_inscricao = '0'){
      global $con;

      if($num_inscricao=='0')
          $query = "select * from aluno where id = '$id';";

      else 
          $query = "select  i.*, a.*, p.corrigido, n.descricao as nivel from inscricoes as i
                      join aluno as a on (a.id=i.id_aluno)
                      join programa as p on (p.id=i.id_programa)
                      join nivel as n on (n.id=i.id_nivel)
                      where id_aluno='$id' and num_inscricao = '$num_inscricao';";

      $res = $con->query($query);

      if($res->num_rows <=0) return false;

      return $res->fetch_assoc();
  }


  //FUNÇÕES EDITAR-INSCRICAO.PHP
  function edita_inscricao($id, $nome, $email, $num_inscricao, $id_programa, $id_nivel, $semestre, $ano){
      global $con;
      $data = date("Y-m-d");

      $query_aluno = "update aluno set nome = '$nome', email = '$email' where  id = '$id';"; 
      $query_inscricao = "insert into inscricoes (num_inscricao, id_aluno, data, id_programa, id_nivel, semestre, ano) 
                            values ('$num_inscricao', '$id', '$data', $id_programa, $id_nivel, '$semestre', $ano)";

      if(!$con->query($query_aluno)){
          email_log($con->error, $id);
          die("A equipe de Informática do IMECC recebeu uma notificação do erro apresentado para verificação.");
      }

      if(!$con->query($query_inscricao)){
          email_log($con->error, $id);
          die($con->error." <br/>Verifique os dados preenchidos.");
      }

      return true;
  }

  function mostra_inscricoes($id, $numero_inscricao = null){
      global $con;

      $aluno = mostra_aluno($id);

      if($numero_inscricao != null)
        $query = "SELECT i.*, p.nome, p.corrigido as programa, n.descricao as nivel, pi.b_num_inscricao, pi.dt_inicial, pi.dt_final
                    from inscricoes as i
                    join programa as p on (p.id=i.id_programa)
                    join nivel as n on (n.id=i.id_nivel)
                    join periodo_inscricoes as pi on (pi.id_programa = i.id_programa and pi.id_nivel = i.id_nivel)
                    where i.id_aluno = '$id' and i.num_inscricao = '$numero_inscricao'";
      else
        $query = "SELECT i.*, p.nome, p.corrigido as programa, n.descricao as nivel, pi.b_num_inscricao, pi.dt_inicial, pi.dt_final
                    from inscricoes as i
                    join programa as p on (p.id=i.id_programa)
                    join nivel as n on (n.id=i.id_nivel)
                    join periodo_inscricoes as pi on (pi.id_programa = i.id_programa and pi.id_nivel = i.id_nivel)
                    where id_aluno = '$id' 
                    order by i.data desc;";

      $res = $con->query($query);

      if($res->num_rows){
          while($linha = $res->fetch_array()) $informacoes[] = $linha;

          return $informacoes;
      }

      return false;
  }

  function mostra_documentos_inscricao($id, $num_inscricao, $id_programa, $id_nivel){
      $diretorio = getcwd(). "/posdados/".$id."/".$num_inscricao."/";
      $arquivos = array();
      $documentos = carrega_documentos($id_programa, $id_nivel);
      $qtddocs = sizeof($documentos);

      if(is_dir($diretorio)){
          $diretorio2 =  "posdados/".$id."/".$num_inscricao."/";

          for($i=0; $i<$qtddocs; $i++){
              $arquivos[$i]['id'] = $documentos[$i]['id'];
              $arquivos[$i]['documento'] = $documentos[$i]['documento'];
              $arquivos[$i]['nome'] = $documentos[$i]['id']."_".$id."_".$documentos[$i]['nome'].'.pdf';

              if (file_exists($diretorio.$arquivos[$i]['nome'])) {
                $arquivos[$i]['modificacao'] = filemtime($diretorio2.$arquivos[$i]['nome']);
                $arquivos[$i]['data_modificacao'] = date("d/m/Y", $arquivos[$i]['modificacao']);
                $arquivos[$i]['hora_modificacao'] = date("H:i:s", $arquivos[$i]['modificacao']);
                $arquivos[$i]['modificacao'] = 'Enviado em '.$arquivos[$i]['data_modificacao'].' às '.$arquivos[$i]['hora_modificacao'];

                $arquivos[$i]['caminho'] = $diretorio2.$arquivos[$i]['nome'];
              }
              else {
                $arquivos[$i]['modificacao'] = "Pendente";
                $arquivos[$i]['caminho'] = " ";
              }
          }
      }else {
          for($i=0; $i<$qtddocs; $i++){
            $arquivos[$i]['id'] = $documentos[$i]['id'];
            $arquivos[$i]['documento'] = $documentos[$i]['documento'];
            $arquivos[$i]['modificacao'] = "Pendente";
            $arquivos[$i]['caminho'] = " ";

          }
      }

      return $arquivos;
  }

  function deleta_inscricao($id, $num_inscricao) {
      global $con;
      $res = $con->query("delete from inscricoes where num_inscricao='$num_inscricao';");
      
      if($con->affected_rows) $msg = "Inscrição deletada com sucesso.";
      else return array("Houve um erro ao deletar a inscrição do aluno: ".$con->error,false);

      $diretorio = getcwd() . "/posdados/". $id . "/" . $num_inscricao . "/";      
      if (!apagar_arquivos($diretorio)) return array($msg." Erro ao excluir arquivos do aluno.",true);

      return array($msg,true);
  }

  function apagar_arquivos($diretorio) {
      if (is_dir($diretorio)) {
        if($aberto = opendir($diretorio)) {
          while ($arquivo=readdir($aberto)) {       
              if ($arquivo!="."&&$arquivo!="..")  unlink($diretorio.$arquivo);        
          }
        }

        if(!rmdir($diretorio)) return false;      
      }
      return true;
  }


  //FUNÇÕES DE EDITAR-DOCUMENTOS-ALUNO.PHP
  function carrega_documentos($programa, $nivel, $id = null){
      global $con;

      if($id == null) {
          $res = $con->query("select d.descricao as documento, d.nome, d.id as id from Documento as d 
                              join Documentos_programa as dp on (d.id=dp.id_documento) 
                              where dp.id_nivel=$nivel and dp.id_programa=$programa;");
          $documentos=array();
          while($linha = $res->fetch_array()) $documentos[] = $linha;
          return $documentos;
      }

      $res = $con->query("select d.descricao as documento, d.nome, d.id as id from Documento as d where d.id = $id;");

      return $res->fetch_assoc();
  }


  //FUNÇÕES DE ABRIR-INSCRIÇÕES.PHP
  function carrega_niveis($programa = null){
      global $con;

      if ($programa != null) $query = "SELECT pi.id_nivel, n.descricao FROM periodo_inscricoes as pi
                                        JOIN nivel AS n ON pi.id_nivel = n.id
                                        WHERE id_programa = $programa";
      else $query = "SELECT * FROM nivel;";

      $res = $con->query($query);

      while($linha = $res->fetch_array()) $niveis[] = $linha;

      return $niveis;
  }

  function abrir_inscricoes($niveis, $programas, $semestres, $anos, $dt_iniciais, $dt_finais){
     global $con;
     $qtd = sizeof($niveis);
     $verifica = false;

     for($i=0;$i<$qtd;$i++) {
       $dt_iniciais[$i] = str_replace('/', '-', $dt_iniciais[$i]);
       $dt_iniciais[$i] = date('Y-m-d', strtotime($dt_iniciais[$i]));
       $dt_finais[$i] = str_replace('/', '-', $dt_finais[$i]);
       $dt_finais[$i] = date('Y-m-d', strtotime($dt_finais[$i]));

       $res = $con->query("update periodo_inscricoes set semestre='".$semestres[$i]."', ano='".$anos[$i]."', 
                             dt_inicial='".$dt_iniciais[$i]."', dt_final='".$dt_finais[$i]."' 
                           where id_programa='".$programas[$i]."' and id_nivel='".$niveis[$i]."';");

       if($con->affected_rows > 0) $verifica = true;
     }

     return $verifica;
 }


  //FUNÇÕES DE ALTERAR-DADOS.PHP
  function valida_arquivo($id_programa, $id_nivel, $arquivo){

     $erro=0;
     /*$ext = strtolower(strrchr($arquivo['name'],'.'));
     if($ext != ".pdf")*/

     if($arquivo['size'] > 0){
//	 if($arquivo['type'] != "application/pdf") return "deve ter formato PDF (" . $arquivo['type'] . ")";
	 if(! preg_match('/application\/.*pdf/', $arquivo['type'])) return "deve ter formato PDF";

         elseif($arquivo['size'] > 3145728)  return "deve ter tamanho máximo de 3MB";
     }
     else return "não enviado";

     return true;
  }

  function insere_arquivo_aluno($id, $num_inscricao, $id_doc){
      global $_FILES;

      $aluno = mostra_aluno($id, $num_inscricao);
      $diretorio = getcwd() . "/posdados/". $id . "/";
      $documentos = carrega_documentos($aluno['id_programa'], $aluno['id_nivel'], $id_doc);

      foreach ($_FILES as $chave => $valor) {
        $valido = valida_arquivo($aluno['id_programa'], $aluno['id_nivel'], $valor); 

        if($valido === true) {
            if (!is_dir($diretorio)) {
              if(!mkdir($diretorio, 0775)) return "Não foi possível criar uma nova pasta com seu número de identificação,
                                                    favor entrar em contato com informatica@ime.unicamp.br para solução do problema.";
            }

            if(!is_dir($diretorio.$num_inscricao)) {
              if(!mkdir($diretorio.$num_inscricao, 0775)) return "Não foi possível criar uma nova pasta com seu número de inscrição,
                                                                    favor entrar em contato com informatica@ime.unicamp.br para solução do problema.";
            }

            $valor['name'] = $documentos['id']."_".$id."_".$documentos['nome'].'.pdf';

            if(is_uploaded_file($valor['tmp_name'])){
              if(!move_uploaded_file($valor['tmp_name'], $diretorio .  $num_inscricao . "/" . $valor["name"]))
                  return "Não foi possível mover o arquivo ".$valor['name']." para a pasta da sua inscrição. Verifique seu envio e, se o erro persistir,
                              entre em contato com informatica@ime.unicamp.br para solução do problema.";

              return array($documentos['documento']." <b>enviado</b>.",true);

            }
        }
        else return $documentos['documento']." <b>".$valido."</b>.";

      }
  }

  /*function insere_arquivo_aluno($id, $num_inscricao){
      global $_FILES;

      $aluno = mostra_aluno($id, $num_inscricao);
      $diretorio = getcwd() . "/posdados/". $id . "/";
      $documentos = carrega_documentos($aluno['id_programa'], $aluno['id_nivel']);
      $i = 0;
      $msg = array();
      $bool = false;

      foreach ($_FILES as $nome => $linha) {

          $valido = valida_arquivo($aluno['id_programa'], $aluno['id_nivel'], $_FILES[$nome]); 

          if($valido === true) {
              if (!is_dir($diretorio)) {
                if(!mkdir($diretorio, 0775)) return "Não foi possível criar uma nova pasta com seu número de identificação, 
                                                      favor entrar em contato com informatica@ime.unicamp.br para solução do problema.";
              }

              if(!is_dir($diretorio.$num_inscricao)) {
                if(!mkdir($diretorio.$num_inscricao, 0775)) return "Não foi possível criar uma nova pasta com seu número de inscrição, 
                                                                      favor entrar em contato com informatica@ime.unicamp.br para solução do problema.";
              }

              $_FILES[$nome]['name'] = $documentos[$i]['id']."_".$id."_".$documentos[$i]['nome'];

              if(is_uploaded_file($_FILES[$nome]['tmp_name'])){
                if(!move_uploaded_file($_FILES[$nome]['tmp_name'], $diretorio .  $num_inscricao . "/" . $_FILES[$nome]["name"]))
                    $msg[] = "Não foi possível mover o arquivo ".$_FILES[$nome]['name']." para a pasta da sua inscrição. Verifique seu envio e, se o erro persistir,
                                entre em contato com informatica@ime.unicamp.br para solução do problema.";
                else {
                  $msg[] = $documentos[$i]['documento']." <b>enviado</b>.";
                  $bool = true;
                }
              }
          }
          else $msg[] = $documentos[$i]['documento']." <b>".$valido."</b>.";

          $i++;
      }

      return array($msg, $bool);
  }*/

  function atualiza_data_aluno($id){
      global $con;
      $res = $con->query("update aluno set data= '".date('Y-m-d H:i:s')."' where id = '".$id."';");

      if($con->affected_rows) return true;

      return false;
  }


    //FUNÇÕES DE EDITAR-ALUNO-ADMINISTRADOR.PHP
  function pdf_unico($id, $num_inscricao){
      $diretorio = getcwd()."/posdados/".$id."/".$num_inscricao."/";
      $diretorio2 = "posdados/".$id."/".$num_inscricao."/";

      if(is_dir($diretorio)){
        $arquivos = "";

        $files = scandir($diretorio2);

        foreach($files as $key => $arquivo){
          if(!is_dir($arquivo)){
            if(preg_match('/\.pdf$/i', $arquivo)){
              if(!strpos($arquivo, "final")) $arquivos .= $diretorio2.$arquivo." ";
            }
          }
        }

        //$comando = "gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=".$diretorio2.$id."final.pdf ".$arquivos;
        $comando = "pdftk $arquivos cat output ".$diretorio2.$id."final.pdf";
        shell_exec($comando);

        return $diretorio2.$id."final.pdf";
      }

      return false;
  }

  /*function insere_cartas_recomendacao($id, $num_inscricao, $recomendante){
      global $_FILES;

      $aluno = mostra_aluno($id, $num_inscricao);
      $diretorio = getcwd() . "/posdados/". $id . "/";
      $cartas = "/cartas";
      $i = 0;
      $msg = array();

      foreach ($_FILES as $nome => $linha) {
          $valido = valida_arquivo($aluno['id_programa'], $aluno['id_nivel'], $linha);

          if($valido === true) {
              if (!is_dir($diretorio)) {
                if(!mkdir($diretorio, 0775)) return "Não foi possível criar uma nova pasta com seu número de identificação,
                                                      favor entrar em contato com informatica@ime.unicamp.br para solução do problema.";
              }

              if(!is_dir($diretorio.$num_inscricao.$cartas)) {
                if(!mkdir($diretorio.$num_inscricao.$cartas, 0775)) return "Não foi possível criar uma nova pasta com seu número de inscrição,
                                                                      favor entrar em contato com informatica@ime.unicamp.br para solução do problema.";
              }

              $_FILES[$nome]['name'] = "Carta de Recomendação - ".$recomendante[$i];

              if(is_uploaded_file($_FILES[$nome]['tmp_name'])){
                if(!move_uploaded_file($_FILES[$nome]['tmp_name'], $diretorio .  $num_inscricao . $cartas . '/' . $_FILES[$nome]['name']))
                    $msg[] = "Não foi possível mover o arquivo ".$_FILES[$nome]['name']." para a pasta da sua inscrição. Verifique seu envio e, se o erro persistir,
                                entre em contato com informatica@ime.unicamp.br para solução do problema.";
                else $msg[] = "Carta do recomendante  ".$recomendante[$i]." enviado com sucesso!";
              }
          }
          else $msg[] = "Carta do recomendante ".$recomendante[$i]." ".$valido;

          $i++;
      }

      if(empty($msg)) return true;
      else return $msg; 
  }*/

  function insere_carta($id, $num_inscricao, $recomendante){
      global $con;
      global $_FILES;

      $aluno = mostra_aluno($id, $num_inscricao);
      $diretorio = getcwd() . "/posdados/". $id . "/";
      $msg = array();

      foreach ($_FILES as $nome => $linha) {
          $valido = valida_arquivo($aluno['id_programa'], $aluno['id_nivel'], $linha);

          if($valido === true) {
              if (!is_dir($diretorio)) {
                if(!mkdir($diretorio, 0775)) return array("Não foi possível criar uma nova pasta com seu número de identificação, 
                                                      favor entrar em contato com informatica@ime.unicamp.br para solução do problema.",'erro');
              }

              $diretorio .= $num_inscricao."/";
              if(!is_dir($diretorio)) {
                if(!mkdir($diretorio, 0775)) return array("Não foi possível criar uma nova pasta com seu número de inscrição, 
                                                        favor entrar em contato com informatica@ime.unicamp.br para solução do problema.",'erro');
              }

              if(is_uploaded_file($_FILES[$nome]['tmp_name'])){
                $status = CR_CARTA_RECEBIDA;
                $add = $con->query("insert into cartas(num_inscricao, recomendante, status) values('$num_inscricao', '$recomendante', '$status')");
                $_FILES[$nome]['name'] = "Carta_".$con->insert_id.'.pdf';

                if($con->affected_rows > 0){
                  if(!move_uploaded_file($_FILES[$nome]['tmp_name'], $diretorio.$_FILES[$nome]['name']))
                      return array("Não foi possível mover ".$_FILES[$nome]['name']." para a pasta da sua inscrição. Verifique seu envio e, se o erro persistir,
                                  entre em contato com informatica@ime.unicamp.br para solução do problema.",'erro');
                  else return array("Carta do recomendante  ".$recomendante." enviado com sucesso!",'sucesso');
                }

                else return array("Não foi possível inserir a carta de recomendante em nosso banco de dados. Verifique seu envio e, se o erro persistir, favor entrar
                            em contato com informatica@ime.unicamp.br para solução do problema", 'erro');

          }
      }
      else return array("Carta do recomendante ".$recomendante." ".$valido,"erro");
    }
  }

	function mostra_cartas_recomendacao($num_inscricao, $seletor = null){
		global $con;
		$query_tag = '';

		if($seletor) $query_tag = ' and '.(is_numeric($seletor) ? 'status' : 'email')." = '$seletor'";

		$res = $con->query("select * from cartas where num_inscricao = '$num_inscricao' $query_tag");

		if($res->num_rows){
			$array = array();

			while($row = $res->fetch_assoc())
				$array[] = $row;

			return $array;
		}

		return false;
	}


  //FUNÇÕES DE EMAIL
  function valida_email($email){
      if (filter_var($email, FILTER_VALIDATE_EMAIL)) 
        return true;
      return "Por favor, digite um e-mail válido";
  }

  function email_administrador($email, $login, $senha){
      $assunto = utf8_decode('Senha para acesso ao sistema de inscrições da Pós Graduação');

      $cabecalhos = "From: inscricao.posgrad@ime.unicamp.br\r\n";
      $cabecalhos .= "Reply-To: inscricao.posgrad@ime.unicamp.br \r\n";
      $cabecalhos .= "MIME-Version: 1.0\r\n";
      $cabecalhos .= "Content-Type: text/html; charset=utf-8\r\n";

      $mensagem = "Prezado avaliador, os seus novos dados para acesso ao sistema de inscrições da Pós Graduação é: \n
      Login : " . $login . "\n
      Senha : " . $senha;

      return mail($email, $assunto, $mensagem, $cabecalhos);
  }

  function verifica_email_bd($email){
      global $con;

      $res = $con->query("SELECT email from aluno where email = '$email'");

      if($res->num_rows > 0) return "E-mail já cadastrado.<br/>";

      return false;
  }

  function envia_email($email, $id = null){
      $assunto = utf8_decode('Sistema de Pós-Graduação do IMECC: Cadastro realizado!');

      $cabecalhos  = "From: inscricao.posgrad@ime.unicamp.br\r\n";
      $cabecalhos .= "Reply-To: inscricao.posgrad@ime.unicamp.br \r\n";
      $cabecalhos .= "MIME-Version: 1.0\r\n";
      $cabecalhos .= "Content-Type: text/html; charset=utf-8\r\n";

      $link = "<a href=\"http://www.ime.unicamp.br/posgrad/inscricao/candidato.php?id=" . $id . "\">
                  http://www.ime.unicamp.br/posgrad/inscricao/candidato.php?id=" . $id ."</a>";

      $mensagem = "Olá candidato(a), <br/><br/><br/>
                  Seu cadastro foi realizado com sucesso.<br/><br/>
                  Acesse o sistema utilizando o link: ".$link." <br/>
                  Seu link de acesso será utilizado para criar e editar suas inscrições durante os próximos 5 anos. <br/><br/><br/>
                  Atenciosamente,<br/>
                  Equipe de Pós-Graduação do IMECC.";

      if($id == null){
        global $con;
          $res = $con->query("select id, email from aluno where email = '$email';");
          $informacoes = $res->fetch_assoc();
          $id = $informacoes['id'];
          $link = "<a href=\"http://www.ime.unicamp.br/posgrad/inscricao/candidato.php?id=" . $id . "\">
                      http://www.ime.unicamp.br/posgrad/inscricao/candidato.php?id=" . $id ."</a>";

          $mensagem = "Olá candidato(a), <br/><br/><br/>
                      Você já estava cadastrado no sistema.<br/><br/>
                      Acesse o sistema utilizando o link: ".$link." <br/>
                      Seu link de acesso será utilizado para criar e editar suas inscrições durante os próximos 5 anos. <br/><br/><br/>
                      Atenciosamente,<br/>
                      Equipe de Pós-Graduação do IMECC.";
      }

      if(mail($email, $assunto, $mensagem, $cabecalhos))
        return "Nós lhe encaminhamos um e-mail contendo o link de acesso ao sistema.";

      else
        return "Erro ao enviar o e-mail de confirmação. Tente novamente e caso o erro persista, entre em contato com os administradores do sistema.";
  }

  function email_de_edicao($email, $id, $num_inscricao){
      $aluno = mostra_aluno($id, $num_inscricao);
      $link = "<a href=\"http://www.ime.unicamp.br/posgrad/inscricao/candidato.php?id=" . $id . "\">http://www.ime.unicamp.br/posgrad/inscricao/candidato.php?id=" . $id ."</a>";// este é o link para que o aluno possa acessar o seu registro e enviar seus arquivos
      $nome = explode(" ", $aluno['nome'],2);

      $assunto = utf8_decode('Sistema de Pós-Graduação do IMECC: Atualização de Inscrição');
      $mensagem = "Olá ".$nome[0].", \n \n
                   Foram realizadas alterações na sua inscrição em <b>".$aluno['nivel']." - ".$aluno['corrigido']."</b>. \n
                   Para visualizar a sua inscrição, acesse o link: \n" . $link.".\n\n
                   Atenciosamente,\n
                   Equipe de Pós-Graduação do IMECC.";

      $cabecalhos  = "From: inscricao.posgrad@ime.unicamp.br\r\n";
      $cabecalhos .= "Reply-To: inscricao.posgrad@ime.unicamp.br \r\n";
      $cabecalhos .= "MIME-Version: 1.0\r\n";
      $cabecalhos .= "Content-Type: text/html; charset=utf-8\r\n";

      return mail($email, $assunto, $mensagem, $cabecalhos);
  }

  function email_log($erro, $id){

      $assunto = utf8_decode('Aviso de erro: Pós-Graduação');

      $cabecalhos = "From: webmaster@ime.unicamp.br\r\n";
      $cabecalhos .= "Reply-To: webmaster@ime.unicamp.br\r\n";

      $cabecalhos .= "MIME-Version: 1.0\r\n";
      $cabecalhos .= "Content-Type: text/html; charset=utf-8\r\n";

      $mensagem = "Aviso, o " . $id . " gerou o seguinte erro: " . $erro;


      mail("webmaster@ime.unicamp.br", $assunto, $mensagem, $cabecalhos);
  }

//------------------------------------------------------------------------


// Retorna a quantidade de cartas de recomendacao necessarias para um
// determinado programa e nivel
function qtde_recomendantes($programa, $nivel) {
	global $con;

	$res = $con->query("select qtde_cartas from cartas_programa where id_programa = $programa and id_nivel = $nivel");

	if($row = $res->fetch_row())
		return $row[0];

	return 0;
}



// Cadastra o pedido de carta de recomendacao no banco de dados e envia o e-mail
// solicitando a carta ao recomendante
function insere_recomendante($num_inscricao, $nome_recomendante, $email_recomendante) {
	global $con;

	// O "x" eh para ter certeza de que o ID da recomendacao sera sempre alfanumerico.
	// Outras funcoes usarao esse fato para decidir se uma busca deve ser feita
	// pelo ID da recomendacao (alfanumerico) ou pelo ID da carta (numerico)
	$id_recomendacao = 'x' . md5($num_inscricao . $email_recomendante . uniqid());

	if(! $nome_recomendante)
		return array('Nome do recomendante não informado.', false);

	if(($res = valida_email($email_recomendante)) !== true)
		return array($res, false);

	if($res = mostra_cartas_recomendacao($num_inscricao, $email_recomendante))
		return array('Você já indicou um recomendante com esse endereço de e-mail.', false);

	$status = CR_RECOMENDANTE_DEFINIDO;

	$res = $con->query("insert into cartas (num_inscricao, recomendante, email, id_recomendacao, status) values ('$num_inscricao', '$nome_recomendante', '$email_recomendante', '$id_recomendacao', '$status')");

	if($res === false)
		// Erro: retorna mensagem de erro e "false"
		return array($con->error, false);

	// Le de volta do banco de dados, para obter o ID da carta
	if(($cartas = mostra_cartas_recomendacao($num_inscricao, $email_recomendante)) === false)
		// Isto nunca deve acontecer...
		return array('Erro procurando carta de recomendação no banco de dados.  Por favor tente novamente.', false);

	if($res = envia_email_recomendante($cartas[0]['id']) !== true)
		return array($res, false);

	// Ok: retorna ID da carta inserida e "true"
	return array($cartas[0]['id'], true);
}



// Envia o e-mail solicitando a carta de recomendacao ao recomendante
// Atualiza o status da carta no banco de dados conseguir enviar o e-mail ok
function envia_email_recomendante($id) {
	global $con;

	$dados = carrega_solicitacao_recomendacao($id);
	$prazoEnvioCarta = strtotime($dados['prazo_inscricao']) + 60*60*24*7;

	// envia o email
	$url = 'http://www.ime.unicamp.br/posgrad/inscricao/carta-recomendacao.php?id=' . $dados['id_recomendacao'];

	$assunto = '=?UTF-8?B?'.base64_encode('Carta de Recomendação para Unicamp / Recommendation Letter for Unicamp').'?=';

	$cabecalhos  = "From: \"=?UTF-8?B?" . base64_encode('Inscrições da Pós-graduação - IMECC / Unicamp') . "?=\"<inscricao.posgrad@ime.unicamp.br>\r\n";
	$cabecalhos .= "Reply-To: inscricao.posgrad@ime.unicamp.br \r\n";
	$cabecalhos .= "MIME-Version: 1.0\r\n";
	$cabecalhos .= "Content-Type: text/html; charset=utf-8\r\n";

	$mensagem  = "<i>(english version: at the end of this message)</i><br><br>\n";
	$mensagem .= "<br>Prezado(a) <strong>" . $dados['nome_recomendante'] . "</strong>:<br><br>\n";
	$mensagem .= "<strong>" . $dados['nome_candidato'] . "</strong>, candidato ao curso de " . $dados['programa']. " (nível: " . $dados['nivel'] . ")<br>\n";
	$mensagem .= "indicou sua pessoa para recomendá-lo(a) em sua candidatura para esta Instituição.<br><br>\n";
	$mensagem .= "Caso esteja de acordo, <strong>por favor acesse o link abaixo até o dia " . date('d/m/Y', $prazoEnvioCarta) . "</strong> (horário de Brasília),<br>\n";
	$mensagem .= "para preencher a Carta de Recomendação Online:<br><br>\n";
	$mensagem .= "<a href=\"$url\">$url</a><br><br>\n";
	$mensagem .= "Em caso de dúvidas ou problemas, por favor nos contacte respondendo este e-mail.<br><br>\n";
	$mensagem .= "Desde já, agradecemos a sua atenção.<br><br>\n";
	$mensagem .= "Atenciosamente,<br><br>\n";
	$mensagem .= "Comissão de Pós-graduação<br>\n";
	$mensagem .= "Instituto de Matemática, Estatística e Computação Científica<br>\n";
	$mensagem .= "Universidade Estadual de Campinas<br>\n";
	$mensagem .=  "<br><hr><br>";
	$mensagem .= "<i>Dear <strong>" . $dados['nome_recomendante'] . "</strong>:<br><br>\n";
	$mensagem .= "<strong>" . $dados['nome_candidato'] . "</strong> pointed you as a recommender for his/her application at our Institution.<br><br>\n";
	$mensagem .= "If you agree, please follow the link below <strong>at or before " . date('F, d Y', $prazoEnvioCarta). "</strong> (Brazil's official time) to fill in an Online Recommendation Form:<br><br>\n";
	$mensagem .= "<a href=\"$url\">$url</a><br><br>\n";
	$mensagem .= "If you have any problems or questions, please contact us by replying this email.<br><br>\n";
	$mensagem .= "Thank you in advance for your cooperation.<br><br>\n";
	$mensagem .= "Best regards,<br><br>\n";
	$mensagem .= "Graduation Office<br>\n";
	$mensagem .= "Institute of Mathematics, Statistics and Scientific Computation<br>\n";
	$mensagem .= "State Univeristy of Campinas - Brazil<br></i>\n";

	if(! mail($dados['email_recomendante'], $assunto, $mensagem, $cabecalhos))
		return 'Erro ao enviar e-mail. Por favor tente novamente mais tarde.';

	$status = CR_AVISO_ENVIADO;
	if($con->query("update cartas set status = '$status', data = now() where id = '" . $dados['id_carta'] . "'") === false)
		return $con->error;

	return true;
}



// Salva os dados pessoais do recomendante
function grava_dados_recomendante($dados) {
	global $con;

	$con->query("delete from recomendantes where email = '" . $dados['Email'] . "'");

	$con->query('insert into recomendantes (email, nome, instituicao, departamento, cargo, grau, ano_grau, instituicao_grau, area, telefone, endereco, data) values (' .
		implode(',', array_map(function($a) use($dados) { return "'" . $dados[$a] ."'"; },
			array('Email', 'Nome', 'Instituicao', 'Departamento', 'Cargo', 'Grau', 'GrauAno', 'GrauInstituicao', 'Area', 'Telefone', 'Endereco'))) . ', now())');
}



// Carrega os dados pessoais do recomendante
function carrega_dados_recomendante($email) {
	global $con;

	if(($res = $con->query("select * from recomendantes where email = '$email'")) !== false)
		return ($result = $res->fetch_assoc()) !== null ? $result : false;

	return false;
}



// Carrega dados relacionados aa solicitacao de carta de recomendacao.
// $id pode ser o ID da carta (numero sequencial) ou o ID da recomendacao
// ("hash" aleatorio, usado pelo recomendante para acessar a pagina da 
// ?carta de recomendacao)
function carrega_solicitacao_recomendacao($id) {
	global $con;

	$query = "select inscricoes.num_inscricao, " .
		"cartas.id as id_carta, " .
		"cartas.id_recomendacao, " .
		"cartas.recomendante as nome_recomendante, " .
		"cartas.email as email_recomendante, " .
		"cartas.status, " .
		"cartas.data as data_atualizacao, " .
		"aluno.nome as nome_candidato, " .
		"aluno.id as id_candidato, " .
		"programa.corrigido as programa, " .
		"nivel.descricao as nivel, " .
		"periodo_inscricoes.dt_final as prazo_inscricao " .
			"from inscricoes " .
				"join cartas on (cartas.num_inscricao = inscricoes.num_inscricao)  " .
				"join aluno on (aluno.id = inscricoes.id_aluno)  " .
				"join programa on (programa.id = inscricoes.id_programa)  " .
				"join nivel on (nivel.id = inscricoes.id_nivel)  " .
				"join periodo_inscricoes on (periodo_inscricoes.id_programa = inscricoes.id_programa  " .
					"and periodo_inscricoes.id_nivel = inscricoes.id_nivel) " .
				(is_numeric($id) ? "where cartas.id = '$id'" : "where cartas.id_recomendacao = '$id'");

	if(($res = $con->query($query)) === false)
		return false;

	return ($result = $res->fetch_assoc()) !== null ? $result : false;
}



// Preenche o template PDF da carta de recomendacao com os dados fornecidos
// pelo recomendante e grava o arquivo usado para o preenchimento (.xfdf)
// e a carta preenchida (.pdf) no diretorio do candidato
function grava_carta_recomendacao($recomendacao, $dados, $lang) {
	global $con;

	$modelo_pdf = getcwd() . '/etc/carta-recomendacao_' . $lang . '.pdf';

	$diretorio = getcwd() . '/posdados/' . $recomendacao['id_candidato'] . '/' . $recomendacao['num_inscricao'];
	$arquivo_base = $diretorio . '/Carta_' . $recomendacao['id_carta'];

	$arquivo_pdf  = $arquivo_base . '.pdf';

	// Por seguranca, mantem-se todas as versoes do XFDF da carta de recomendacao
	// (serah gerado PDF apenas da versao mais recente)
	$arquivo_xfdf = $arquivo_base . '-' . strftime('%Y%m%d%H%M%S') . '.xfdf';

	if(! is_dir($diretorio))
		if(! mkdir($diretorio, 0775, true))
			return 'Erro criando diretorio';

	$raiz = '<xfdf xmlns="http://ns.adobe.com/xfdf" xml:space="preserve"></xfdf>';
	$xfdf = new SimpleXMLElement($raiz);
	$fields = $xfdf->addChild('fields');

	foreach($dados as $chave => $valor) {
		switch(substr($chave, 0, 2)) {
			case 'cb': // Check Boxes
				foreach($valor as $item) {
					$field = $fields->addChild('field');
					$field->addAttribute('name', $chave . '_' . $item);
					$field->addChild('value', 'Yes');
				}
				continue 2; // continua o foreach()
				break;
			case 'rb': // Radio Buttons
				$chave .= '_' . $valor;
				$valor = 'Yes';
				break;
			case 'ck': // Check marks, usados na tabela
				$chave .= '_' . $valor;
				$valor = 'a'; // Check mark em fonte webdings
				break;
		}
		$field = $fields->addChild('field');
		$field->addAttribute('name', $chave);
		$field->addChild('value', $valor);
	}

	// Formata o arquivo XFDF com indentacoes, quebras de linha e trata acentos
	$dom = new DOMDocument('1.0');
	$dom->preserveWhiteSpace = false;
	$dom->formatOutput = true;

	if(! $dom->loadXML($xfdf->asXML()))
		return 'Erro ao consolidar dados.';

	if(! $dom->save($arquivo_xfdf))
		return 'Erro ao salvar arquivo.';

	exec("pdftk $modelo_pdf fill_form $arquivo_xfdf output $arquivo_pdf flatten");

	// Atualiza status e data de envio da carta no BD
	return atualiza_status_recomendacao($recomendacao['id_carta'], CR_CARTA_RECEBIDA);

	//$status = CR_CARTA_RECEBIDA;
	//if($con->query("update cartas set status = '$status', data = now() where id = '" . $recomendacao['id_carta'] . "'") === false)
	//	return $con->error;
	//return true;
}



// Atualiza o status da solicitacao de recomendacao no banco de dados.
// $id pode ser o ID da carta (numero sequencial) ou o ID da recomendacao
// ("hash" aleatorio, usado pelo recomendante para acessar a pagina da 
// ?carta de recomendacao)
function atualiza_status_recomendacao($id, $status) {
	global $con;

	if($con->query("update cartas set status = '$status', data = now() " .
		(is_numeric($id) ? "where id = '$id'" : "where id_recomendacao = '$id'")) === false)
			return $con->error;

	return true;
}



// Retorna uma lista com anos para os quais existem inscricoes
function lista_anos_inscricoes() {
	global $con;
	$anos = array();

	if(($res = $con->query("select distinct ano from inscricoes order by ano")) !== false)
		while($ano = $res->fetch_array())
			$anos[] = $ano[0];

		return $anos;
}



// Wrapper sobre a funcao gettext(), com a sintaxe de sprintf()
function t() {
	$args = func_get_args();
	$args[0] = gettext($args[0]);

	if(func_num_args() <= 1)
		return $args[0];

	return call_user_func_array('sprintf', $args);
}
