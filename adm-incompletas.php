<?php
	require_once 'db.php';
	session_start();

	if(!login_adm()) header('Location: logout.php');
	permissao_adm(); //como só administradores podem acessar essa página, temos que verificar se o usuário realmente é um administrador

	$adm = verifica_adm();
?>

<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Inscrições Incompletas - Administrador</title>
        <link rel="icon" href="imagens/favicon.png" />
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/estilo.css" />
        <link rel="stylesheet" href="css/adm-incompletas.css" />
        <link rel="stylesheet" href="css/adm-cabecalho.css" />
        <!--Precisamos importar esses scripts para que consigamos utilizar recursos jquery/ajax-->
        <script src="js/jquery-1.11.3.js"></script>
        <script src="js/jquery-ui-1.12.0.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery-adm-incompletas.js"></script>
    </head>    
    
	<body>
        <div class="container" id="conteudo-principal">
            <?php require_once('adm-cabecalho.php'); ?>   

            <div id="incompletas">
	            <h2><i class="fa fa-user-times" aria-hidden="true"></i> Inscrições Incompletas</h2>

            	<div id="div-busca">
					<input type="text" id="input-busca" placeholder="Buscar..."/>
					<i class="fa fa-search icone-busca" aria-hidden="true"></i>
				</div>

				<div class="div-adm">
					<?php 
					
						$registros = incompletos();
						echo "<div class='nreg'>Total de registros incompletos: ".$registros[1].".</div>";
						echo "<div class='nencontrados'></div>";

						echo "<table id='registros'>
					            <thead>
					               <th>E-mail</th>
					               <th>Data</th>
					            </thead>
					            
					            <tbody>";

					    if($registros[1]>0)
				            foreach ($registros[0] as $chave => $valor) 
					         	echo " <tr class='linha' data-href='adm-candidato.php?id=".$valor['id']."'>
						                 <td class='email'>".$valor["email"]."</td>
						                 <td class='data'>" . $valor['data'] ."</td>
						               </tr>";
			                 
			     		echo "</tbody></table></div>";
					?>
				</div>
            
		<?php require_once('footer.html'); ?>
		</div>
	</body>
</html>