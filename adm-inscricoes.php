<?php
    require_once('db.php');
    session_start();

    if(!login_adm()) header('Location: logout.php');

    permissao_adm(); //como só administradores podem acessar essa página, temos que verificar se o usuário realmente é um administrador

    $ano = date('Y');
    $periodo_inscricoes = pegar_periodo_inscricoes();
    $qtdperiodos = sizeof($periodo_inscricoes);
    $niveis = carrega_niveis();
    $adm = verifica_adm();

    for($i=0;$i<$qtdperiodos;$i++) {
         //convertendo a data, de formato padrão do mysql para o formato que temos como padrão
        $periodo_inscricoes[$i]['dt_inicial'] = date('d/m/Y', strtotime($periodo_inscricoes[$i]['dt_inicial']));
        $periodo_inscricoes[$i]['dt_final'] = date('d/m/Y', strtotime($periodo_inscricoes[$i]['dt_final']));
    }
    //passamos as variáveis por session para a página chamada pelo jquery
    $_SESSION['periodo_inscricoes'] = $periodo_inscricoes; 
    $_SESSION['niveis'] = $niveis;
?>

<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Abrir Inscriçoes - Administrador</title>
        <link rel="icon" href="imagens/favicon.png" />
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/estilo.css" />
        <link rel="stylesheet" href="css/adm-inscricoes.css" />
        <link rel="stylesheet" href="css/adm-cabecalho.css" />
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />
        <!--Precisamos importar esses scripts para que consigamos utilizar recursos jquery/ajax-->
        <script src="js/jquery-1.11.3.js"></script>
        <script src="js/jquery-ui-1.12.0.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery-adm-inscricoes.js"></script>
    </head>    
        
    <body>
        <div class="container" id="conteudo-principal">
            <?php require_once('adm-cabecalho.php');?>   

            <h2><i class="fa fa-calendar" aria-hidden="true"></i> Abrir Inscrições</h2>

            <form role="form" id="form" method="POST" enctype="multidata/form-data" action="">
                <div id='tabela-informacoes'>
                    <table id="informacoes">
                        <thead>
                            <th class='nivel'>Nível</th>
                            <th class='programa'>Programa</th>
                            <th class='semestre'>Semestre</th>
                            <th class='ano'>Ano</th>
                            <th class='dt-inicial'>Data Inicial</th>
                            <th class='dt-final'>Data Final</th>
                        </thead>                                        

                        <tbody>                        
                            <tr style='display:none;'>
                                <td colspan='6'><input type="hidden" id="controle" value="0" class="<?=$qtdperiodos /*é passado aqui para que possa ser recebido no jquery*/?>" /></td>
                            </tr>

                            <tr class="linha">
                                <td class='nivel'>
                                    <select class="select_nivel campos-padrao-2" name="niveis[]"> <!--São utilizados vetores no name para que, ao clonar as linhas, possamos pegar valores de todos os campos do formulário-->
                                        <option value="" disabled selected style="display:none">Nivel</option>
                                        <?php
                                            $qtdreg = sizeof($niveis);
                                            for ($i=0; $i<$qtdreg; $i++) {
                                                echo "<option value=\"".$niveis[$i]['id']."\">".$niveis[$i]['descricao']."</option>";
                                            }
                                        ?>
                                    </select>
                                </td>

                                <td class='programa'>
                                    <select class="select_programa campos-padrao-2" name="programas[]">
                                        <option value="" disabled selected style="display:none">Programa</option>
                                    </select>
                                </td>

                                <td class='semestre'>
                                    <select class="select_semestre campos-padrao-2" name="semestres[]">
                                        <option value="" style="display:none" disabled selected>Semestre</option>
                                        <option value="1º semestre">1º semestre</option>
                                        <option value="2º semestre">2º semestre</option>
                                        <option value="Verão">Verão</option>
                                    </select>
                                </td>

                                <td class='ano'>
                                    <select class="select_ano campos-padrao-2" name="anos[]">
                                        <option value="" style="display:none" disabled selected>Ano</option>
                                        <option value="<?= $ano ?>" ><?= $ano ?></option>
                                        <option value="<?= $ano + 1 ?>" ><?= $ano + 1 ?></option>
                                    </select>
                                </td>

                                <td class='dt-inicial'><input type="text" class="dt_inicial campos-padrao-2" name="dt_iniciais[]" value="" required></td>
                                <td class='dt-final'><input type="text" class="dt_final campos-padrao-2" name="dt_finais[]" value="" required></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div id='botoes'>
                    <button type='button' id="adicionar" class='bp-3'>+ Alterar outra inscrição</button>
                    <button type='button' id="alterar_tudo" class='bp-3'>+ Alterar todas as inscrições</button>
                    <input type="submit" value="Enviar" class="bp-1">
                </div>
            </form>

               
            <?php 
                if(isset($_POST['niveis'])){ //caso o vetor níveis esteja setado, significa que o usuário já enviou os dados do formulário para atualização dos períodos de inscrição
                    if(abrir_inscricoes($_POST['niveis'], $_POST['programas'], $_POST['semestres'], $_POST['anos'], $_POST['dt_iniciais'], $_POST['dt_finais']))     
                        echo "Períodos atualizados com sucesso!";
                        //se a função de abrir inscrições teve êxito, exibe mensagem de sucesso, caso contrário, mensagem de erro
                    else
                        echo "Não foi possível atualizar os períodos de inscrição. Cheque se preencheu todos os campos corretamentes e, 
                                caso o erro persista, entre em contato com os administradores do sistema!";
                }

            ?>
        <?php require_once('footer.html'); ?>
        </div>
    </body>
</html>