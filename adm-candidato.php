<?php
	require_once('db.php');
	session_start();

	$id 	= $_GET['id'];
	$adm 	= verifica_adm();
	$aluno 	= mostra_aluno($id);

	if(!login_adm()) header('Location: logout.php');
	
    if($aluno === false) echo "Aluno inexistente."; 
    else {
        // $documentos = carrega_documentos($aluno['id_programa'], $aluno['id_nivel']);
        // $num_inscricao = $_GET['ni'];
        // $aluno = mostra_aluno($id, $num_inscricao);
        // $inscricao = mostra_inscricoes($id, $num_inscricao);
        // $link = pdf_unico($id, $num_inscricao);

        $year = date("Y");
        $periodo_inscricoes = pegar_periodo_inscricoes();
        $qtdreg = sizeof($periodo_inscricoes);
        $_SESSION['periodo_inscricoes'] = $periodo_inscricoes;

        $inscricoes = mostra_inscricoes($id);
        if ($inscricoes == false) $tam = 0;
        else $tam = sizeof($inscricoes);

        if(isset($_POST['id_doc'])){
            $n_inscricao = $_POST['num_inscricao'];
            $id_documento = $_POST['id_doc'];
            
            $insere_arquivos = insere_arquivo_aluno($id, $n_inscricao, $id_documento);
            $msg = $insere_arquivos[0];

            if(is_array($insere_arquivos)) {//significa que a inserção deu certo
				email_de_edicao($aluno['email'], $id, $num_inscricao);

                if(!atualiza_data_aluno($id)) 
                    $msg .= " Falha ao atualizar data de modificação do candidato.";

                $class = 'sucesso';
                $icone = "<i class='fa fa-check-circle-o' aria-hidden='true'></i>";//ícone do bootstrap de exito
            }

            else {
                $msg = $insere_arquivos;
                $class = 'erro';
                $icone = "<i class='fa fa-times-circle-o' aria-hidden='true'></i>"; //ícone do bootstrap de erro;
            }

            $titulo = "Envio de documentos";              
        }
        elseif(isset($_POST['recomendante'])){
        	$recomendante = $_POST['recomendante'];
        	$num_inscricao = $_POST['num_inscricao'];
			$insere_carta = insere_carta($id, $num_inscricao, $recomendante);
			$msg = $insere_carta[0];
			$class = $insere_carta[1];
			
			if($class == 'sucesso'){ //significa que deu certo a inserção

				if(!atualiza_data_aluno($id)) $msg = "Cartas de recomendação de ".$recomendante." <b>enviada</b>.<br/> Falha ao atualizar data de modificação do candidato.";
	            $icone = "<i class='fa fa-check-circle-o' aria-hidden='true'></i>";//ícone do bootstrap de exito

			}else 
                $icone = "<i class='fa fa-times-circle-o' aria-hidden='true'></i>"; //ícone do bootstrap de erro;            
			
			$titulo = "Envio de Cartas de Recomendação";
        }
?>
		<!DOCTYPE HTML>
		<html>
			<head>
				<meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, initial-scale=1">
		        <title>Candidato - Administrador | Pós-Graduação do IMECC</title>
		        <link rel="icon" href="imagens/favicon.png" />
		        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />
		        <link rel="stylesheet" href="css/bootstrap.min.css" />
		        <link rel="stylesheet" href="css/estilo.css" />
		        <link rel="stylesheet" href="css/adm-candidato.css" />
		        <link rel="stylesheet" href="css/adm-cabecalho.css" />
		        <script src="js/jquery-1.11.3.js"></script>
		        <script src="js/jquery-ui-1.12.0.js"></script>
		        <script src="js/bootstrap.min.js"></script>
		        <script src="js/jquery-adm-candidato.js"></script>
			</head>

			<body>

				<?php
					if(isset($msg))
		                echo "<div class='modal fade msg' role='dialog'>
		                    <div class='modal-dialog modal-sm'>
		                        <div class='modal-content'>
		                            <div class='modal-header $class'>
		                                <button type='button' class='close' data-dismiss='modal'>&times;</button>
		                                <h4 class='modal-title'>$icone $titulo</h4>
		                            </div>

		                            <div class='modal-body' id='corpo-modal'>
		                                <p>$msg</p>
		                            </div>
		                        </div>
		                    </div>
		                </div>"; 
				?>

				<div class="container" id="conteudo-principal">
					<?php 	require_once('adm-cabecalho.php'); ?>

					<input type='number' id='qtd_inscricoes' value='<?= $tam ?>' hidden>

					<!-- se não for inscrição nova -->
					<div id="informacoes-candidato">
						<h2><i class="fa fa-user" aria-hidden="true"></i> Dados do Candidato</h2>

						<p><span class='b'>Nome:</span> <?= $aluno['nome'] ?></p>
						<p><span class='b'>E-mail:</span> <?= $aluno['email'] ?></p>	         
						<p><span class='b'>Data de modificação da conta:</span> <?= date("d/m/Y \à\s H:i:s", strtotime($aluno['data'])); ?></p>
						<p><span class='b'>Link de acesso:</span> <a href='http://www.ime.unicamp.br/posgrad/inscricao/candidato.php?id=<?= $id ?>' target='_blank'>http://www.ime.unicamp.br/posgrad/inscricao/candidato.php?id=<?= $id ?></a></li>       
					</div>

					<div id="nova-inscricao">
		                <h2 id="h2-nova-inscricao"><i class="fa fa-plus-circle" aria-hidden="true"></i> Nova Inscrição</h2>

		                <form role="form" id='form-inscricao' class="form-horizontal fp-1" method="post" action="">
		                    <input type= "hidden" id="id_aluno" name="id" value="<?= $id ?>" />

		                    <div class="form-group">
		                        <label class="control-label col-sm-3 label-padrao-2" for="input-nome">Nome completo: </label>
		                        <div class="col-sm-9">
		                            <input type="text" id="input-nome" class="campos-padrao-2" name="nome" value="<?= $aluno['nome'] ?>" required />
		                        </div>
		                    </div>

		                    <div class="form-group">
		                        <label class="control-label col-sm-3 label-padrao-2" for="input-email">E-mail: </label>
		                        <div class="col-sm-9">
		                            <input type="email" id="input-email" class="campos-padrao-2" name="email" value="<?= $aluno['email'] ?>" required/>
		                        </div>
		                    </div>

		                    <div class="form-group">
		                        <label class="control-label col-sm-3 label-padrao-2" for="select-programa">Programa: </label>
		                        <div class="col-sm-9">
		                            <select name="programa" id="select-programa" class="campos-padrao-2" required>
		                                    <option value="" selected style="display: none;" disabled>Selecione um programa...</option>
		                                    <?php 
		                                        for ($i=0; $i<$qtdreg; $i++) {
		                                            $chaves = array_keys(array_column($periodo_inscricoes,'programa'),
		                                                                  $periodo_inscricoes[$i]['programa']);

                                                    if($chaves[0] == $i)
                                                        echo "<option value=\"".$periodo_inscricoes[$i]['id_programa']."\" >".$periodo_inscricoes[$i]['corrigido']."</option>";
		                                        }
		                                    ?>
		                                </select>
		                        </div>
		                    </div>

		                    <div class="form-group" id="campos-nivel">
		                        <label class="control-label col-sm-3 label-padrao-2" for="select-nivel">Nível: </label>
		                        <div class="col-sm-9">
		                            <select name="nivel" id="select-nivel" class="campos-padrao-2" required>
		                                <option value="" disabled selected style="display:none">Nivel</option>
		                            </select>
		                        </div>
		                    </div>

		                    <div class="form-group" id="campos-periodo">
		                        <label class="control-label col-sm-3 label-padrao-2">Período: </label>
		                        <div class="col-sm-9">
		                            <span class="nivel">
		                                <select name="semestre" id="select-semestre" class="campos-padrao-2">
		                                	<option value='1º semestre'>1º semestre</option>
		                                	<option value='2º semestre'>2º semestre</option>
		                                	<option value='Verão'>Verão</option>
		                                </select>
		                                 de
		                                <select name="ano" id="select-ano" class="campos-padrao-2">
		                                	<option value='<?=$year?>'><?=$year?></option>		                                	
		                                	<option value='<?=$year+1?>'><?=$year+1?></option>		                                	
		                                </select>
		                            </span>
		                        </div>
		                    </div>

		                    <div class="form-group" id="campos-numero-inscricao">
		                        <label class="control-label col-sm-3 label-padrao-2" for="input-num-inscricao">Número de inscrição: </label>
		                        <div class="col-sm-9">
		                            <span class="numero-inscricao">
		                                 <input id="input-num-inscricao" class="numero-inscricao campos-padrao-2" type="text" name="num_inscricao" value="" required/>
		                            </span>
		                        </div>
		                    </div>


		                    <div class='fp-div-botao'>
		                        <input type="submit" class='bp-1' value="Adicionar Inscrição">
		                    </div>
		                </form>
		            </div>

					<div id="documentos-candidato">
						<h2><i class="fa fa-files-o" aria-hidden="true"></i> Visualização de Documentos</h2>

						<?php

                            if ($tam != 0) {
    	                        echo "  <p id='apresentacao-inscricoes'>Caso deseje realizar alguma alteração, utilize o menu lateral da inscrição.</p>

                                		<p id='aviso-arquivo'>Os arquivos enviados devem ser do formato PDF, com tamanho máximo de 3MB.</p>";

		                        for ($i=0;$i<$tam;$i++) {   
    								$link = pdf_unico($id, $inscricoes[$i]['num_inscricao']);

		                            echo "<div class='row container'>
		                                    <div id='".$inscricoes[$i]['num_inscricao']."' class='inscricao col-sm-12'>
		                                        <div class='inscricao-header'> ";

		                            if($inscricoes[$i]['b_num_inscricao'] == 0)
		                                echo $inscricoes[$i]['nivel']." • ".$inscricoes[$i]['programa']."<span class='float-right'>";

		                            else
		                                echo $inscricoes[$i]['nivel']." • ".$inscricoes[$i]['programa']." <span class='float-right'><span class='inscricao-numero'>".$inscricoes[$i]['num_inscricao']."</span>";


		                                    echo    "<div class='dropdown'>
		                                                <button data-toggle='dropdown' class='botao-opcoes-inscricoes'><i class='fa fa-ellipsis-v inscricoes-icone' aria-hidden='true'></i></button>
		                                                    <ul class='dropdown-menu dropdown-menu-right opcoes'>
		                                                        <li class='opcao editar-docs'><a class='opcao'><i class='fa fa-files-o' aria-hidden='true'></i> Editar documentos</a></li>
		                                                        <li class='opcao add-carta'><a class='opcao'><i class='fa fa-envelope-o' aria-hidden='true'></i> Enviar carta de recomendação</a></li>
		                                                        <li class='opcao deleta-inscricao' id='".$inscricoes[$i]['num_inscricao']."'><a class='opcao'><i class='fa fa-trash-o' aria-hidden='true'></i> Apagar inscrição</a></li>		                                                        		                                        				
		                                                    </ul>
		                                            </div></span>

		                                        </div>";

		                
		                            $documentos = mostra_documentos_inscricao($id, $inscricoes[$i]['num_inscricao'], $inscricoes[$i]['id_programa'], $inscricoes[$i]['id_nivel']);
		                            $qtddocs = sizeof($documentos);
		                        
		                            echo "<div class='conteudo-inscricao'>
		                                    <div class='row container'>
		                                    <div class='col-sm-3 inscricao-info'>";

		                                    if($inscricoes[$i]['b_num_inscricao'] != 0)
		                                        echo "<p><span class='b'>Número de Inscrição</span>: ".$inscricoes[$i]['num_inscricao'];

		                                echo "<p><span class='b'>Data de Inscrição</span>: ".strftime('%d de %B de %Y', strtotime($inscricoes[$i]['data']))."</p>
		                                        <br/>
		                                        <p><span class='b'>Programa:</span> ".$inscricoes[$i]['programa']."</p>
		                                        <p><span class='b'>Nível:</span> ".$inscricoes[$i]['nivel']."</p> 
		                                        <p><span class='b'>Período:</span> ".$inscricoes[$i]['semestre']." de ".$inscricoes[$i]['ano']."</p>		                                        
		                                        <br/>
	                                        	
	                                        	<div style='text-align: center;'>
	                                        		<button class='b-download-pdf bp-2' type='button' data-href='".$link."'>
	                                        			<img src='imagens/download-pdf.png'/> Baixar arquivos
	                                        		</button>
	                                        	</div>
	                                        	
		                                    </div>";

		                                    echo "<div class='col-sm-9 inscricao-documentos'>
		                                        	<table class='tabela-documentos'>
		                                        		<th colspan='3'>Documentos</th>";
		                                    for($l=0; $l<$qtddocs; $l++) {
		                                        echo "<tr>
		                                                <td style='width: 40%' class='nome-documento'>";

		                                                if($documentos[$l]['caminho'] != " ")
		                                                    echo "<a href='".$documentos[$l]['caminho']."' target='_blank'>".$documentos[$l]['documento']."</a></td>";

		                                                else
		                                                    echo $documentos[$l]['documento']."</td>";

		                                        echo " <td style='width: 50%' class='modificacao'><span>".$documentos[$l]['modificacao']."</span></td>";

		                                        echo " <td style='width: 10%' class='upload'>
		                                                    <div class='button botao-upload'>    
		                                                        <form id='form-upload-".$i.$l."' class='form' action='' enctype='multipart/form-data' method='post'>
		                                                            <span class='span-upload'><img src='imagens/upload-pdf.png' class='img-upload-pdf' /> </span>

		                                                            <input type='file' name='".$documentos[$l]['id']."' id='".$i.$l."' class='input-arquivo' />
		                                                            <input type='hidden' name='id_doc' value='".$documentos[$l]['id']."' />
		                                                            <input type='hidden' name='id_aluno' value='".$inscricoes[$i]['id_aluno']."' />
		                                                            <input type='hidden' name='num_inscricao' value='".$inscricoes[$i]['num_inscricao']."' />
		                                                        </form>
		                                                    </div>
		                                                </td>
		                                              </tr>";     
		                                    }
					                        $cartas = mostra_cartas_recomendacao($inscricoes[$i]['num_inscricao'], CR_CARTA_RECEBIDA);
					                        $qtdcartas = sizeof($cartas);

					                        echo "<th colspan='3'>Cartas de Recomendação</th>
					                        	  <tr style='display:none' id='num-inscricao' name='".$inscricoes[$i]['num_inscricao']."'></tr>";
											
											if(!is_bool($cartas)){
												foreach($cartas as $key => $carta){
													echo "<tr name='".$key."' class='tr_carta'>
						                                   <td style='width: 40%' class='nome-documento'><a id='".$carta['id']."' href='posdados/".$id."/".$inscricoes[$i]['num_inscricao']."/Carta_".$carta['id'].".pdf' target='_blank'>".$carta['recomendante']."</a></td>
						                                   <td style='width: 53%' class='modificacao-carta'><span>".strftime('Enviado em %d/%m/%Y às %H:%M:%S', strtotime($carta['data']))."</span></td>";
													if($adm)
														echo "<td style='width: 7%' class='deleta-arquivo' data-idr='".$carta['id_recomendacao']."'><i class='fa fa-trash'></i></td>";
													
													echo "</tr>";	
												}
											}else echo "<tr name='cartas' class='nenhuma-carta'><td colspan='3' style='text-align:center;'>Nenhuma carta de recomendação enviada.</td></tr>";
		                                    
											echo "<tr hidden class='tr-upload-carta'>
                                                    <td style='width:50%'>
                                                    	<form class='form form-carta' action='' enctype='multipart/form-data' method='post'>
	                                                    <input type='text' id='recomendante' name='recomendante' class='input-recomendante campos-padrao-2' placeholder='Recomendante...'/>
	                                                </td>
	                                                <td style='width: 30%'></td>
	                                                <td style='width: 20%' class='upload-carta'>
	                                                    <div class='button botao-upload'>
	                                                            <span class='span-upload'> <img src='imagens/upload-pdf.png' class='img-upload-pdf' /> </span>
	                                                            <input type='file' name='carta_".$inscricoes[$i]['num_inscricao']."' class='input-carta'/>
	                                                            <input type='hidden' name='num_inscricao' value='".$inscricoes[$i]['num_inscricao']."' />
	                                                        </form>
	                                                    </div>
	                                                </td>
	                                            </tr>";

	                                         /*echo "<tr>
                                                    <td style='width:50%'>
	                                                    <input type='text' id='recomendante' name='recomendante' class='input-recomendante campos-padrao-2' placeholder='Recomendante...'/>\
	                                                </td>
	                                                <td style='width: 30%'></td>
	                                                <td style='width: 20%' class='upload-carta'>
	                                                    <div class='button botao-upload'>
	                                                        <form class='form' action='' enctype='multipart/form-data' method='post'>
	                                                            <span class='span-upload'> <img src='imagens/upload-pdf.png' class='img-upload-pdf' /> </span>
	                                                            <input type='file' name='carta_"+controle+"' id='"+controle+"' class='input-carta' onchange='envia_carta("+controle+")'/>
	                                                            <input type='hidden' name='num_inscricao' value='"+ni+"' />
	                                                        </form>
	                                                    </div>
	                                                </td>
	                                            </tr>";*/   
		                                    
		                                    echo    "</table>
		                                        </div>
		                                    </div>				                                                                      
		                                </div>
		                                </div>
		                                </div>";
											
					                    } 

		                                // <div class='col-sm-12 inscricao-aviso' style='text-align:center'>
		                                //         <span style='text-transform:uppercase'>".$_SESSION['login']."</span>, baixe os documentos do aluno clicando na imagem <img src='imagens/download-pdf.png' class='img-upload-pdf' /></p>
		                                //     </div>	
		                        }

						?>
					</div>
				<?php require_once('footer.html'); ?>
				</div>
			</body>
		</html>
<?php } ?>
