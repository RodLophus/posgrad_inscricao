<?php 
    require_once('db.php');

    $informacoes = $_POST['infos'];
    $modificado = array();

    foreach($informacoes as $chave => $valor) {
        $modificado[$valor['name']]=$valor['value'];
        extract($modificado);

    }

    if(!$num_inscricao || $num_inscricao == "") {
        $sem = $semestre[0];
        $num_inscricao = "NA".date('y').$sem."S".$programa.$nivel.$id;
        $num_inscricao = strtoupper($num_inscricao);
    }

    if(valida_email(trim($email))){
        if(sql_injection($nome, $num_inscricao) == false){
            $verificacao = edita_inscricao($id, trim($nome), trim($email), trim($num_inscricao), $programa, 
                                           $nivel, $semestre, $ano);

            if($verificacao == false)
                echo "Não foi possível criar a inscrição. \n Tente novamente.";

            else {
                atualiza_data_aluno($id);
                echo 1;
            }

        }else echo "Insira dados coerentes.";
        
    }else echo "Insira um E-mail válido.";
?>