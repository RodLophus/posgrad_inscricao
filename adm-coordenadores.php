<?php
	require_once 'db.php';
	session_start();

	if(!login_adm()) header('Location: logout.php');
	permissao_adm(); //como só administradores podem acessar essa página, temos que verificar se o usuário realmente é um administrador

	$i=0;
	$coordenadores = coordenadores();
	$adm = verifica_adm();
?>

<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Gerenciar Avaliadores - Administrador | Pós Graduação do IMECC</title>
        <link rel="icon" href="imagens/favicon.png" />
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/estilo.css" />
        <link rel="stylesheet" href="css/adm-coordenadores.css" />
        <link rel="stylesheet" href="css/adm-cabecalho.css" />
        <!--Precisamos importar esses scripts para que consigamos utilizar recursos jquery/ajax-->
        <script src="js/jquery-1.11.3.js"></script>
        <script src="js/jquery-adm-coordenadores.js"></script>
        <script src="js/jquery-ui-1.12.0.js"></script>
        <script src="js/bootstrap.min.js"></script>
	</head>
	<body>

		<div class="container" id="conteudo-principal">
         	<?php require_once('adm-cabecalho.php'); ?> 		
		
            <h2>
				<i class="fa fa-users" aria-hidden="true"></i>  Gerenciar Avaliadores
			</h2>

			<div class='div-adm col-sm-12'>
				<form role="form" action = "" id='form-emails' class='form-horizontal' method = "post">				
						<?php
							$qtd = sizeof($coordenadores);

							for($i=0; $i<$qtd;$i++) {
								$niveis = coordenadores($coordenadores[$i]['id_programa']);//salva apenas os níveis gerenciados pelo programa do respectivo administrador

								echo "<div class='row div-coord'>
										    <div class='div-ficha col-sm-8'>  
												<div class='form-group'>
													<span class='nome-coord col-sm-12'>".$coordenadores[$i]['login']."</span>
												</div>

												<div class='form-group'>
													<label class='control-label label-padrao-2 col-sm-2'>Programa:</label> 
													<span class='col-sm-9 desc-prog'>".$coordenadores[$i]['corrigido']." (".implode(', ', array_column($niveis, 'descricao')).")</span>
												</div>

												<div class='form-group'>
													<label for='".$coordenadores[$i]['login']."' class='control-label label-padrao-2 col-sm-2'>E-mail:</label> 
													<input class='col-sm-5' type='text' id='".$coordenadores[$i]['login']."' name='".$coordenadores[$i]['login']."' 
														placeholder='".$coordenadores[$i]['email']."'/>
												</div>
		                                    </div>
	                                  		<div class='col-sm-3 div-msg ".$coordenadores[$i]['login']."'></div>
	                                  </div>";//implode concatena os elementos de um array, com o separador ','									
	                                  
							}
		                ?>                                        

						<div class='fp-div-botao'><input type="submit" class='bp-1' value="Gerar nova senha"/></div>

					</form>
				</div>		
			<?php require_once('footer.html'); ?>
		</div>
	</body>
</html>
