<?php
	require_once('db.php');
	session_start();

	if(!login_adm()) header('Location: logout.php'); 

	excluir_registros_antigos();
	excluir_zip_antigos();
	$niveis = carrega_niveis();
	$login = $_SESSION["login"];
	$atual = date("Y");
	$atual_mais = $atual + 1;
	$adm = verifica_adm();

	if(!$adm)
		header('location: adm-buscas.php');

	/*if(isset($_GET['lra'])){ //se foi clicado na opção excluir registros antigos, a página é recarregada passando essa variável, e se tiver setada, executa a função para exclusão
		$msg = excluir_registros_antigos(); //a função retorna um array com (mensagem de sucesso ou erro, variável controle com erro ou sucesso)
		$class 	= $msg[1]; // variável que indica se houve sucesso ou erro
		$titulo = "Limpeza de registros antigos";
		if($class=='erro') $icone = "<i class='fa fa-times-circle-o' aria-hidden='true'></i>"; //ícone do bootstrap de erro
		else 				$icone  = "<i class='fa fa-check-circle-o' aria-hidden='true'></i>"; //ícone do bootstrap de exito

		// janela modal com a mensagem obtida		
		echo 	"<div class='modal fade msg' role='dialog'>
					<div class='modal-dialog modal-lg'>
						<div class='modal-content'>
							<div class='modal-header $class'>
								<button type='button' class='close' data-dismiss='modal'>&times;</button>
								<h4 class='modal-title'>$icone $titulo</h4>
	      					</div>

		      				<div class='modal-body' id='corpo-modal'>
		        				<p>$msg[0]</p>
		      				</div>
	      				</div>
	    			</div>
				</div>";	
	}*/

?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Área do Administrador | Pós-Graduação do IMECC</title>
        <link rel="icon" href="imagens/favicon.png" />
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/adm-cabecalho.css" />
        <link rel="stylesheet" href="css/estilo.css" />
        <link rel="stylesheet" href="css/administrador.css" />
        <script src="js/jquery-1.11.3.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script type="text/javascript">
        	$(document).ready(function(){
        		ajusta();
        		$(window).on('resize', ajusta);
        	});

        	function ajusta(){
	        	$('.mi-item').css('height', 'auto');
	        	
	        	var maiorHeight = 0;
        		$('.mi-item').each(function(){
        			if($(this).height() > maiorHeight)
        				maiorHeight = $(this).height();
        		});

        		$('.mi-item').height(maiorHeight);
        	}
        </script>
    </head>

    <body>
    	<div class="container" id="conteudo-principal">
    		<?php require_once('adm-cabecalho.php'); ?>

    		<div id="painel">
                <p>Pós-Graduação • Área do Administrador</p>
            </div>

            <div id="mi">
            	<h1>Bem-vindo, administrador!</h1>
            	<p>Selecione uma das opções abaixo. </p>

				<div class="row">
					<a href="adm-buscas.php">
						<div class="col-sm-4 mi-item" id="mi-busca">
							<i class="fa fa-search" aria-hidden="true"></i> Buscar inscrições
						</div>
					</a>

					<a href="adm-inscricoes.php">
				        <div class="col-sm-4 mi-item" id="mi-inscricoes">
				        	<i class="fa fa-calendar" aria-hidden="true"></i> Abrir inscrições
				        </div>
			        </a>

		            <a href="adm-avaliadores.php">
			            <div class="col-sm-4 mi-item" id="mi-gerenciar">
			            	<i class="fa fa-users" aria-hidden="true"></i> Gerenciar avaliadores
			            </div>
			        </a>
		        </div>

		        <div class="row">
	            	<a href="adm-incompletas.php">
		            	<div class="col-sm-4 mi-item" id="mi-incompletas">
		            		<i class="fa fa-list-alt" aria-hidden="true"></i> Inscrições incompletas
		            	</div>
		            </a>
	            
	            	<!-- <a href="administrador.php?lra=1">
		            	<div class="col-sm-4 mi-item" id="mi-limpeza">
		            		<i class="fa fa-trash" aria-hidden="true"></i> Limpeza de registros antigos
		            	</div>
	            	</a> -->

            		<a href="adm-senha.php">
	            		<div class="col-sm-4 mi-item" id="mi-senha">
	            			<span id="fa-senha"><i class="fa fa-key" aria-hidden="true"></i><i class="fa fa-lock" aria-hidden="true"></i></span> Alterar senha
		            	</div>
		            </a>

		        </div>
            </div>

    	<?php require_once('footer.html'); ?>
    	</div>
    </body>
</html>