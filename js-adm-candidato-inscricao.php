<?php
    session_start();

	$periodo_inscricoes = $_SESSION['periodo_inscricoes'] ;
	$programa = $_POST['programa'];	
	$chave = array_keys(array_column($periodo_inscricoes,'id_programa'),$programa); //encontra as posições do array onde o id_programa = $programa recebido como parâmetro

	$retorno = array();
	$i=0;

	if(!isset($_POST['nivel'])) {
		foreach($chave as $val) {	//percorrendo o array original e salvando num novo apenas as informações desejadas
			$retorno[$i]['id_nivel'] = $periodo_inscricoes[$val]['id_nivel'];
			$retorno[$i]['nivel'] = $periodo_inscricoes[$val]['nivel'];		
			$retorno[$i]['status'] = $periodo_inscricoes[$val]['status'];
			$retorno[$i]['ni'] = $periodo_inscricoes[$val]['b_num_inscricao'];

			$i++;//índice do novo array
		}

		echo json_encode($retorno); //codificar o array para ser lido no jquery
	}

	else {
		foreach($chave as $pos) {//percorrendo todos os índices necessários
			if ($periodo_inscricoes[$pos]['id_nivel'] == $_POST['nivel']) {//encontrando as linha correspondente ao nivel setado 				

				$semestre = $periodo_inscricoes[$pos]['semestre']; 
				$ano = $periodo_inscricoes[$pos]['ano']; 
				$ni = $periodo_inscricoes[$pos]['b_num_inscricao']; //boolean que indica se o candidato precisa preencher seu número de inscrição ou não
			}
		}
		$nivel_semestre_ano = array("semestre" => $semestre, 
									"ano"      => $ano,
									"ni"	   => $ni);
		echo json_encode($nivel_semestre_ano);//codifica o array para que possa ser lido no jquery
	}
?>