<?php 
	require_once('db.php');
	session_start();
	$adm = verifica_adm();

	$mensagem = "";
	if(!login_adm()) header('Location: logout.php');
		
	if(isset($_POST['nova_senha']) and isset($_POST['confirmar_nova_senha'])){
		
		if($_POST['nova_senha'] == $_POST['confirmar_nova_senha']){

				if(senha_adm($_SESSION['login'], $_POST['nova_senha'])){						
	                $class = 'sucesso';
	                $icone = "<i class='fa fa-check-circle-o' aria-hidden='true'></i>";//ícone do bootstrap de exito	
					$mensagem = "Nova senha definida com sucesso.";
					$_SESSION['senha'] =  md5($salt.$_POST['nova_senha']);
				}
				else {
					$mensagem = "Não foi possível redefinir a senha, verifique se escolheu uma senha diferente da atual.";
		            $class = 'erro';
		            $icone = "<i class='fa fa-times-circle-o' aria-hidden='true'></i>"; //ícone do bootstrap de erro;
				}

		}
		else {
			$mensagem = "As senhas inseridas não conferem. Tente novamente.";
            $class = 'erro';
            $icone = "<i class='fa fa-times-circle-o' aria-hidden='true'></i>"; //ícone do bootstrap de erro;
		}

	}

?>

<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mudança de Senha - Administrador | Pós-Graduação do IMECC</title>
        <link rel="icon" href="imagens/favicon.png" />
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/estilo.css" />
        <link rel="stylesheet" href="css/adm-senha.css" />
        <link rel="stylesheet" href="css/adm-cabecalho.css" />
        <!--Precisamos importar esses scripts para que consigamos utilizar recursos jquery/ajax-->
        <script src="js/jquery-1.11.3.js"></script>
        <script src="js//jquery-ui-1.12.0.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery-adm-inscricoes.js"></script>
	</head>

	<script>
		$(document).ready(function(){
			$('.msg').modal('show');
		});
	</script>

	<body> 
			<?php 
				if($mensagem != "" && $class=='sucesso') { 		           
		            $titulo = "Alteração de senha";              


		            echo "<div class='modal fade msg' role='dialog'>
		                <div class='modal-dialog modal-sm'>
		                    <div class='modal-content'>
		                        <div class='modal-header $class'>
		                            <button type='button' class='close' data-dismiss='modal'>&times;</button>
		                            <h4 class='modal-title'>$icone $titulo</h4>
		                        </div>

		                        <div class='modal-body' id='corpo-modal'>
		                            <p>$mensagem</p>
		                        </div>
		                    </div>
		                </div>
		            </div>";
	        	}
     		?>

        <div class="container" id="conteudo-principal">
         	<?php require_once('adm-cabecalho.php');?> 		
		
            <h2>
				<i class="fa fa-unlock-alt"></i> 
				Alterar senha 
			</h2>

			<div id='div-senha col-sm-10 col-sm-offset-2'>
				<form role="form" action = "" class='form-horizontal fp-1' method = "post">
					<div class='form-group'>
						<label for='nova' class="control-label col-sm-5 label-padrao-2">Nova senha:</label>		
						<div class='col-sm-5'>
							<input type = "password" class="campos-padrao-2" id='nova' name = "nova_senha" placeholder = "*********" minlength="7" required/>
						</div>
					</div>

					<div class='form-group'>
						<label for='confirmar' class="control-label col-sm-5 label-padrao-2">Confirmar nova senha:</label>
						<div class='col-sm-5'>
							<input type = "password" class="campos-padrao-2" id='confirmar' name = "confirmar_nova_senha" placeholder = "*********" minlength="7" required/>
						</div>
					</div>

					<div id='msg-erro'>
						<?php 
							if($mensagem != "" && $class=='erro') {
								echo $mensagem; 		      
							}
						?>
					</div>	

					<div class='fp-div-botao'>
						<input type = "submit" class = 'bp-1' value = "Enviar">
					</div>
				</form>
			</div>
		<?php require_once('footer.html'); ?>
	</div>
	</body>
</html>