<?php
	require_once('db.php');
	header('Content-Type: text/html; charset=UTF-8');

	excluir_zip_antigos();
	$id = $_GET['id'];
	$dados_aluno = mostra_aluno($id);
	$sucesso = false;

	if($dados_aluno === false)
		echo "Aluno inexistente.";
	else {
		$year = date("Y");
		$periodo_inscricoes = pegar_periodo_inscricoes();
		$qtdreg = sizeof($periodo_inscricoes);

		$inscricoes = mostra_inscricoes($id);

		if($inscricoes === false) $tam = 0;
		else $tam = sizeof($inscricoes);

		SESSION_START();
		$_SESSION['periodo_inscricoes'] = $periodo_inscricoes;

		if(isset($_POST['id_doc'])){
			$titulo = "Envio de documentos";

			$n_inscricao = $_POST['num_inscricao'];
			$id_documento = $_POST['id_doc'];

			$insere_arquivos = insere_arquivo_aluno($id, $n_inscricao, $id_documento);

			if(is_array($insere_arquivos)) { //significa que a inserção deu certo
				email_de_edicao($dados_aluno['email'], $id, $n_inscricao);

					if(!atualiza_data_aluno($id))
						$insere_arquivos[0] .= " Falha ao atualizar data de modificação do candidato.";

					$msg = $insere_arquivos[0];
					$sucesso = true;
			} else
				$msg = $insere_arquivos;
		}

		if(isset($_POST['nome_recomendante'])){
			$titulo = "Cartas de recomendação";

			$nome_recomendante = $_POST['nome_recomendante'];
			$email_recomendante = $_POST['email_recomendante'];
			$n_inscricao = $_POST['num_inscricao'];

			$result = insere_recomendante($n_inscricao, $nome_recomendante, $email_recomendante);
			if($result[1] != true) {
					$msg = 'Falha ao cadastrar recomendante. ' . $result[0];
			} else {
					$msg = "Recomendante cadastrado com sucesso.<br/>O recomendante foi avisado por e-mail. Você saberá por esta página quando ele enviar a carta de recomendação.";
					$sucesso = true;
			}
		}

	$class = $sucesso ? 'sucesso' : 'erro';
	$icone = "<i class='fa " . ($sucesso ? 'fa-check-circle-o' : 'fa-times-circle-o ') . "' aria-hidden='true'></i>";

?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Área do Candidato | Pós-Graduação do IMECC</title>
        <link rel="shortcut icon" href="imagens/favicon.png">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />

        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/estilo.css" />
        <link rel="stylesheet" href="css/candidato.css" />
        <script src="js/jquery-1.11.3.js"></script>
        <script src="js/jquery-candidato.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </head>

    <body>
        <?php
            if(isset($msg))
                echo "<div class='modal fade msg modal-msg' role='dialog'>
                    <div class='modal-dialog modal-sm'>
                        <div class='modal-content'>
                            <div class='modal-header $class'>
                                <button type='button' class='close' data-dismiss='modal'>&times;</button>
                                <h4 class='modal-title'>$icone $titulo</h4>
                            </div>

                            <div class='modal-body' id='corpo-modal'>
                                <p>$msg</p>
                            </div>
                        </div>
                    </div>
                </div>";
        ?>

        <div class="container" id="conteudo-principal">
            <nav class="navbar" id="menu">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="http://www.ime.unicamp.br/"><img src="imagens/logo-imecc-branco.png" id="menu-logo"/> <span id="menu-nome">Instituto de Matemática, Estatística e Computação Científica</span></a>
                    </div>
              </div>
            </nav>

            <div id="painel">
                <p>Pós-Graduação • Área do Candidato</p>
            </div>

            <input type='number' id='qtd_inscricoes' value='<?= $tam ?>' hidden>

            <div id='apresentacao'></div>

            <div id="nova-inscricao">
                <h2 id="h2-nova-inscricao"><i class="fa fa-plus-circle" aria-hidden="true"></i> Nova Inscrição</h2>

                <form role="form" id='form-inscricao' class="form-horizontal fp-1" method="post" action="">
                    <input type= "hidden" id="id_aluno" name="id" value="<?= $id ?>" />

                    <div class="form-group">
                        <label class="control-label col-sm-3 label-padrao-2" for="input-nome">Nome completo: </label>
                        <div class="col-sm-9">
                            <input type="text" id="input-nome" class="campos-padrao-2" name="nome" value="<?= $dados_aluno['nome'] ?>" required />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3 label-padrao-2" for="input-email">E-mail: </label>
                        <div class="col-sm-9">
                            <input type="email" id="input-email" class="campos-padrao-2" name="email" value="<?= $dados_aluno['email'] ?>" required/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3 label-padrao-2" for="select-programa">Programa: </label>
                        <div class="col-sm-9">
                            <select name="programa" id="select-programa" class="campos-padrao-2" required>
                                    <option value="" selected style="display: none;" disabled>Selecione um programa...</option>
                                    <?php
                                        $programas_exibidos = array();
                                        for ($i=0; $i<$qtdreg; $i++) {
                                            if ($periodo_inscricoes[$i]['status'] == "aberto" && ($programas_exibidos == NULL || !in_array($periodo_inscricoes[$i]['programa'], $programas_exibidos))) {
                                                echo "<option value=\"".$periodo_inscricoes[$i]['id_programa']."\" >".$periodo_inscricoes[$i]['corrigido']."</option>";
                                                $programas_exibidos[] = $periodo_inscricoes[$i]['programa'];
                                            }
                                        }
                                    ?>
                                </select>
                        </div>
                    </div>

                    <div class="form-group" id="campos-nivel">
                        <label class="control-label col-sm-3 label-padrao-2" for="select-nivel">Nível: </label>
                        <div class="col-sm-9">
                            <select name="nivel" id="select-nivel" class="campos-padrao-2" required>
                                <option value="" disabled selected style="display:none">Nivel</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group" id="campos-periodo">
                        <label class="control-label col-sm-3 label-padrao-2">Período: </label>
                        <div class="col-sm-9">
                            <span class="nivel">
                                <select name="semestre" id="select-semestre" class="campos-padrao-2"></select>
                                 de
                                <select name="ano" id="select-ano" class="campos-padrao-2">

                                </select>
                            </span>
                        </div>
                    </div>

                    <div class="form-group" id="campos-numero-inscricao">
                        <label class="control-label col-sm-3 label-padrao-2" for="input-num-inscricao">Número de inscrição: </label>
                        <div class="col-sm-9">
                            <span class="numero-inscricao">
                                <input id="input-num-inscricao" class="numero-inscricao campos-padrao-2" type="text" name="num_inscricao" maxlength="20" placeholder="(número que consta na ficha de inscrição da DAC)" required/>
                                <p>Caso não possua a ficha de inscrição, <a href="https://www1.sistemas.unicamp.br/siga/ingresso/candidato" target="_blank">clique aqui</a>.</p>
                            </span>
                        </div>
                    </div>


                    <div class='fp-div-botao'>
                        <input type="submit" class='bp-1' value="Adicionar Inscrição">
                    </div>
                </form>
            </div>


            <div id="inscricoes">
                <h2><i class="fa fa-th-list" aria-hidden="true"></i> Suas Inscrições</h2>

                <?php
                    if ($inscricoes != false) {
                        /*echo "  <p id='apresentacao-inscricoes'>Clicando sobre a inscrição desejada, você pode visualizar os documentos já enviados e os pendentes. Caso deseje realizar alguma alteração, utilize o menu lateral à direita da inscrição, vide <i class='fa fa-ellipsis-v' aria-hidden='true' style='color: white; background: #18a9ad; padding: 5px'></i> .</p>

                                <p id='aviso-arquivo'>Os arquivos enviados devem ser do formato PDF, com tamanho máximo de 3MB.</p>";*/
                        echo "<p id='apresentacao-inscricoes'>Clicando sobre a inscrição desejada, você pode visualizar os documentos já enviados e os pendentes. Caso deseje realizar o envio de algum documento, clique sobre o ícone de adição ao final da linha do documento desejado.</p>

                            <p id='aviso-arquivo'>Os arquivos enviados devem ser no formato PDF, com tamanho máximo de 3MB.</p>";

                        for ($i=0;$i<$tam;$i++) {
                            $inscricoes_abertas = data_entre($inscricoes[$i]['dt_inicial'], $inscricoes[$i]['dt_final']);
                            echo "<div class='row container'>
                                    <div id='".$inscricoes[$i]['num_inscricao']."' class='inscricao col-sm-12'>
                                        <div class='inscricao-header'> ";

                            if($inscricoes[$i]['b_num_inscricao'] == 0)
                                echo $inscricoes[$i]['nivel']." • ".$inscricoes[$i]['programa']."<span class='float-right'>";

                            else
                                echo $inscricoes[$i]['nivel']." • ".$inscricoes[$i]['programa']." <span class='float-right'><span class='inscricao-numero'>".$inscricoes[$i]['num_inscricao']."</span>";


                                    /*echo    "<div class='dropdown'>
                                                <button data-toggle='dropdown' class='botao-opcoes-inscricoes' title='Menu de opções'><i class='fa fa-ellipsis-v inscricoes-icone' aria-hidden='true'></i></button>
                                                    <ul class='dropdown-menu dropdown-menu-right opcoes'>
                                                        <li class='opcao editar-docs'><a class='opcao'><i class='fa fa-files-o' aria-hidden='true'></i> Editar documentos</a></li>
                                                        <li class='opcao deleta-inscricao' id='".$inscricoes[$i]['num_inscricao']."'><a class='opcao'><i class='fa fa-trash-o' aria-hidden='true'></i> Apagar inscrição</a></li>
                                                    </ul>
                                            </div></span>

                                        </div>";*/


                                    echo    "<button type='button' class='deleta-inscricao' title='Apagar inscrição' data-toggle='tooltip'><i class='fa fa-trash-o di-icone' aria-hidden='true'></i></button>
                                            </span>
                                        </div>";


                            $documentos = mostra_documentos_inscricao($id, $inscricoes[$i]['num_inscricao'], $inscricoes[$i]['id_programa'], $inscricoes[$i]['id_nivel']);
                            $qtddocs = sizeof($documentos);

                            echo "<div class='conteudo-inscricao'>
                                    <div class='row container'>
                                    <div class='col-sm-3 inscricao-info'>
                                        <p><span class='b'>Data de Inscrição</span>: ".strftime('%d de %B de %Y', strtotime($inscricoes[$i]['data']))."
                                        <p><span class='b'>Programa:</span> ".$inscricoes[$i]['programa']."</p>
                                        <p><span class='b'>Nível:</span> ".$inscricoes[$i]['nivel']."</p> 
                                        <p><span class='b'>Período:</span> ".$inscricoes[$i]['semestre']." de ".$inscricoes[$i]['ano']."</p>

                                    </div>

                                    <div class='col-sm-9 inscricao-documentos'>
                                        <table class='tabela-documentos'>
                                        <th colspan='3'>Documentos</th>";
                                    for($l=0; $l<$qtddocs; $l++) {
                                        echo "<tr>
                                                <td style='width: 40%' class='nome-documento'>";

                                                if($documentos[$l]['caminho'] != " ")
                                                    echo "<a href='".$documentos[$l]['caminho']."' target='_blank'>".$documentos[$l]['documento']."</a></td>";

                                                else
                                                    echo $documentos[$l]['documento']."</td>";

                                        echo " <td style='width: 50%' class='modificacao'><span>".$documentos[$l]['modificacao']."</span></td>";

                                        if($inscricoes_abertas)
                                            echo " <td style='width: 10%' class='upload'>
                                                    <div class='button botao-upload'>
                                                        <form id='form-upload-".$i.$l."' class='form' action='' enctype='multipart/form-data' method='post'>
                                                            <span class='span-upload'> <img src='imagens/upload-pdf.png' class='img-upload-pdf' /> </span>

                                                            <input type='file' name='".$documentos[$l]['id']."' id='".$i.$l."' class='input-arquivo' />
                                                            <input type='hidden' name='id_doc' value='".$documentos[$l]['id']."' />
                                                            <input type='hidden' name='id_aluno' value='".$inscricoes[$i]['id_aluno']."' />
                                                            <input type='hidden' name='num_inscricao' value='".$inscricoes[$i]['num_inscricao']."' />
                                                        </form>
                                                    </div>
                                                </td>";
                                        echo "</tr>";     
                                    }

					                     if($qtdrecomendantes = qtde_recomendantes($inscricoes[$i]['id_programa'], $inscricoes[$i]['id_nivel'])) {
					                        $cartas = mostra_cartas_recomendacao($inscricoes[$i]['num_inscricao']);
					                        $qtdcartas = 0;

					                        echo "<th colspan='3'>Cartas de Recomendação" .
													($inscricoes_abertas ? "<button type='button' class='adiciona-recomendante' title='Adicionar recomendante' data-toggle='tooltip'><i class='fa fa-user-plus di-icone' aria-hidden='true'></i></button>" : '') .
													"</th><tr style='display:none' id='num-inscricao' name='".$inscricoes[$i]['num_inscricao']."'></tr>";
											
											if(!is_bool($cartas)){
												foreach($cartas as $key => $carta){
													$qtdcartas ++;
													switch($carta['status']) {
														case CR_AVISO_ENVIADO:
															$estado_carta = array('Carta solicitada',
																'A Carta de Recomendação já foi solicitada ao recomendante por e-mail.  Aguardando envio da Carta de Recomendação.',
																'fa-clock-o',
																'carta-solicitada');
															break;
														case CR_CARTA_RECEBIDA:
															$estado_carta = array('Carta recebida',
																'O recomendante já enviou a Carta de Recomendação!',
																'fa-check-square-o',
																'carta-recebida');
															break;
														case CR_RECOMENDACAO_REJEITADA:
															$qtdcartas --;
															$estado_carta = array('Solicitação rejeitada',
																'O recomendante rejeitou seu pedido de Carta de Recomendação.  Você deve indicar outro recomendante.',
																'fa-ban',
																'recomendacao-rejeitada');
															break;
														default: $estado_carta = array('Recomendante cadastrado',
																'O sistema não conseguiu enviar a solicitação de carta de recomendação ao recomendante.  Por favor clique no ícone do envelope para tentar enviar o e-mail novamente.',
																'fa-exclamation-triangle',
																'recomendante-cadastrado'); break;
													}
													echo "<tr name='".$key."' class='tr_carta'>
						                                   <td style='width: 40%' class='nome-documento'><a id='".$carta['id']."' href='javascript:;' title='".($carta['email'] ? 'E-mail: ' . $carta['email'] : '(carta de recomendação inserida pelo administrador do sistema)')."' data-toggle='tooltip'>".$carta['recomendante']."</a></td>
						                                   <td style='width: 53%' class='estado-carta ".$estado_carta[3]."'><a href='javascript:;' title='".$estado_carta[1]."' data-toggle='tooltip'>".$estado_carta[0].strftime(' em %d/%m/%Y às %H:%M:%S', strtotime($carta['data']))."&nbsp;&nbsp;<i class='fa ".$estado_carta[2]."' aria-hidden='true'></i></a></td>";
													if($inscricoes_abertas) {
                                                        echo "<td class='botoes-carta'>";
                                                        if(($carta['status'] != CR_CARTA_RECEBIDA) && ($carta['status'] != CR_RECOMENDACAO_REJEITADA))
                                                            echo "<button type='button' class='envia-email-recomendante' title='Re-enviar solicitação de carta de recomendação para: ".$carta['email']."' data-toggle='tooltip' data-placement='left' data-idr='".$carta['id_recomendacao']."'><i class='fa fa-envelope-o di-icone' aria-hidden='true'></i></button>";
                                                        if($carta['id_recomendacao'] && ($carta['status'] != CR_RECOMENDACAO_REJEITADA))
                                                            echo "<button type='button' class='deleta-recomendante' title='Apagar recomendante' data-toggle='tooltip' data-idr='".$carta['id_recomendacao']."'><i class='fa fa-trash-o di-icone' aria-hidden='true'></i></button>";
                                                        echo "</td>";
                                                    }
													echo "</tr>";
												}
											}else echo "<tr name='cartas' class='nenhuma-carta'><td colspan='3' style='text-align:center;'>Nenhuma carta de recomendação enviada.</td></tr>";
										}
                                    echo    "</table>
                                        </div>
                                    </div>";

                                    if($inscricoes_abertas)
                                        echo "<div class='col-sm-12 inscricao-aviso'>
                                                <p><i class='fa fa-exclamation-circle' aria-hidden='true'></i> <span class='b'>ATENÇÃO, CANDIDATO!</span>
                                                <ul><li>Envie todos os documentos pendentes até ".strftime('%d de %B de %Y', strtotime($inscricoes[$i]['dt_final'])).".</li>
																<li class='aviso-recomendantes' ".(($qtdcartas >= $qtdrecomendantes) ? "style='display:none'" : '').">Você precisa indicar ao menos $qtdrecomendantes pessoa".($qtdrecomendantes > 1 ? 's' : '')." para enviar cartas de recomendação (pedidos de recomendação que forem rejeitados pelo recomendante serão desconsiderados).</li>
																</ul></p></div>";

                                echo "</div>
                                    </div>
                                </div>";
                        }
                }

                ?>

            </div>
            <?php require_once('footer.html'); ?>
        </div>
        <?php } ?>
    </body>

</html>
