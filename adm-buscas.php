<?php
	require_once('db.php');
	session_start();

	if(!login_adm()) header('Location: logout.php'); 

	excluir_registros_antigos();
	excluir_zip_antigos();
	$niveis = carrega_niveis();
	$login = $_SESSION["login"];
	$anos_inscricoes = lista_anos_inscricoes();
	$ano_atual = date("Y");
	$adm = verifica_adm();

	/*se o usuário for um administrador, são exebidos todos os programas para que ele faça a busca de qual desejar, 
	caso contrário só é permitida a pesquisa do programa cadastrado para aquele login*/
	if($adm) 
		$informacoes = carrega_programas(); 

	else
		$informacoes = programa_adm($login);
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Busca de Inscrições - Administrador | Pós-Graduação do IMECC</title>
        <link rel="icon" href="imagens/favicon.png" />
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/estilo.css" />
        <link rel="stylesheet" href="css/adm-buscas.css" />
        <link rel="stylesheet" href="css/adm-cabecalho.css" />
        <script src="js/jquery-1.11.3.js"></script>
        <script src="js/jquery-ui-1.12.0.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery-adm-buscas.js"></script>
	</head>

	<body>
		<div class="container" id="conteudo-principal">
			<?php 	require_once('adm-cabecalho.php');

						echo "<div id='apresentacao-coord'>";	
						
						if(!$adm) echo "<h1>Olá, ".$_SESSION['login']."!</h1>";
						
						echo "<p>Para pesquisar inscrições, utilize uma das opções abaixo.</p>
							</div>";
			 ?>

			<div id="buscas">
				<h2 class="h2-busca" name="busca-1"><i class="fa fa-search" aria-hidden="true"></i> Busca por período</h2>
					<form role="form" class="form-horizontal fp-1" method="post" action="" id="busca-1">
						<input type="hidden" name="tipo-busca" value="1" />

						<div class="form-group">
	                        <label class="control-label col-sm-3 label-padrao-2" for="busca-ano">Ano: </label>
	                        <div class="col-sm-9">
	                        	<select id="busca-ano" name="ano" class="campos-padrao-2" required>
									<?php foreach($anos_inscricoes as $ano): ?>
									<option value="<?= $ano ?>" <?= $ano == $ano_atual ? 'selected' : '' ?>><?= $ano ?></option>
									<?php endforeach; ?>
								</select>
	                        </div>
	                    </div>

						<div class="form-group">
	                        <label class="control-label col-sm-3 label-padrao-2" for="busca-semestre">Semestre: </label>
	                        <div class="col-sm-9">
	                        	<select id="busca-semestre" name="semestre" class="campos-padrao-2" required>
	                        		<option value="" style="display: none" disabled selected>Selecione...</option>
									<option value = '1º semestre'>1º semestre</option>
									<option value = '2º semestre'>2º semestre</option>			
									<option value = 'Verão'>Verão</option>
								</select>
	                        </div>
	                    </div>

	                    <div class="form-group">
	                    	<label class="control-label col-sm-3 label-padrao-2" for="busca-programa">Programa:</label>
	                    	<div class="col-sm-9">
	                    		<select id="busca-programa" name="programa" class="campos-padrao-2" required>
	                    			<?php
									if($adm){ //se for administrador todos os programas podem ser escolhidos
										echo "<option value='' selected style='display: none' disabled>Selecione...</option>";
										foreach($informacoes as $valor) echo "<option value=".$valor['id']."> ".$valor['corrigido']." </option>";								
									}else //caso contrário somente um programa pode ser consultado
										echo "<option value=".$informacoes['id_programa']." selected> ".$informacoes['corrigido']." </option>";
									?>
	                    		</select>
	                    	</div>
	                    </div>

	                    <div class="form-group">
	                    	<label class="control-label col-sm-3 label-padrao-2" for="busca-nivel">Nível:</label>
	                    	<div class="col-sm-9">
	                    		<select id="busca-nivel" name="nivel" class="campos-padrao-2"></select>
	                    	</div>
	                    </div>

		                <div class='fp-div-botao'>
		                    <button class="bp-1">Buscar</button>
		                </div>

					</form>

				<h2 class="h2-busca" name="busca-2"><i class="fa fa-search" aria-hidden="true"></i> Busca por nome</h2>
					<form role="form" class="form-horizontal fp-1" method="post" action="" id="busca-2">
						<input type="hidden" name="tipo-busca" value="2" />

						<div class="form-group">
	                    	<label class="control-label col-sm-3 label-padrao-2" for="busca-nome">Nome do candidato:</label>
	                    	<div class="col-sm-9">
	                    		<input type="text" id="busca-nome" name="nome" class="campos-padrao-2" placeholder="Exemplo: João Pedro da Silva Souza" required></select>
	                    	</div>
	                    </div>

	                    <div class='fp-div-botao'>
		                    <button class="bp-1">Buscar</button>
		                </div>
						
					</form>

				<h2 class="h2-busca" name="busca-3"><i class="fa fa-search" aria-hidden="true"></i> Busca por inscrição</h2>
					<form role="form" class="form-horizontal fp-1" method="post" action="" id="busca-3">
						<input type="hidden" name="tipo-busca" value="3" />
	                    <div class="form-group">
	                    	<label class="control-label col-sm-3 label-padrao-2" for="busca-num-inscricao">Número de inscrição:</label>
	                    	<div class="col-sm-9">
	                    		<input type="text" id="busca-num-inscricao" name="num_inscricao" class="campos-padrao-2" placeholder="Exemplo: <?= $ano_atual ?>1S123456" required></select>
	                    	</div>
	                    </div>

	                    <div class='fp-div-botao'>
		                    <button class="bp-1">Buscar</button>
		                </div>
					</form>
			</div>

			<div id="div-resultados">
				<?php
					if(isset($_POST['tipo-busca'])){ //se tiver setada a variável controle significa que o usuário está realizando uma busca
						$registros = array();		

						echo "<h2><i class='fa fa-check-square-o' aria-hidden='true'></i> Resultados da Busca</h2>";

						switch ($_POST['tipo-busca']){						
							case '1': 
							  	$registros = busca_periodo($_POST['ano'], $_POST['semestre'], $_POST['programa'], $_POST['nivel']);
      							
      							echo '<div class="qtd-resultados">Encontrado(s) '.$registros[1].' registro(s).';
							  	if($registros[1]) echo "<p><a id='download-arquivo'
							  									class='compactar-arquivos bp-4'
							  									data-program = '".str_replace(' ', '', ucwords($registros[0][0]['nivel']."Em".$registros[0][0]['corrigido']))."'
							  									data-period = '".filter_var($_POST['semestre'], FILTER_SANITIZE_NUMBER_INT)."'
							  									data-year = '".$_POST['ano']."'
							  									data-href = '".json_encode($registros[0])."'>
							  										<i class='fa fa-download' aria-hidden='true'></i> Download dos Arquivos
							  								</a></p>";
							  	echo '</div>';
							break;
							
							case '2': 
								$registros = busca_nome($_POST['nome'], $adm);
      							echo '<div class="qtd-resultados">Encontrado(s) '.$registros[1].' registro(s) para "'.$_POST['nome'].'".</div>';
							break;

							case '3': 
								$registros = busca_num_inscricao($_POST['num_inscricao'], $adm);
      							echo '<div class="qtd-resultados">Encontrado(s) '.$registros[1].' registro(s) para "'.$_POST['num_inscricao'].'".</div>';
							break;
						}

      					
      					//a variável registro recebe, de acordo com cada busca, em sua primeira posição [0] os registros e na segunda [1] o número de registros
       					if($registros[1]){ //caso haja registros 
       						echo "<p>Clique sobre o candidato desejado para ter acesso à página onde poderá visualizar e alterar informações de sua inscrição.</p>";

       						//if(isset($registros[2])) echo "<a href='".$registros[2]."' download>Download dos arquivos</a>";

	      					echo "<table id='resultados'><tr>
	      							<thead>
				  						<th class='inscricao'>Inscrição</th>
				  						<th class='nome'>Candidato(a)</th>
				  						<th class='programa'>Programa</th>
				  					</thead>

				  					<tbody>";

	      							
							foreach ($registros[0] as $chave => $valor) {
								echo "<tr data-href='adm-candidato.php?id=$valor[id]'>";

								if ($valor['b_num_inscricao'] == 0) $valor['num_inscricao'] = "Não se aplica"; /*significa que o curso não possui um número de inscrição */
								echo "<td class='inscricao'>".$valor['num_inscricao']."</td>
									  <td class='nome'>".$valor['nome']."</td>";

								if ($valor['programa'] != 4)
									echo "<td class='programa'>".$valor['nivel']." em ".$valor['corrigido']."		 </td></a></tr>";
								else
									echo "<td class='programa'>".$valor['corrigido']."</td></a></tr>";

					            }

					            echo "</tbody>
					            	</table>";
						}else{
							echo "<input type='hidden' id='controle-resultados' value='1'>";
						}
					}

				?>
			</div>
			<?php require_once('footer.html'); ?>
		</div>
	</body>
</html>
