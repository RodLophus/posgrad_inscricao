<?php
	require_once('db.php');

	$orientacao = 'Por favor, tente novamente.  Se o problema persistir, contacte: inscricao.posgrad@ime.unicamp.br';

	if(!(isset($_GET['id']) && (($recomendacao = carrega_solicitacao_recomendacao($_GET['id'])) !== false))) {
		print("Não foi possível carregar a solicitação de carta de recomendação.  $orientacao");

	} else {
		$arquivo = getcwd() . sprintf('/posdados/%s/%s/Carta_%s.pdf',
			$recomendacao['id_candidato'], $recomendacao['num_inscricao'], $recomendacao['id_carta']);

		if(is_readable($arquivo)) {
			$tamanho = filesize($arquivo);

			header('Content-type: application/pdf');
			header('Content-Disposition: inline; filename="carta-recomendacao.pdf"');
			header("Content-length: $tamanho");
			header("Cache-control: private");

			readfile($arquivo);
		} else {
			print("Arquivo não encontado.  $orientacao");
		}
	}
?>
